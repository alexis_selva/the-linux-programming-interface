/* Exercice 48-5 */

#ifndef T_DIRECTORY_H
#define T_DIRECTORY_H

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include "binary_sems.h"
#include "tlpi_hdr.h"

/* Permissions for our IPC objects */
#define OBJ_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)

/* Key for shared memory segment */
#define SHM_KEY 0x1234   

/* Key for semaphore set */
#define SEM_KEY 0x5678      

/* Limitation for pair */
#define BUF_SIZE 	(1024)
#define PAIRS_SIZE 	(10)

/* Value for action */
enum {
	CREATE,
	MODIFY,
	DELETE,
	RETRIEVE,
};

/* Pair structure */
struct pair {
	char name[BUF_SIZE];
    char value[BUF_SIZE];
};

#endif /* T_DIRECTORY_H */