/* Exercice 48-6 */

#define _GNU_SOURCE
#include <sys/shm.h>
#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
    int             maxind;
    int             shmid;
    int             ind;
    struct shmid_ds ds;
    struct shm_info info;

    /* Obtain size of kernel 'entries' array */
    maxind = shmctl(0, IPC_INFO, (struct shmid_ds *) &info);
    if (maxind == -1)
        errExit("shmctl-SHM_INFO");
    printf("maxind: %d\n\n", maxind);
    printf("index     id       size\n");

    /* Retrieve and display information from each element of 'entries' array */
    for (ind = 0; ind <= maxind; ind++) {
        shmid = shmctl(ind, SHM_STAT, &ds);
        if (shmid == -1) {
            if (errno != EINVAL && errno != EACCES)
                errMsg("shmctl-SHM_STAT");              /* Unexpected error */
            continue;                                   /* Ignore this item */
        }
        printf("%4d %8d %9ld\n", ind, shmid, (long) ds.shm_segsz);
    }

    exit(EXIT_SUCCESS);
}
