/* Listing 48-5 */

#include "t_directory.h"

static void
usage_error(const char *progName, const char *msg)
{
    if (msg != NULL)
        fprintf(stderr, "%s", msg);
    fprintf(stderr, "Usage: %s {-c name value | -m name value | -d name | -r name}\n", progName);
    fprintf(stderr, "    -c name value	Create   name:value pair\n");
    fprintf(stderr, "    -m name value  Modify   name:value pair\n");
    fprintf(stderr, "    -d name        Delete   name:value pair\n");
    fprintf(stderr, "    -r name        Retrieve name:value pair\n");
    exit(EXIT_FAILURE);
}

static inline int
read_options(int argc, char *argv[], char **name, char **value)
{
	int numKeyFlags = 0;
    int opt;
    int action;

    while ((opt = getopt(argc, argv, "cmdr")) != -1) {
        switch (opt) {
        	case 'c':
            	action = CREATE;
            	numKeyFlags++;
            	break;

        	case 'm':
            	action = MODIFY;
            	numKeyFlags++;
            	break;

        	case 'd':
        		action = DELETE;
            	numKeyFlags++;
            	break;

        	case 'r':
            	action = RETRIEVE;
            	numKeyFlags++;
            	break;

        default:
            usage_error(argv[0], "Unknown option");
        }
    }
    
    if (numKeyFlags != 1)
        usage_error(argv[0], "Exactly one of the options -c, -m, -d or -r must be supplied\n");

	if (action < DELETE && argc != 4)
		usage_error(argv[0], "Name and value must be supplied\n");
	else if (action >= DELETE && argc != 3)
        usage_error(argv[0], "Name must be supplied\n");
        
	*name  = argv[optind];
	*value = argv[optind + 1];
	
	return action;
}

static inline void
reserve_sem(int semid){
	if (reserveSem(semid, 0) == -1)
 		errExit("reserveSem");
}

static inline void
release_sem(int semid){
	if (releaseSem(semid, 0) == -1)
 		errExit("reserveSem");
}

static inline int
do_create(int semid, struct pair *start, char *name, char *value)
{
	struct pair *pair;
    int          index;
    
    for (index = 0, pair = start ; index < PAIRS_SIZE ; index++, pair++) {
    	reserve_sem(semid);
		if (pair->name[0] == '\0') {
        	strncpy(pair->name, name, BUF_SIZE);
        	strncpy(pair->value, value, BUF_SIZE);
        	release_sem(semid);
        	return 0;
		}
		release_sem(semid);
    }
    
	return -1;
}

static inline int
do_modify(int semid, struct pair *start, char *name, char *value)
{
    struct pair *pair;
    int          index;

    for (index = 0, pair = start ; index < PAIRS_SIZE ; index++, pair++) {
    	reserve_sem(semid);
		if (strncmp(pair->name, name, BUF_SIZE) == 0) {
        	strncpy(pair->value, value, BUF_SIZE);
        	release_sem(semid);
        	return 0;
		}
		release_sem(semid);
    }

	return -1;
}

static inline int
do_delete(int semid, struct pair *start, char *name)
{
    struct pair *pair;
    int          index;

    for (index = 0, pair = start ; index < PAIRS_SIZE ; index++, pair++) {
    	reserve_sem(semid);
		if (strncmp(pair->name, name, BUF_SIZE) == 0) {
        	pair->name[0] = '\0';
        	release_sem(semid);
        	return 0;
		}
		release_sem(semid);
    }
    
	return -1;
}

static inline int
do_retrieve(int semid, struct pair *start, char *name)
{
    struct pair *pair;
    int          index;
        
    for (index = 0, pair = start; index < PAIRS_SIZE ; index++, pair++) {
    	reserve_sem(semid);
    	if (strncmp(pair->name, name, BUF_SIZE) == 0) {
    		printf("%s -> %s\n", pair->name, pair->value);
    		release_sem(semid);
        	return 0;
    	}
    	release_sem(semid);
    }

	return -1;
}

static inline void
initialize_action(int *semid, struct pair **start)
{
	int shmid;
	
	/* Create semaphore */
    *semid = semget(SEM_KEY, 1, IPC_CREAT | IPC_EXCL | OBJ_PERMS);
    if (*semid == -1) {
    	if (errno != EEXIST) 
    		errExit("shmget-SEM_KEY");
    	*semid = semget(SEM_KEY, 0, 0);
    } else {
   		if (initSemAvailable(*semid, 0) == -1)
 			errExit("initSemAvailable");
    }
    
	/* Create shared memory; attach at address chosen by system */
    shmid = shmget(SHM_KEY, PAIRS_SIZE * sizeof(struct pair), IPC_CREAT | OBJ_PERMS);
    if (shmid == -1)
        errExit("shmget-SHM_KEY");
    *start = shmat(shmid, NULL, 0);
    if (*start == (void *) -1)
        errExit("shmat");
}

static inline int
do_action(int action, char *name, char *value)
{
	int          semid;
    int          ret;
    struct pair *start;
    
    initialize_action(&semid, &start);
	switch (action) {
		case CREATE:
			ret = do_create(semid, start, name, value);
			break;
		case MODIFY:
			ret = do_modify(semid, start, name, value);
			break;
		case DELETE:
			ret = do_delete(semid, start, name);
			break;
		case RETRIEVE:
			ret = do_retrieve(semid, start, name);
			break;
		default:
			ret = -1;
			break;
	}
	
	if (shmdt(start) == -1)
        errExit("shmdt");
	
	return ret;
}

int
main(int argc, char *argv[])
{
    int   action;
    char *name;
    char *value;
    
    action = read_options(argc, argv, &name, &value);
    do_action(action, name, value);

    exit(EXIT_SUCCESS);
}
