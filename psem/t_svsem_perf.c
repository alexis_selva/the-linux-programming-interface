/* Exercice 53-4 */

#include <sys/sem.h>
#include <sys/stat.h>
#include "semun.h"
#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
	struct sembuf sops = { 0 };
	union semun arg;
	int semid;
	int loops;
    int j;

    loops = (argc > 1) ? getInt(argv[1], GN_GT_0, "num-loops") : 10000000;

    /* Initialize a semaphore with the value 1 */
    semid = semget(IPC_PRIVATE, 1, IPC_CREAT | S_IRUSR | S_IWUSR);
    if (semid == -1)
        errExit("semget");
    arg.val = 1;
    if (semctl(semid, 0, SETVAL, arg) == -1)
        errExit("semctl");
    
    /* Increment/decrement the semaphore loops times */
    for (j = 0; j < loops; j++) {
    	sops.sem_op = -1;
   		if (semop(semid, &sops, 1) == -1)
            errExit("semop");
    	
    	sops.sem_op = 1;
   		if (semop(semid, &sops, 1) == -1)
            errExit("semop");
    }
    
    /* Destroy the semaphore */
	if (semctl(semid, 0, IPC_RMID, arg) == -1)
    	errExit("semctl");

    exit(EXIT_SUCCESS);
}