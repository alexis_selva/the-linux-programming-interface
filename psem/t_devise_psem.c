/* Exercice 53-3 */

#include <sys/sem.h>
#include <sys/stat.h>
#include "semun.h"
#include "tlpi_hdr.h"

#define my_sem_t int

static my_sem_t *my_sem_open(const char *name, int oflag, mode_t mode, unsigned int value)
{
	key_t key = ftok(name, 1);
    if (key == -1)
    	return NULL;
	int semid = semget(key, 1, oflag | mode);
    if (semid == -1)
        return NULL;
    union semun arg = { 0 };
    arg.val = value;
    if (semctl(semid, 0, SETVAL, arg) == -1)
    	return NULL;
    my_sem_t *ptr = malloc(sizeof(my_sem_t));
    if (ptr == NULL)
    	return NULL;
    *ptr = semid;
    return ptr;
}

static int
my_sem_wait(my_sem_t *sem)
{
	struct sembuf sops = { 0 };
	sops.sem_op = -1;
   	return semop(*sem, &sops, 1);
}

static int
my_sem_post(my_sem_t *sem) 
{
	struct sembuf sops = { 0 };
	sops.sem_op = 1;
   	return semop(*sem, &sops, 1);	
}

static int 
my_sem_close(my_sem_t *sem)
{
	if (sem == NULL)
		return -1;
	free(sem);
	return 0;
}

static int
my_sem_unlink(const char *name)
{
	key_t key = ftok(name, 1);
    if (key == -1)
    	return -1;
	int semid = semget(key, 0, 0);
    if (semid == -1)
        return -1;
    union semun arg = { 0 };
	return semctl(semid, 0, IPC_RMID, arg);
}

int
main(int argc, char *argv[])
{
    /* Initialize a semaphore with the value 1 */
    my_sem_t *sem = my_sem_open("./Makefile", IPC_CREAT, S_IRUSR | S_IWUSR, 1);
    if (sem == NULL)
    	exit(EXIT_FAILURE);

    /* Increment/decrement the semaphore */
    if (my_sem_wait(sem) == -1)
        errExit("my_sem_wait");

    if (my_sem_post(sem) == -1)
       	errExit("my_sem_post");
    
    /* Destroy the semaphore */
    if (my_sem_close(sem) == -1)
    	errExit("my_sem_close");
    if (my_sem_unlink("./Makefile") == -1)
        errExit("my_sem_destroy");

    exit(EXIT_SUCCESS);
}