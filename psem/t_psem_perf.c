/* Exercice 53-4 */

#include <semaphore.h>
#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
 	sem_t sem;
    int loops;
    int j;

    loops = (argc > 1) ? getInt(argv[1], GN_GT_0, "num-loops") : 10000000;

    /* Initialize a semaphore with the value 1 */
    if (sem_init(&sem, 0, 1) == -1)
        errExit("sem_init");

    /* Increment/decrement the semaphore loops times */
    for (j = 0; j < loops; j++) {
        if (sem_wait(&sem) == -1)
            errExit("sem_wait");

        if (sem_post(&sem) == -1)
            errExit("sem_post");
    }
    
    /* Destroy the semaphore */
    if (sem_destroy(&sem) == -1)
        errExit("sem_destroy");

    exit(EXIT_SUCCESS);
}