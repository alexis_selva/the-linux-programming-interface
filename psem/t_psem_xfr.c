/* Exercice 48-1 */

#include <semaphore.h>
#include <pthread.h>
#include "tlpi_hdr.h"

#define WRITE_SEM 0             /* Writer has access to shared memory */
#define READ_SEM 1              /* Reader has access to shared memory */

#ifndef BUF_SIZE                /* Allow "cc -D" to override definition */
#define BUF_SIZE 1024           /* Size of transfer buffer */
#endif

static sem_t sem[2];
int cnt;                    
char buf[BUF_SIZE];

static void *
reader(void *arg)
{
    int xfrs, bytes;

    /* Transfer blocks of data from shared memory to stdout */
    for (xfrs = 0, bytes = 0; ; xfrs++) {
		
		/* Wait for our turn */
		if (sem_wait(&sem[READ_SEM]) == -1)
            errExit("sem_wait");

		/* Writer encountered "end" followed by a new line feed */
        if ((cnt == sizeof("end\n") - 1) && (strncmp(buf, "end\n", sizeof("end\n") - 1) == 0))                     
            break;
        bytes += cnt;

        if (write(STDOUT_FILENO, buf, cnt) != cnt)
            fatal("partial/failed write");

		/* Give writer a turn */
        if (sem_post(&sem[WRITE_SEM]) == -1)
            errExit("sem_post");
    }

    fprintf(stderr, "Received %d bytes (%d xfrs)\n", bytes, xfrs);
    return NULL;
}

static void *
writer(void *arg)
{
    int bytes, xfrs;

    /* Transfer blocks of data from stdin to shared memory */
    for (xfrs = 0, bytes = 0; ; xfrs++, bytes += cnt) {
    	
    	/* Wait for our turn */
    	if (sem_wait(&sem[WRITE_SEM]) == -1)
    		errExit("sem_wait");

        cnt = read(STDIN_FILENO, buf, BUF_SIZE);
        if (cnt == -1)
            errExit("read");

        /* Give reader a turn */
		if (sem_post(&sem[READ_SEM]) == -1)
            errExit("sem_post");

		/* Reader encountered "end" followed by a new line feed */
        if ((cnt == sizeof("end\n") - 1) && (strncmp(buf, "end\n", sizeof("end\n") - 1) == 0))                     
            break;
    }
        
    return NULL;
}

int
main(int argc, char *argv[]) 
{
	int s;
	pthread_t t1, t2;
	
	/* Initialize the two semaphores */
    if (sem_init(&sem[WRITE_SEM], 0, 1) == -1)
        errExit("sem_init");
    if (sem_init(&sem[READ_SEM], 0, 0) == -1)
        errExit("sem_init");
        
    /* Create two threads */
    s = pthread_create(&t1, NULL, reader, NULL);
    if (s != 0)
        errExitEN(s, "pthread_create");
    s = pthread_create(&t2, NULL, writer, NULL);
    if (s != 0)
        errExitEN(s, "pthread_create");

    /* Wait for threads to terminate */
    s = pthread_join(t1, NULL);
    if (s != 0)
        errExitEN(s, "pthread_join");
    s = pthread_join(t2, NULL);
    if (s != 0)
        errExitEN(s, "pthread_join");
        
    /* Destroy the two semaphores */
    if (sem_destroy(&sem[READ_SEM]) == -1)
        errExit("sem_destroy");
    if (sem_destroy(&sem[WRITE_SEM]) == -1)
        errExit("sem_destroy");
	
	exit(EXIT_SUCCESS);
}