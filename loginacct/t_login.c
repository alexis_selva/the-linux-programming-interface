/* Exercice 40-3 */

#define _GNU_SOURCE
#include <time.h>
#include <utmpx.h>
#include <paths.h>              /* Definitions of _PATH_UTMP and _PATH_WTMP */
#include "tlpi_hdr.h"

static void
my_login(struct utmpx *ut)
{
    char *devName;

    ut->ut_type = USER_PROCESS;   
    if (time((time_t *) &ut->ut_tv.tv_sec) == -1)
        errExit("time");
    ut->ut_pid = getpid();
    devName = ttyname(STDIN_FILENO);
    if (devName == NULL)
        errExit("ttyname");
    if (strlen(devName) <= 8)           
        fatal("Terminal name is too short: %s", devName);
    strncpy(ut->ut_line, devName + 5, sizeof(ut->ut_line));
    
    /* Rewind to start of utmp file */
    setutxent();                    
    
    /* Write login record to utmp */
    if (pututxline(ut) == NULL)        
        errExit("pututxline");
        
    /* Append login record to wtmp */
    updwtmpx(_PATH_WTMP, ut);          

    /* Close utmp file */
    endutxent();
}

int
main(int argc, char *argv[])
{
    struct utmpx ut;
    
    /* Read options */
    if (argc < 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s username\n", argv[0]);

    /* Fill login record for utmp and wtmp files */
    memset(&ut, 0, sizeof(struct utmpx));
    strncpy(ut.ut_user, argv[1], sizeof(ut.ut_user));
    my_login(&ut);
    
    exit(EXIT_SUCCESS);
}
