/* Exercice 40-4 */

#define _GNU_SOURCE
#include <time.h>
#include <utmpx.h>
#include <paths.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
    struct utmpx *ut;
    char         *ut_time;
    int           ut_time_len;
    setutxent();
    
    while ((ut = getutxent()) != NULL) {        /* Sequential scan to EOF */
        if (ut->ut_type == USER_PROCESS) {
            printf("%s ", ut->ut_user);
            printf("%s ",ut->ut_line);
            ut_time = ctime((time_t *) &(ut->ut_tv.tv_sec));
            ut_time_len = strlen(ut_time);
            printf("%.*s ", ut_time_len - 1, ut_time);
            printf("(%s)\n", ut->ut_host);
        }
    }

    endutxent();
    exit(EXIT_SUCCESS);
}
