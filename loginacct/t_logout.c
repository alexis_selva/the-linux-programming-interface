/* Exercice 40-3 */

#define _GNU_SOURCE
#include <time.h>
#include <utmpx.h>
#include <paths.h>              /* Definitions of _PATH_UTMP and _PATH_WTMP */
#include "tlpi_hdr.h"

static int
my_logout(const char* ut_line)
{
    struct utmpx *ut;
    
    /* Rewind to start of utmp file */
    setutxent();                    

     /* Read utmp file */
    ut = getutxent();
    while (ut) {
        if (strncmp(ut_line, ut->ut_line, __UT_NAMESIZE) == 0) {
            
            ut->ut_type = DEAD_PROCESS;
            time((time_t *) &ut->ut_tv.tv_sec);  /* Stamp with logout time */
            memset(&ut->ut_user, 0, sizeof(ut->ut_user));
            memset(&ut->ut_host, 0, sizeof(ut->ut_host));
            
            if (pututxline(ut) == NULL)        /* Overwrite previous utmp record */
                errExit("pututxline");
            updwtmpx(_PATH_WTMP, ut);          /* Append logout record to wtmp */
            endutxent();
            return 1;   
        }
        ut = getutxent();
    }
    
    endutxent();
    return 0;
}

int
main(int argc, char *argv[])
{
    char *devName;
    devName = ttyname(STDIN_FILENO);
    if (devName == NULL)
        errExit("ttyname");
    if (strlen(devName) <= 8)           
        fatal("Terminal name is too short: %s", devName);
    if (my_logout(devName + 5) == 0) {
        errExit("my_logout");
    }
    
    exit(EXIT_SUCCESS);
}