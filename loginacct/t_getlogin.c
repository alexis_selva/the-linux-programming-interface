/* Exercice 40-1 */

#define _GNU_SOURCE
#include <time.h>
#include <utmpx.h>
#include <paths.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "tlpi_hdr.h"

static char *
my_getlogin(void) {
    static char username[__UT_NAMESIZE];
    struct utmpx *ut;
    char *console;
    
    /* Get console name */
    console = ttyname(STDOUT_FILENO);
    if (console == NULL) {
        errExit("ttyname");
    }
    
    /* Reset username */
    bzero(username, __UT_NAMESIZE);
    
    /* Rewind utmp file to the beginning */
    setutxent();
    
    /* Read utmp file */
    ut = getutxent();
    if (ut == NULL) {
        errExit("getutxent");
    }
    
    do {
        if (strncmp(console + 5, ut->ut_line, sizeof(ut->ut_line)) == 0) {
            strncpy(username, ut->ut_user, sizeof(ut->ut_user));
        } else {
            ut = getutxent();
        }
    } while (ut && username[0] == '\0');

    /* Close utmp file */
    endutxent();
    
    return username;
}

int
main(int argc, char *argv[])
{
    char *login = my_getlogin();
    if (login) {
        printf("%s\n", login);
    }
    
    exit(EXIT_SUCCESS);
}
