/* Exercice 40-3 */

#define _GNU_SOURCE
#include <time.h>
#include <utmpx.h>
#include <paths.h>              /* Definitions of _PATH_UTMP and _PATH_WTMP */
#include "tlpi_hdr.h"

static void my_logwtmp(const char *line, const char *name, const char *host) {
    struct utmpx ut;
    memset(&ut, 0, sizeof(struct utmpx));
    strncpy(ut.ut_line, line, sizeof(ut.ut_line));
    strncpy(ut.ut_user, name, sizeof(ut.ut_user));
    strncpy(ut.ut_host, host, sizeof(ut.ut_host));
    if (time((time_t *) &ut.ut_tv.tv_sec) == -1)
        errExit("time");
    ut.ut_pid = getpid();
    
    setutxent();                    
    updwtmpx(_PATH_WTMP, &ut);
    endutxent();
}


int
main(int argc, char *argv[])
{
    /* Read options */
    if (argc < 4 || strcmp(argv[1], "--help") == 0)
        usageErr("%s line name host\n", argv[0]);
    my_logwtmp(argv[1], argv[2], argv[3]);
    exit(EXIT_SUCCESS);
}
