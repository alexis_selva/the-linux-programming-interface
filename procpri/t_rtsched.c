/* Exercice 35-2 */

#include <sys/time.h>
#include <sys/resource.h>
#include <ctype.h>
#include <unistd.h>
#include <sched.h>
#include "tlpi_hdr.h"

static void
usageError(char *progName, char *msg)
{
    if (msg != NULL) fprintf(stderr, "%s\n", msg);
    fprintf(stderr, "Usage: %s policy priority command arg\n", progName);
    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
    int policy;
    struct sched_param param;

    /* Read options */
    if (argc < 4) {
        usageError(argv[0], "Missing argument");
    } else if (strcmp(argv[1], "--help") == 0) {
        usageError(argv[0], NULL);
    }
    policy = (argv[1][0] == 'r') ? SCHED_RR : (argv[1][0] == 'f') ? SCHED_FIFO : SCHED_OTHER;
    param.sched_priority = getInt(argv[2], 0, "priority");
    
    /* Verify priority is in the range */
    int min_param = sched_get_priority_min(policy);
    if (min_param == -1) {
        errExit("sched_get_priority_min");
    }
    int max_param = sched_get_priority_max(policy);
    if (max_param == -1) {
        errExit("sched_get_priority_min");
    }
    if ((param.sched_priority < min_param) || (param.sched_priority > max_param)) {
        usageError(argv[0], "Invalid priority");
    }

    /* Set Scheduling priority */
    if (sched_setscheduler(0, policy, &param) == -1) {
        errExit("sched_setscheduler");
    }

    /* Drop set-root-ID privileges */
    setuid(getuid());

    /* Run command */
    execvp(argv[3], &argv[3]);
        
    /* Error */
    errExit("execv");
}
