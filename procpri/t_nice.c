/* Exercice 35-1 */

#include <sys/time.h>
#include <sys/resource.h>
#include <ctype.h>
#include <unistd.h>
#include "tlpi_hdr.h"

static void
usageError(char *progName, char *msg)
{
    if (msg != NULL) fprintf(stderr, "%s\n", msg);
    fprintf(stderr, "Usage: %s [OPTIONS] [COMMAND] [ARG]\n", progName);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t -h: display help and exit\n");
    fprintf(stderr, "\t -n N: add integer N to niceness\n");
    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
    int opt;
    char *pstr = NULL;
    int prio;

    /* Read options */
    while ((opt = getopt(argc, argv, ":n:h")) != -1) {
        switch (opt) {
            case 'n': pstr = optarg; break;
            case 'h': usageError(argv[0], NULL);
            case ':': usageError(argv[0], "Missing argument");
            case '?': usageError(argv[0], "Unrecognized option");
            default: fatal("Unexpected case in switch()");
        }
    }

    if (pstr == NULL) {
        /* Get current niceness */
        prio = getpriority(PRIO_USER, 0);
        if (prio == -1 && errno != 0)
            errExit("getpriority");
        printf("Nice value = %d\n", prio);
    } else {
        /* Set niceness */
        prio = getInt(pstr, 0, "prio");
        if (setpriority(PRIO_PROCESS, 0, prio) == -1)
            errExit("setpriority");
    }
    
    if (optind < argc) {
        
        /* Run command */
        execvp(argv[optind], &argv[optind]);
        
        /* Error */
        errExit("execv");
    }
    
    exit(EXIT_SUCCESS);
}
