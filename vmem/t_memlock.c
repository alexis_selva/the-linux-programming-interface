/* Exercice 50-1 */

#define _BSD_SOURCE             /* MAP_ANONYMOUS definition from <sys/mman.h> */
#include <sys/mman.h>
#include <sys/resource.h>
#include "tlpi_hdr.h"
#include "print_rlimit.h"   	         /* Declaration of printRlimit() */

int
main(int argc, char *argv[])
{
	long pageSize;
	struct rlimit rl;
    char *addr;
    size_t len, lockLen;

    if (argc != 5 || strcmp(argv[1], "--help") == 0)
        usageErr("%s soft-limit hard-limit num-pages lock-page-len\n", argv[0]);
        
#ifndef _SC_PAGESIZE
    pageSize = getpagesize();
    if (pageSize == -1)
        errExit("getpagesize");
#else
    pageSize = sysconf(_SC_PAGESIZE);
    if (pageSize == -1)
        errExit("sysconf(_SC_PAGESIZE)");
#endif
        
    rl.rlim_cur = getInt(argv[1], GN_GT_0, "soft-limit") * pageSize;
    rl.rlim_max = getInt(argv[2], GN_GT_0, "hard-limit") * pageSize;
    len         = getInt(argv[3], GN_GT_0, "num-pages") * pageSize;
    lockLen     = getInt(argv[4], GN_GT_0, "lock-page-len") * pageSize;

    printRlimit("Initial maximum process limits: ", RLIMIT_MEMLOCK);

    if (setrlimit(RLIMIT_MEMLOCK, &rl) == -1)
        errExit("setrlimit");

    printRlimit("New maximum process limits:     ", RLIMIT_MEMLOCK);

    addr = mmap(NULL, len, PROT_READ, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (addr == MAP_FAILED)
        errExit("mmap");

    printf("Allocated %ld (%#lx) bytes starting at %p\n",
            (long) len, (unsigned long) len, addr);
            
    if (mlock(addr, lockLen) == -1)
    	errExit("mlock");
    
    printf("Locked %ld (%#lx) bytes starting at %p\n",
            (long) lockLen, (unsigned long) lockLen, addr);

    exit(EXIT_SUCCESS);
}
