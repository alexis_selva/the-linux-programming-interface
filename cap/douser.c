/* Exercice 39-2 
 * cmd:
 *  # sudo chown root:root ./douser
 *  # sudo chmod +s ./douser
 *  # ./douser -u <user> sleep 10
 */

#define _BSD_SOURCE
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE
#endif
#include <sys/capability.h>
#include <unistd.h>
#include <limits.h>
#include <pwd.h>
#include <shadow.h>
#include "tlpi_hdr.h"

static void
usageError(char *progName, char *msg)
{
    if (msg != NULL) fprintf(stderr, "%s\n", msg);
    fprintf(stderr, "Usage: %s [-u user] program [arg]\n", progName);
    fprintf(stderr, "    -u   run program by user\n");
    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
    char *username = "root";
    char *password, *encrypted, *p;
    struct passwd *pwd;
    struct spwd *spwd;
    Boolean authOk;
    int opt;
    size_t len;
    
    /* Read options */
    while ((opt = getopt(argc, argv, ":u:")) != -1) {
        switch (opt) {
            case 'u': username = optarg; break;
            case 'h': usageError(argv[0], NULL);
            case ':': usageError(argv[0], "Missing argument");
            case '?': usageError(argv[0], "Unrecognized option");
            default: fatal("Unexpected case in switch()");
        }
    }
    
    /* Purify username */
    len = strlen(username);
    if (username[len - 1] == '\n')
        username[len - 1] = '\0';  

    /* Look up password record for username */
    pwd = getpwnam(username);
    if (pwd == NULL)
        fatal("couldn't get password record");

    /* Look up shadow password record for username */
    spwd = getspnam(username);
    if (spwd == NULL && errno == EACCES)
        fatal("no permission to read shadow password file");

    /* Use the possible shadow password */
    if (spwd != NULL) 
        pwd->pw_passwd = spwd->sp_pwdp;     

    /* Request the password */
    password = getpass("Password: ");
    
    /* Encrypt password and erase cleartext version immediately */
    encrypted = crypt(password, pwd->pw_passwd);
    for (p = password; *p != '\0'; )
        *p++ = '\0';
    if (encrypted == NULL)
        errExit("crypt");

    /* Authentify the user */
    authOk = strcmp(encrypted, pwd->pw_passwd) == 0;
    if (!authOk) {
        printf("Incorrect password\n");
        exit(EXIT_FAILURE);
    }

    /* Drop permantently privileges */
    if (setuid(pwd->pw_uid) < 0) {
        errExit("setuid");
    }
    if (setgid(pwd->pw_gid) < 0) {
        errExit("setuid");
    }
    
    /* Now do authenticated work... */
    if (optind < argc) {
        
        /* Run command */
        execvp(argv[optind], &argv[optind]);
        
        /* Error */
        errExit("execv");
    }

    exit(EXIT_SUCCESS);
}
