/* Solution for Exercise 36-3 */

#define _GNU_SOURCE
#include <sched.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/times.h>
#include "tlpi_hdr.h"

#define CSEC_STEP 25            

static void
useCPU(int when)
{
    struct tms tms;
    int cpuCentisecs, prevStep, prevSec;

    prevStep = 0;
    prevSec = 0;
    for (;;) {
        if (times(&tms) == -1)
            errExit("times");
        cpuCentisecs = (tms.tms_utime + tms.tms_stime) * 100 / sysconf(_SC_CLK_TCK);
        if (cpuCentisecs >= prevStep + CSEC_STEP) {
            prevStep += CSEC_STEP;
        } else if (cpuCentisecs > when) {
            struct rlimit rlim;
            if (getrlimit(RLIMIT_CPU, &rlim) == -1)
                errExit("getrlimit");
            rlim.rlim_cur = 1;
            if (setrlimit(RLIMIT_CPU, &rlim) == -1)
                errExit("setrlimit");
        } else if (cpuCentisecs >= prevSec + 100) {
            prevSec = cpuCentisecs;
        }
    }
}


int
main(int argc, char *argv[])
{
    
    setbuf(stdout, NULL);
    useCPU(100);
}
