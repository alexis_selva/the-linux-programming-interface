/* Exercice 26-6 */

#include <signal.h>
#include <sys/wait.h>
#include "print_wait_status.h"
#include "curr_time.h"
#include "tlpi_hdr.h"

static void
sigchldHandler(int sig)
{
    pid_t childPid;
    int status;

    printf("%s handler: Caught SIGCHLD\n", currTime("%T"));
    childPid = waitpid(-1, &status, 0);
    if (childPid > 0) {
        printf("%s handler: Reaped child %ld - ", currTime("%T"), (long) childPid);
        printWaitStatus(NULL, status);
    } else if (childPid == -1) {
        if (errno == ECHILD) {
            printf("%s handler: Unable to reap child\n", currTime("%T"));
        } else {
            errMsg("waitpid");
        }
    }
}

int
main(int argc, char *argv[])
{
    sigset_t blockMask, previousMask;
    struct sigaction sa;
    pid_t childPid;
    int status;

    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = sigchldHandler;
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
        errExit("sigaction");

    sigemptyset(&blockMask);
    sigaddset(&blockMask, SIGCHLD);
    if (sigprocmask(SIG_SETMASK, &blockMask, &previousMask) == -1)
        errExit("sigprocmask");

    childPid = fork();
    switch (childPid) {
    case -1:
        errExit("fork");

    case 0:
        printf("%s main: Child (PID=%ld) exiting\n", currTime("%T"), (long) getpid());
        _exit(EXIT_SUCCESS);
        
    default:
        if (waitpid(childPid, &status, 0) < 0)
            errExit("waitpid");
        printf("%s main: Reaped child (PID=%ld) - ", currTime("%T"), (long) childPid);
        printWaitStatus(NULL, status);
        break;
    }

    if (sigprocmask(SIG_SETMASK, &previousMask, NULL) == -1)
        errExit("sigprocmask");
    
    exit(EXIT_SUCCESS);
}
