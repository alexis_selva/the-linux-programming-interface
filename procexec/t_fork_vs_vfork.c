/* Exercice 28-1 */

#define _BSD_SOURCE

#include <sys/wait.h>
#include "tlpi_hdr.h"

#define MAX_RUNS 100000

static inline void 
create_child(int call_vfork) 
{
    pid_t child;
    
    if (call_vfork) {
        child = vfork();
    } else {
        child = fork();
    }
    switch (child) {
    case -1:
        errExit("vfork");

    case 0:
        _exit(EXIT_SUCCESS);

    default:
        wait(NULL);
    }
}

int
main(int argc, char *argv[])
{
    size_t run = 0;
    
    for ( ; run < MAX_RUNS ; run++) {
        create_child(argc > 1);
    }
    
    exit(EXIT_SUCCESS);
}
