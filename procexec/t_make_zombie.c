/* Exercice 26-4 */

#include <signal.h>
#include <libgen.h>             /* For basename() declaration */
#include "tlpi_hdr.h"

#define CMD_SIZE 200

static void             /* Signal handler - does nothing but return */
handler(int sig)
{
}

int
main(int argc, char *argv[])
{
    char cmd[CMD_SIZE];
    pid_t childPid;
    sigset_t blockMask, origMask, emptyMask;
    struct sigaction sa;
       
    /* Disable buffering of stdout */
    setbuf(stdout, NULL);

    /* SIGCHLD handler (ignored by default) */
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;    
    sa.sa_handler = handler;
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Block signal */
    sigemptyset(&blockMask);
    sigaddset(&blockMask, SIGCHLD);    
    if (sigprocmask(SIG_BLOCK, &blockMask, &origMask) == -1)
        errExit("sigprocmask");
        
    printf("Parent PID=%ld\n", (long) getpid());

    switch (childPid = fork()) {
    case -1:
        errExit("fork");

    case 0:
        /* Synchronize the parent and its child */
        printf("Child (PID=%ld) exiting\n", (long) getpid());
        _exit(EXIT_SUCCESS);

    default:
        /* Synchronize the parent and its child */
        sigemptyset(&emptyMask);
        if (sigsuspend(&emptyMask) == -1 && errno != EINTR) 
            errExit("sigsuspend");
        snprintf(cmd, CMD_SIZE, "ps | grep %s", basename(argv[0]));
        
        /* View zombie child again */
        system(cmd);            
        
        /* Now send the "sure kill" signal to the zombie */
        if (kill(childPid, SIGKILL) == -1)
            errMsg("kill");
        
        /* Give child a chance to react to signal */
        sleep(3);               
        printf("After sending SIGKILL to zombie (PID=%ld):\n", (long) childPid);
        
        /* View zombie child again */
        system(cmd);            

        exit(EXIT_SUCCESS);
    }
}
