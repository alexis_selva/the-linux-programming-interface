/* Exercice Z6-2 */

#include <sys/types.h> 
#include <sys/wait.h>
#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
    pid_t ppid, w;
    int status;

    setbuf(stdout, NULL);

    switch (fork()) {
    case -1:
        errExit("fork 1");

    case 0:             
        switch (fork()) {
        case -1:
            errExit("fork 2");
            
        case 0:         /* Grand child */
            printf("Grand child running (PID=%ld PPID=%ld)\n", (long) getpid(), (long) getppid());
            while ((ppid = getppid()) != 1) {
            }
            printf("Grand child is orphaned (PID=%ld PPID=%ld)\n", (long) getpid(), (long) getppid());
            _exit(EXIT_SUCCESS);
    
        default:        /* Parent */
            printf("Parent sleeping (PID=%ld PPID=%ld)\n", (long) getpid(), (long) getppid());
            sleep(1);
            printf("Parent exiting (PID=%ld PPID=%ld)\n", (long) getpid(), (long) getppid());
            _exit(EXIT_SUCCESS);
        }
        
    default:            /* Grand parent */
        printf("Grand parent (PID=%ld) waiting\n", (long) getpid());
        do {
            w = waitpid(-1, &status, WNOHANG);
            if (w == -1) {
                errExit("waitpid");
            }
        } while (w == 0);
        printf("Grand parent (PID=%ld) sleeping\n", (long) getpid());
        sleep(2);
        printf("Grand parent exiting\n");
        exit(EXIT_SUCCESS);
    }
}
