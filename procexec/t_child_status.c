/* Exercice 26-3 */

#include <sys/wait.h>
#include "tlpi_hdr.h"

static void
printWaitStatus(const char *msg, siginfo_t *info)
{
    if (msg != NULL)
        printf("%s", msg);

    switch(info->si_code) {
    
    case CLD_EXITED:
        printf("child exited, status=%d\n", info->si_status);
        break;
        
    case CLD_KILLED:
        printf("child killed by signal %d (%s)\n",
                info->si_status, strsignal(info->si_status));
        break;
        
    case CLD_DUMPED:
        printf("child killed by signal %d (%s) (core dumped)\n",
                info->si_status, strsignal(info->si_status));
        break;
        
    case CLD_STOPPED:
        printf("child stopped by signal %d (%s)\n",
                info->si_status, strsignal(info->si_status));
        break;
        
    case CLD_CONTINUED:
        printf("child continued\n");
        break;
        
    default:            /* Should never happen */
        printf("what happened to this child? (status=%x)\n",
                (unsigned int) info->si_status);
        break;
    }
}

int
main(int argc, char *argv[])
{
    pid_t childPid;
    siginfo_t info;

    if (argc > 1 && strcmp(argv[1], "--help") == 0)
        usageErr("%s [exit-status]\n", argv[0]);

    switch (fork()) {
    case -1: errExit("fork");

    case 0:             /* Child: either exits immediately with given
                           status or loops waiting for signals */
        printf("Child started with PID = %ld\n", (long) getpid());
        if (argc > 1)                   /* Status supplied on command line? */
            exit(getInt(argv[1], 0, "exit-status"));
        else                            /* Otherwise, wait for signals */
            for (;;)
                pause();
        exit(EXIT_FAILURE);             /* Not reached, but good practice */

    default:            /* Parent: repeatedly wait on child until it
                           either exits or is terminated by a signal */
        for (;;) {
            memset(&info, 0, sizeof(siginfo_t));
            if (waitid(P_ALL, 0, &info, WEXITED | WSTOPPED| WCONTINUED) == -1) errExit("waitid");

            /* Print status in hex, and as separate decimal bytes */
            childPid = info.si_pid;
            printf("waitid() returned: PID=%ld; status=%d; code=%d\n",
                    (long) childPid,
                    (unsigned int) info.si_status, info.si_code);
            printWaitStatus(NULL, &info);

            if (info.si_code == CLD_EXITED || info.si_code == CLD_KILLED || info.si_code == CLD_DUMPED)
                exit(EXIT_SUCCESS);
        }
    }
}
