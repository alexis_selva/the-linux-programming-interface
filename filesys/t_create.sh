#!/bin/bash

# Exercice 14.1

nb=$1
order=$2

# Create file xNNNNNN
mkdir test
if [ ! "$order" ]
then
    # In random order
    for i in $(shuf -i 1-$nb)
    do
        echo "1" > test/x$(printf %06d $i)
    done
else
    # In increasing order
    for i in $(seq 1 1 $nb)
    do
        echo "1" > test/x$(printf %06d $i)
    done
fi

# Remove in increasing order
for i in $(seq 1 1 $nb)
do
    rm test/x$(printf %06d $i)
done
rmdir test

exit 0
