/* Exercice 63-2 */

#include "t_echo.h"

int
main(int argc, char *argv[])
{
    int sfd, j;
    size_t len;
    ssize_t numRead;
    char buf[BUF_SIZE];
    int type;

    if (argc < 3 || strcmp(argv[1], "--help") == 0)
        usageErr("%s host protocol msg...\n", argv[0]);
        
    /* Determine socket type */
    if (strncmp(argv[2], "tcp", sizeof("tcp") - 1) == 0) {
    	type = SOCK_STREAM;
    } else if (strncmp(argv[2], "udp", sizeof("udp") - 1) == 0) {
    	type = SOCK_DGRAM;
    } else {
    	usageErr("%s invalid protocol (%s)\n", argv[0], argv[2]);
    }

    /* Construct server address from first command-line argument */
    sfd = inetConnect(argv[1], SERVICE, type);
    if (sfd == -1)
        fatal("Could not connect to server socket");

    /* Send remaining command-line arguments to server as separate datagrams */
    for (j = 3; j < argc; j++) {
        len = strlen(argv[j]);
        if (write(sfd, argv[j], len) != len)
            fatal("partial/failed write");

        numRead = read(sfd, buf, BUF_SIZE);
        if (numRead == -1)
            errExit("read");

        printf("[%ld bytes] %.*s\n", (long) numRead, (int) numRead, buf);
    }

    exit(EXIT_SUCCESS);
}
