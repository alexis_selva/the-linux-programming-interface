/* Exercice 63-2 */

#include <sys/epoll.h>
#include <fcntl.h>
#include <syslog.h>
#include "t_echo.h"
#include "become_daemon.h"

#define MAX_EVENTS     5

int
main(int argc, char *argv[])
{
    int epoll_sfd, ready, cfd, udp_sfd, tcp_sfd, j;
    struct epoll_event ev;
    struct epoll_event evlist[MAX_EVENTS];
    ssize_t numRead;
    char buf[BUF_SIZE];
    socklen_t len;
    struct sockaddr_storage claddr;

    if (becomeDaemon(0) == -1)
        errExit("becomeDaemon");

    epoll_sfd = epoll_create(2);
    if (epoll_sfd == -1)
        syslog(LOG_ERR, "Could not create epoll file descriptor (%s)", strerror(errno));
       
    udp_sfd = inetBind(SERVICE, SOCK_DGRAM, NULL);
    if (udp_sfd == -1) {
        syslog(LOG_ERR, "Could not create UDP server socket (%s)", strerror(errno));
        exit(EXIT_FAILURE);
    }

	ev.events = EPOLLIN;
    ev.data.fd = udp_sfd;
    if (epoll_ctl(epoll_sfd, EPOLL_CTL_ADD, udp_sfd, &ev) == -1)
        syslog(LOG_ERR, "Could not add UDP server socket to epoll file descriptor (%s)", strerror(errno));

    tcp_sfd = inetListen(SERVICE, 10, NULL);
    if (tcp_sfd == -1) {
        syslog(LOG_ERR, "Could not create TCP server socket (%s)", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    ev.events = EPOLLIN;
    ev.data.fd = tcp_sfd;
    if (epoll_ctl(epoll_sfd, EPOLL_CTL_ADD, tcp_sfd, &ev) == -1) {
        syslog(LOG_ERR, "Could not add TCP server socket to epoll file descriptor (%s)", strerror(errno));
        exit(EXIT_FAILURE);
	}
	
    /* Receive datagrams/segments and return copies to senders */
    for (;;) {
    	ready = epoll_wait(epoll_sfd, evlist, MAX_EVENTS, -1);
        if (ready == -1) {
            if (errno == EINTR)
                continue;               /* Restart if interrupted by signal */
            else
                errExit("epoll_wait");
        }
        
        for (j = 0; j < ready; j++) {
        	if (evlist[j].events & EPOLLIN) {
        		if (evlist[j].data.fd == tcp_sfd) {
        			cfd = accept(tcp_sfd, NULL, NULL);  /* Wait for connection */
        			if (cfd == -1) {
            			syslog(LOG_ERR, "Failure in accept(): %s", strerror(errno));
            			exit(EXIT_FAILURE);
        			}
        			numRead = read(cfd, buf, BUF_SIZE);
        			if (numRead == -1)
        				errExit("read");
        			if (write(cfd, buf, numRead) != numRead)
            			syslog(LOG_WARNING, "Error echoing response (%s)", strerror(errno));
            		if (close(cfd)) {
						syslog(LOG_WARNING, "Error closing client socket (%s)", strerror(errno));
            		}
        		} else if (evlist[j].data.fd == udp_sfd) {
        			len = sizeof(struct sockaddr_storage);
        			numRead = recvfrom(udp_sfd, buf, BUF_SIZE, 0, (struct sockaddr *) &claddr, &len);
        			if (numRead == -1)
            			errExit("recvfrom");
			        if (sendto(udp_sfd, buf, numRead, 0, (struct sockaddr *) &claddr, len) != numRead)
            			syslog(LOG_WARNING, "Error echoing response to (%s)", strerror(errno));
                } else {
        			syslog(LOG_ERR, "Unknown socket");
        			exit(EXIT_FAILURE);
        		}

    		}
        }
    }
}
