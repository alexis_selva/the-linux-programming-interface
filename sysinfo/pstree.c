/* Exercise 12-2 */

#define _GNU_SOURCE
#include <limits.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include "ugid_functions.h"
#include "tlpi_hdr.h"

#define MAX_LINE 1000

typedef struct snapshot_t {
    char *name;
    pid_t pid;
    struct snapshot_t *parent;
    struct snapshot_t *child;
    struct snapshot_t *next;
} snapshot_t;

static void
print_process(snapshot_t *process, char is_child, char index, char buffer[]) {
    if (process) {
        printf("%*.s+-%s", is_child ? 0 : index, buffer, process->name);
        if (process->child) {
            printf("-");
            print_process(process->child, 1, index, buffer);
        }
        if (process->next) {
            printf("\n");
            print_process(process->next, 0, is_child ? index + strlen(process->parent->name) + 3 : index, buffer);
        }
    }
}

int
main(int argc, char *argv[])
{
    DIR *dirp;
    struct dirent *dp;
    char path[PATH_MAX];
    char line[MAX_LINE], name[MAX_LINE];
    FILE *fp;
    char *p;
    pid_t pid, ppid;
    Boolean gotName, gotPid, gotPpid;

    fp = fopen("/proc/sys/kernel/pid_max", "r");
    if (fp == NULL)
        errExit("fopen");
    if (fgets(line, MAX_LINE, fp) == NULL)
        errExit("fgets");
    size_t process_max = strtol(line, NULL, 10);
    fclose(fp);
    
    dirp = opendir("/proc");
    if (dirp == NULL)
        errExit("opendir");

    /* Scan entries under /proc directory */
    snapshot_t *snapshots[process_max];
    for (;;) {
        errno = 0;              /* To distinguish error from end-of-directory */
        dp = readdir(dirp);
        if (dp == NULL) {
            if (errno != 0)
                errExit("readdir");
            else
                break;
        }

        /* Skip entries that are not directories, or don't begin with a digit. */
        if (dp->d_type != DT_DIR || !isdigit((unsigned char) dp->d_name[0]))
            continue;

        snprintf(path, PATH_MAX, "/proc/%s/status", dp->d_name);
        fp = fopen(path, "r");
        if (fp == NULL)
            continue;

        gotName = FALSE;
        gotPid = FALSE;
        gotPpid = FALSE;
        while (!gotName || !gotPid || !gotPpid) {
            if (fgets(line, MAX_LINE, fp) == NULL)
                break;
            if (strncmp(line, "Name:", 5) == 0) {
                for (p = line + 5; p != '\0' && isspace((unsigned char) *p); ) {
                    p++;
                }
                strncpy(name, p, strlen(p) - 1);
                name[strlen(p) - 1] = '\0';        /* Ensure null-terminated */
                gotName = TRUE;
            } else if (strncmp(line, "Pid:", 4) == 0) {
                pid = strtol(line + 4, NULL, 10);
                gotPid = TRUE;
            } else if (strncmp(line, "PPid:", 5) == 0) {
                ppid = strtol(line + 5, NULL, 10);
                gotPpid = TRUE;
            }
        }
        fclose(fp);

        /* Store PID and orocess name */
        if (gotName && gotPid && gotPpid) {
            if (snapshots[pid] == NULL) {
                snapshots[pid] = malloc(sizeof(snapshot_t));
                bzero(snapshots[pid], sizeof(snapshot_t));
            }
            if (snapshots[ppid] == NULL) {
                snapshots[ppid] = malloc(sizeof(snapshot_t));
                bzero(snapshots[ppid], sizeof(snapshot_t));
            } else {   
                snapshots[pid]->next = snapshots[ppid]->child;
            }
            snapshots[pid]->name = strdup(name);
            snapshots[pid]->parent = snapshots[ppid];
            snapshots[ppid]->child = snapshots[pid];
        }
    }
    
    char index = 0;
    char buffer[128];
    memset(buffer, ' ', 128);
    print_process(snapshots[1], 0, index, buffer);

    exit(EXIT_SUCCESS);
}
