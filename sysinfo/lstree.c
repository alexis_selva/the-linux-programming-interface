/* Exercise 12-2 */

#define _GNU_SOURCE
#include <limits.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include "ugid_functions.h"
#include "tlpi_hdr.h"

#define MAX_LINE 1000

int
main(int argc, char *argv[])
{
    DIR *dirp;
    DIR *dirp_fd;
    struct dirent *dp;
    struct dirent *dp_fd;
    char path[PATH_MAX];
    char line[MAX_LINE], name[MAX_LINE];
    FILE *fp;
    char *p;
    Boolean gotName;
    
    dirp = opendir("/proc");
    if (dirp == NULL)
        errExit("opendir");

    /* Scan entries under /proc directory */
    for (;;) {
        errno = 0;              /* To distinguish error from end-of-directory */
        dp = readdir(dirp);
        if (dp == NULL) {
            if (errno != 0)
                errExit("readdir");
            else
                break;
        }

        /* Skip entries that are not directories, or don't begin with a digit. */
        if (dp->d_type != DT_DIR || !isdigit((unsigned char) dp->d_name[0]))
            continue;
            
        snprintf(path, PATH_MAX, "/proc/%s/status", dp->d_name);
        fp = fopen(path, "r");
        if (fp == NULL)
            continue;

        gotName = FALSE;
        while (!gotName) {
            if (fgets(line, MAX_LINE, fp) == NULL)
                break;
            if (strncmp(line, "Name:", 5) == 0) {
                for (p = line + 5; p != '\0' && isspace((unsigned char) *p); ) {
                    p++;
                }
                strncpy(name, p, strlen(p) - 1);
                name[strlen(p) - 1] = '\0';        /* Ensure null-terminated */
                gotName = TRUE;
            }
        }
        fclose(fp);
        printf("%s:\n", name);

        snprintf(path, PATH_MAX, "/proc/%s/fd", dp->d_name);
        dirp_fd = opendir(path);
        if (dirp_fd == NULL)
            errExit("opendir");
        
        /* Scan entries under /proc directory */
        for (;;) {
            errno = 0;              /* To distinguish error from end-of-directory */
            dp_fd = readdir(dirp_fd);
            if (dp_fd == NULL) {
                if (errno != 0)
                    errExit("readdir");
                else
                    break;
            }
            
            /* Skip entries that are not directories, or don't begin with a digit. */
            if (dp->d_type != DT_DIR || !isdigit((unsigned char) dp_fd->d_name[0]))
                continue;
            
            snprintf(path, PATH_MAX, "/proc/%s/fd/%s", dp->d_name, dp_fd->d_name);
            char buffer[MAX_LINE];
            ssize_t length = readlink(path, buffer, MAX_LINE);
            if (length == -1) 
                errExit("readlink");
            buffer[length] = '\0';
            printf("+%s\n", buffer);
        }
        closedir(dirp_fd);
        printf("\n");
    }
    closedir(dirp);
    
    exit(EXIT_SUCCESS);
}
