/* Exercice 16.1 */
#include <sys/xattr.h>
#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
    char *type;
    char *value;

    if (argc < 4 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file user.type value\n", argv[0]);

    type = argv[2];
    value = argv[3];
    if (setxattr(argv[1], type, value, strlen(value), 0) == -1)
        errExit("setxattr");
        
    exit(EXIT_SUCCESS);
}
