/* Exercice 17-1 */

#include <acl/libacl.h>
#include <sys/acl.h>
#include "ugid_functions.h"
#include "tlpi_hdr.h"

static void
usageError(char *progName, char *msg)
{
    fprintf(stderr, "Usage: %s -t tag -n name filename\n", progName);
    fprintf(stderr, "%s\n", msg);
    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
    acl_t acl;
    acl_entry_t entry;
    acl_tag_t requested_tag, tag;
    uid_t *qualifier;
    acl_permset_t permset;
    char *tag_ptr = NULL;
    char *requested_name = NULL;
    char *name = NULL;
    int entryId, permVal, opt;
    int mask_permval[3];

    /* Read the option */
    while ((opt = getopt(argc, argv, "t:n:")) != -1) {
        switch (opt) {
            case 't': tag_ptr = optarg; break;
            case 'n': requested_name = optarg; break;
            case ':': usageError(argv[0], "Missing argument");
            case '?': usageError(argv[0], "Unrecognized option");
            default: fatal("Unexpected case in switch()");
        }
    }

    /* Analyse the options */
    if (optind + 1 != argc)
        usageError(argv[0], "Invalid number of argument");
    if (tag_ptr == NULL)
        usageError(argv[0], "Missing tag value");
    if (tag_ptr[0] == 'u')
        requested_tag = ACL_USER;
    else if (tag_ptr[0] == 'g')
        requested_tag = ACL_GROUP;
    if (requested_name == NULL)
        usageError(argv[0], "Missing name value");
        
    /* Get the ACL metadata from the file */
    acl = acl_get_file(argv[optind], ACL_TYPE_ACCESS);
    if (acl == NULL)
        errExit("acl_get_file");

    /* Find ACL mask */
    for (entryId = ACL_FIRST_ENTRY ; ; entryId = ACL_NEXT_ENTRY) {
        
        /* Get the next entry */
        if (acl_get_entry(acl, entryId, &entry) != 1)
            break;                      

        /* Retrieve tag */
        if (acl_get_tag_type(entry, &tag) == -1)
            errExit("acl_get_tag_type");
        
        /* Is it the requested_tag ? */       
        if (tag == ACL_MASK) {
            if (acl_get_permset(entry, &permset) == -1)
                errExit("acl_get_permset");
            mask_permval[0] = acl_get_perm(permset, ACL_READ);
            if (mask_permval[0] == -1)
                errExit("acl_get_perm - ACL_READ");
            mask_permval[1] = acl_get_perm(permset, ACL_WRITE);
            if (mask_permval[1] == -1)
                errExit("acl_get_perm - ACL_WRITE");
            mask_permval[2] = acl_get_perm(permset, ACL_EXECUTE);
            if (mask_permval[2] == -1)
                errExit("acl_get_perm - ACL_EXECUTE");
            break;
        }
    }

    /* Walk through each entry in this ACL */
    for (entryId = ACL_FIRST_ENTRY ; ; entryId = ACL_NEXT_ENTRY) {
        
        /* Get the next entry */
        if (acl_get_entry(acl, entryId, &entry) != 1)
            break;                      

        /* Retrieve tag */
        if (acl_get_tag_type(entry, &tag) == -1)
            errExit("acl_get_tag_type");
        
        /* Is it the requested_tag ? */       
        if (tag == requested_tag) {

            /* Retrieve tag qualifier */
            qualifier = acl_get_qualifier(entry);
            if (qualifier == NULL)
                errExit("acl_get_qualifier");
            if (tag == ACL_USER) {
                name = userNameFromId(*qualifier);
            } else if (tag == ACL_GROUP) {
                name =  groupNameFromId(*qualifier);
            } else {
                continue;
            }
            
            /* Display permission */
            if ((name) && (strlen(name) == strlen(requested_name)) && (strcmp(name, requested_name) == 0)) {
                if (acl_get_permset(entry, &permset) == -1)
                    errExit("acl_get_permset");
                permVal = acl_get_perm(permset, ACL_READ);
                if (permVal == -1)
                    errExit("acl_get_perm - ACL_READ");
                printf("%c", ((permVal & mask_permval[0]) == 1) ? 'r' : '-');
                permVal = acl_get_perm(permset, ACL_WRITE);
                if (permVal == -1)
                    errExit("acl_get_perm - ACL_WRITE");
                printf("%c", ((permVal & mask_permval[1]) == 1) ? 'w' : '-');
                permVal = acl_get_perm(permset, ACL_EXECUTE);
                if (permVal == -1)
                    errExit("acl_get_perm - ACL_EXECUTE");
                printf("%c", ((permVal & mask_permval[2]) == 1) ? 'x' : '-');
                printf("\n");
            }
        }
    }
    
    /* Free ACL */
    if (acl_free(acl) == -1)
        errExit("acl_free");

    exit(EXIT_SUCCESS);
}
