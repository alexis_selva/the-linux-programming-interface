/* Solution for Exercise 45-2 */

#include <sys/stat.h>
#include "tlpi_hdr.h"

static inline key_t 
my_ftok(char *pathname, int proj)
{
    struct stat sb;
    key_t       key;
    
    /* Get file infos */
    if (stat(pathname, &sb) == -1 || proj == 0)
        return -1;
    
    /* Take the least 8 significant bits of proj */
    key = proj & 0XFF;
    key <<= 8;

    /* Take the least 8 significant bits of device number */    
    key |= sb.st_dev & 0XFF;
    key <<= 16;    

    /* Take the least 16 significant bits of i-node number */
    key |= sb.st_ino & 0XFFFF;

    return key;
}

int
main(int argc, char *argv[])
{
    key_t key;
    
    if (argc != 3 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file-name keychar\n", argv[0]);

    key = my_ftok(argv[1], argv[2][0]);
    if (key == -1)
        errExit("ftok");

    printf("Key = %lx\n", (unsigned long) key);
    
    exit(EXIT_SUCCESS);
}
