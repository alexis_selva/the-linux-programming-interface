/* Exercice 45-3 */

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include "tlpi_hdr.h"

static void
create_remove_queue(key_t key)
{
    int msqid = msgget(key, IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR | S_IWGRP);
    if (msqid == -1)
        errExit("msget(à failed to create new queue");
    printf("Queue created with id %d\n", msqid);
    if (msgctl(msqid, IPC_RMID, NULL) == -1) 
        errExit("msgget() failed to delete old queue");
}

int
main(void)
{
    key_t key = ftok("./Makefile", 1);
    if (key == -1)
        errExit("ftok");
 
    create_remove_queue(key);
    create_remove_queue(key);
    
    exit(EXIT_SUCCESS);
}
