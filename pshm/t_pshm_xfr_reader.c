/* Exercice 54-1 */

#include "t_pshm_xfr.h"

int
main(int argc, char *argv[])
{
    int fd, xfrs, bytes;
    struct shmseg *shmp;
    sem_t *sem_writer;
    sem_t *sem_reader;

    /* Get semaphore set and shared memory created by writer */
    sem_writer = sem_open("/writer", 0);
    if (sem_writer == SEM_FAILED)
        errExit("sem_open");
    sem_reader = sem_open("/reader", 0);
    if (sem_reader == SEM_FAILED)
        errExit("sem_open");
    fd = shm_open("/test", O_RDONLY, OBJ_PERMS);
    if (fd == -1)
        errExit("shm_open");

    /* Attach shared memory read-only, as we will only read */
    shmp = mmap(NULL, sizeof(struct shmseg), PROT_READ, MAP_SHARED, fd, 0);
    if (shmp == MAP_FAILED)
        errExit("mmap");
    if (close(fd) == -1)
    	errExit("close");

    /* Transfer blocks of data from shared memory to stdout */
    for (xfrs = 0, bytes = 0; ; xfrs++) {

		/* Wait for our turn */
		if (sem_wait(sem_reader) == -1)
            errExit("sem_wait");

        if (shmp->cnt == 0)                     /* Writer encountered EOF */
            break;
        bytes += shmp->cnt;

        if (write(STDOUT_FILENO, shmp->buf, shmp->cnt) != shmp->cnt)
            fatal("partial/failed write");
		
		/* Give writer a turn */
        if (sem_post(sem_writer) == -1)
            errExit("sem_post");
    }

    if (munmap(shmp, sizeof(struct shmseg)) == -1)
        errExit("munmap");

    /* Give writer one more turn, so it can clean up */
    if (sem_post(sem_writer) == -1)
        errExit("sem_post");

    fprintf(stderr, "Received %d bytes (%d xfrs)\n", bytes, xfrs);
    exit(EXIT_SUCCESS);
}
