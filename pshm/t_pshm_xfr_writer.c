/* Exercice 54-1 */

#include "t_pshm_xfr.h"

int
main(int argc, char *argv[])
{
    int fd, bytes, xfrs;
    struct shmseg *shmp;
    sem_t *sem_writer;
    sem_t *sem_reader;

    /* Create set containing two semaphores; initialize so that
       writer has first access to shared memory. */
    sem_writer = sem_open("/writer", O_CREAT | O_EXCL, OBJ_PERMS, 1);
    if (sem_writer == SEM_FAILED)
        errExit("sem_open");
    sem_reader = sem_open("/reader", O_CREAT | O_EXCL, OBJ_PERMS, 0);
    if (sem_reader == SEM_FAILED)
        errExit("sem_open");

    /* Create shared memory; attach at address chosen by system */
    fd = shm_open("/test", O_CREAT | O_EXCL | O_RDWR, OBJ_PERMS);
    if (fd == -1)
        errExit("shm_open");
    if (ftruncate(fd, sizeof(struct shmseg)) == -1)
        errExit("ftruncate");
        
    /* Map shared memory object */
    shmp = mmap(NULL, sizeof(struct shmseg), PROT_WRITE, MAP_SHARED, fd, 0);
    if (shmp == MAP_FAILED)
        errExit("mmap");
    if (close(fd) == -1)
    	errExit("close");

    /* Transfer blocks of data from stdin to shared memory */
    for (xfrs = 0, bytes = 0; ; xfrs++, bytes += shmp->cnt) {
    	
    	/* Wait for our turn */
    	if (sem_wait(sem_writer) == -1)
    		errExit("sem_wait");

        shmp->cnt = read(STDIN_FILENO, shmp->buf, BUF_SIZE);
        if (shmp->cnt == -1)
            errExit("read");

        /* Give reader a turn */
		if (sem_post(sem_reader) == -1)
            errExit("sem_post");

        /* Have we reached EOF? We test this after giving the reader
           a turn so that it can see the 0 value in shmp->cnt. */
        if (shmp->cnt == 0)
            break;
    }

    /* Wait until reader has let us have one more turn. We then know
       reader has finished, and so we can delete the IPC objects. */
    if (sem_wait(sem_writer) == -1)
    	errExit("sem_wait");

    if (sem_unlink("/writer") == -1)
        errExit("sem_unlink");
    if (sem_unlink("/reader") == -1)
        errExit("sem_unlink");
    if (munmap(shmp, sizeof(struct shmseg)) == -1)
        errExit("munmap");
    if (shm_unlink("/test") == -1)
        errExit("shm_unlink");

    fprintf(stderr, "Sent %d bytes (%d xfrs)\n", bytes, xfrs);
    exit(EXIT_SUCCESS);
}
