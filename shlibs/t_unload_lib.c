/* Exercice 42-1 */

#include <dlfcn.h>
#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
    void *libHandle1;
    void *libHandle2;
    void (*funcp)(void);
    const char *err;

    if (argc != 4 || strcmp(argv[1], "--help") == 0)
        usageErr("%s lib1-path lib2-path func2-name\n", argv[0]);

    /* Load the shared library 1 and get a handle for later use */
    libHandle1 = dlopen(argv[1], RTLD_NOW | RTLD_GLOBAL);
    if (libHandle1 == NULL)
        fatal("dlopen: %s", dlerror());

    /* Load the shared library 2 and get a handle for later use */
    libHandle2 = dlopen(argv[2], RTLD_NOW);
    if (libHandle2 == NULL)
        fatal("dlopen: %s", dlerror());

    /* Search library for symbol named in argv[3] */
    (void) dlerror();                           
#pragma GCC diagnostic ignored "-pedantic"
    funcp = (void (*)(void)) dlsym(libHandle2, argv[3]);
    err = dlerror();
    if (err != NULL)
        fatal("dlsym: %s", err);

    /* Call symbol */
    if (funcp == NULL)
        printf("%s is NULL\n", argv[3]);
    else
        (*funcp)();

     /* Close the libraries */
    dlclose(libHandle1);
    dlclose(libHandle2);
        
    exit(EXIT_SUCCESS);
}
