/* Exercice 42-1 */
#include <stdlib.h>
#include <stdio.h>

void foo2()
{
    printf("call %s %s\n", __FILE__, __FUNCTION__);
    foo1();
}

void __attribute__ ((constructor)) lib2_load(void)
{
    printf("call %s %s\n", __FILE__, __FUNCTION__);
}

void __attribute__ ((destructor)) lib2_unload(void)
{
    printf("call %s %s\n", __FILE__, __FUNCTION__);
}