#!/bin/sh

set -v

gcc -g -Wl,-allow-shlib-undefined -shared -fPIC -o libfoo1.so foo1.c

gcc -g -Wl,-allow-shlib-undefined -shared -fPIC -o ./libfoo2.so foo2.c -L. -lfoo1

LD_LIBRARY_PATH=. ../t_unload_lib libfoo1.so libfoo2.so foo2
