/* Exercice 42-1 */
#include <stdlib.h>
#include <stdio.h>

foo1(void)
{
    printf("call %s %s\n", __FILE__, __FUNCTION__);
}

void __attribute__ ((constructor)) lib1_load(void)
{
    printf("call %s %s\n", __FILE__, __FUNCTION__);
}

void __attribute__ ((destructor)) lib1_unload(void)
{
    printf("call %s %s\n", __FILE__, __FUNCTION__);
}