/* Exercice 9-3 */

#define _GNU_SOURCE
#include <unistd.h>
#include <sys/fsuid.h>
#include <limits.h>
#include <grp.h>
#include <pwd.h>

#include "ugid_functions.h"   /* userNameFromId() & groupNameFromId() */
#include "tlpi_hdr.h"

#define SG_SIZE (NGROUPS_MAX + 1)

static void
usageError(char *progName)
{
    fprintf(stderr, "Usage: sudo %s user\n", progName);
    exit(EXIT_FAILURE);
}

static inline int 
my_initgroups(const char *name, gid_t group) {
    gid_t suppGroups[SG_SIZE];
    int j = 0;
    struct group *grp;
    int len = strlen(name);
    while ((grp = getgrent()) != NULL) {
        char **user = grp->gr_mem; 
        for (user = grp->gr_mem ; *user ; user++) {
            if ((strlen(*user) == len) && (strcmp(*user, name) == 0))
                suppGroups[j++] = grp->gr_gid;
        }
    }
    endgrent();
    suppGroups[j++] = group;
    return setgroups(j, suppGroups);
}

int
main(int argc, char *argv[])
{
    gid_t rgid, egid, sgid;
    gid_t suppGroups[SG_SIZE];
    int numGroups, j;
    char *p;
    
    /* Verify command line */
    if (argc != 2) 
        usageError(argv[0]);

    /* Get effective group ID */
    if (getresgid(&rgid, &egid, &sgid) == -1)
        errExit("getresgid");
        
    /* Initialize Supplementary groups relative to user according to /etc/group */
    if (my_initgroups(argv[1], egid) == -1)
        errExit("initgroups");
        
    /* Get Supplementary groups */
    numGroups = getgroups(SG_SIZE, suppGroups);
    if (numGroups == -1)
        errExit("getgroups");
    printf("Supplementary groups (%d): ", numGroups);
    for (j = 0; j < numGroups; j++) {
        p = groupNameFromId(suppGroups[j]);
        printf("%s (%ld) ", (p == NULL) ? "???" : p, (long) suppGroups[j]);
    }
    printf("\n");

    exit(EXIT_SUCCESS);
}
