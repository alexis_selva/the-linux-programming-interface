/* Exercice 13-5 */
#define _GNU_SOURCE
#include <sys/stat.h>
#include <fcntl.h>
#include "tlpi_hdr.h"

#ifndef BUF_SIZE        /* Allow "cc -D" to override definition */
#define BUF_SIZE 1024
#endif

static int 
usageError(char *progName, char *msg, int opt)
{
    fprintf(stderr, "%s\n", msg);
    fprintf(stderr, "Usage: %s [-n] file\n", progName);
    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
    int inputFd, outputFd;
    ssize_t numRead;
    char buf[BUF_SIZE];
    int opt;
    int nb_lines = 10 + 1;

    /* Read options */
    while ((opt = getopt(argc, argv, ":n:")) != -1) {
        switch (opt) {
            case 'n': 
                nb_lines = atoi(optarg) + 1;
                break;
            case ':': 
                usageError(argv[0], "Missing argument", optopt);
            case '?': 
                usageError(argv[0], "Unrecognized option", optopt);
            default:  
                fatal("Unexpected case in switch()");
        }
    }

    /* Open input and output files */
    inputFd = open(argv[optind], O_RDONLY);
    if (inputFd == -1)
        errExit("opening file %s", argv[1]);
    outputFd = STDOUT_FILENO;
    
    /* Search the offset from where the transher shall start */
    off_t offset = lseek(inputFd, 0, SEEK_END);
    if (offset == -1)
        errExit("lseek: init");
    while (nb_lines) {
        if (offset < BUF_SIZE) {
            offset = lseek(inputFd, 0, SEEK_SET);
        } else {
            offset = lseek(inputFd, offset - BUF_SIZE, SEEK_SET);
        }
        if (offset == -1)
            errExit("lseek: search");
        numRead = read(inputFd, buf, BUF_SIZE);
        if (numRead == 0)
            break;
        else if (numRead == -1)
            errExit("read");
        
        char * ptr = buf + numRead;
        while (nb_lines && ptr) {
            nb_lines--;
            ptr = memrchr(buf, '\n', ptr - buf);
        }
        if (ptr) {
            offset += ptr - buf + 1;
        }
    }
    
    /* Transfer data until we encounter end of input or an error */
    offset = lseek(inputFd, offset, SEEK_SET);
    if (offset == -1)
        errExit("lseek: write");
    while ((numRead = read(inputFd, buf, BUF_SIZE)) > 0)
        if (write(outputFd, buf, numRead) != numRead)
            fatal("couldn't write whole buffer");
    if (numRead == -1)
        errExit("read");

    if (close(inputFd) == -1)
        errExit("close input");;

    exit(EXIT_SUCCESS);
}
