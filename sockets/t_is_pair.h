/* Exercise 59-4 */

#include <netinet/in.h>
#include <sys/socket.h>
#include <signal.h>
#include "inet_sockets.h"       /* Declares our socket functions */
#include "read_line.h"          /* Declaration of readLine() */
#include "tlpi_hdr.h"

#define PORT_NUM_STR "50000"    /* Port number for server */

#define STRING_LEN 30           /* Size of any string */
