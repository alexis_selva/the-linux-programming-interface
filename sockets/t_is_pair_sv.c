/* Exercise 59-4 */

#include "t_is_pair.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <sys/queue.h>

#define MAX_LINES   			(64)

typedef struct pair_ht_entry {
    SLIST_ENTRY(pair_ht_entry)	next;
    char                       *name;
    char                       *value;
} pair_ht_entry_t;

typedef struct pair_ht {
    SLIST_HEAD(, pair_ht_entry) head[MAX_LINES];
} pair_ht_t;

enum {
	MODE_ADD,
	MODE_DELETE,
	MODE_MODIFY,
	MODE_RETRIEVE,
	MODE_MAX,
};

static inline int
string_count_init(pair_ht_t *ht)
{
    int line = 0;

    for ( ; line < MAX_LINES ; line++) {
        SLIST_INIT(&ht->head[line]);
    }

    return 0;
}

static uint64_t 
murmur_hash(const char *data, unsigned long len)
{
    const uint64_t     m     = 0xc6a4a7935bd1e995;
    const int          r     = 47;
    const unsigned int seed  = 0x9747b28c;
    uint64_t           h     = seed ^ (len * m);
    const uint64_t    *lptr  = (const uint64_t *)data;
    unsigned int       lstep = len / sizeof(*lptr);

    while(lstep--) {
        uint64_t k = *lptr++;

        k *= m;
        k ^= k >> r;
        k *= m;

        h ^= k;
        h *= m;
    }

    data = (const char *)lptr;

    switch(len % sizeof(*lptr))
    {
        case 7: h ^= (uint64_t)(data[6]) << 48;
        case 6: h ^= (uint64_t)(data[5]) << 40;
        case 5: h ^= (uint64_t)(data[4]) << 32;
        case 4: h ^= (uint64_t)(data[3]) << 24;
        case 3: h ^= (uint64_t)(data[2]) << 16;
        case 2: h ^= (uint64_t)(data[1]) << 8;
        case 1: h ^= (uint64_t)(data[0]);
                h *= m;
    };

    h ^= h >> r;
    h *= m;
    h ^= h >> r;
    
    return h;
}

static int
determine_mode(char *m)
{
	if (strcmp(m, "add\n") == 0) {
		return MODE_ADD;
	}
	if (strcmp(m, "delete\n") == 0) {
		return MODE_DELETE;
	}
	if (strcmp(m, "modify\n") == 0) {
		return MODE_MODIFY;
	}
	if (strcmp(m, "retrieve\n") == 0) {
		return MODE_RETRIEVE;
	}
	return -1;
}

static char *
add_pair(pair_ht_t *ht, int line, char *n, char *v) 
{
	struct pair_ht_entry *phe  = NULL;
	
	SLIST_FOREACH(phe, &ht->head[line], next) {
		if (strcmp(phe->name, n) == 0) {
			fprintf(stderr, "Error when adding pair: already exist\n");
			return "error\n";
	    }
	}
	
	/* Create a new entry to memorize the current string */
	phe = malloc(sizeof(struct pair_ht_entry));
	if (phe == NULL) {
		fprintf(stderr, "Error when adding pair: malloc\n");
		return "error\n";
	}
	phe->name = strdup(n);
	phe->value = strdup(v);
	
	/* Insert the new entry into the hashtable */
	SLIST_INSERT_HEAD(&ht->head[line], phe, next);

	return phe->value;	
}

static char *
delete_pair(pair_ht_t *ht, int line, char *n, char *v) 
{
	struct pair_ht_entry *phe  = NULL;

	SLIST_FOREACH(phe, &ht->head[line], next) {
		if (strcmp(phe->name, n) == 0) {
			SLIST_REMOVE(&ht->head[line], phe, pair_ht_entry, next);
			free(phe->name);
			free(phe->value);
			free(phe);
			return "ok\n";
		}
	}
	
	return "error\n";
}

static char *
modify_pair(pair_ht_t *ht, int line, char *n, char *v) 
{
	struct pair_ht_entry *phe  = NULL;

	SLIST_FOREACH(phe, &ht->head[line], next) {
		if (strcmp(phe->name, n) == 0) {
			free(phe->value);
			phe->value = strdup(v);
			return phe->value;
		}
	}
	
	return "error\n";
}

static char *
retrieve_pair(pair_ht_t *ht, int line, char *n, char *v) 
{
	struct pair_ht_entry *phe  = NULL;

	SLIST_FOREACH(phe, &ht->head[line], next) {
		if (strcmp(phe->name, n) == 0) {
			return phe->value;
		}
	}
	
	return "error\n";
}

static char * 
handle_pair(pair_ht_t *ht, char *m, char *n, char *v)
{
    int line = 0;

    /* Determine the line where the current string shall be memorized */
    line = murmur_hash(n, strlen(n)) & (MAX_LINES - 1);
        
    switch (determine_mode(m)) {
    	case MODE_ADD:
    		return add_pair(ht, line, n, v);
		case MODE_DELETE:
    		return delete_pair(ht, line, n, v);
		case MODE_MODIFY:
    		return modify_pair(ht, line, n, v);
        case MODE_RETRIEVE:
        	return retrieve_pair(ht, line, n, v);
        default:
            fprintf(stderr, "Error when adding pair: unexpected mode (%s)\n", m);
			return "error\n";
    }
}

static int 
read_infos(int cfd, char *mode, char *name, char *value)
{
	/* Read client request: mode */
    if (readLine(cfd, mode, STRING_LEN) <= 0) {
		fprintf(stderr, "Error when reading mode\n");
		return -1;
    }

    /* Read client request: name */
    if (readLine(cfd, name, STRING_LEN) <= 0) {
		fprintf(stderr, "Error when reading name\n");
		return -1;
    }
        
    /* Read client request: value */
    if (readLine(cfd, value, STRING_LEN) <= 0) {
		fprintf(stderr, "Error when reading value\n");
		return -1;
	}
	
	return 0;
}

int
main(int argc, char *argv[])
{
	char mode[STRING_LEN];
    char name[STRING_LEN];
    char value[STRING_LEN];
    char *answer;
    struct sockaddr *claddr;
    int lfd, cfd;
    socklen_t addrlen, alen;
    char addrStr[IS_ADDR_STR_LEN];
    pair_ht_t pairs;

    /* Ignore the SIGPIPE signal, so that we find out about broken connection
       errors via a failure from write(). */
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
        errExit("signal");

    lfd = inetListen(PORT_NUM_STR, 5, &addrlen);
    if (lfd == -1)
        fatal("inetListen() failed");

    /* Allocate a buffer large enough to hold the client's socket address */
    claddr = malloc(addrlen);
    if (claddr == NULL)
        errExit("malloc");

	/* Handle clients iteratively */
    for (;;) {                  

        /* Accept a client connection, obtaining client's address */
        alen = addrlen;
        cfd = accept(lfd, (struct sockaddr *) claddr, &alen);
        if (cfd == -1) {
            errMsg("accept");
            continue;
        }

        printf("Connection from %s\n", inetAddressStr(claddr, alen,
                        addrStr, IS_ADDR_STR_LEN));
                    
        /* Read client infos */
		if (read_infos(cfd, mode, name, value)) {
			close(cfd);
            continue;                   /* Failed read; skip request */
        }
		
        /* Handle new pair request */
        answer = handle_pair(&pairs, mode, name, value);
        if (answer == NULL) {
            close(cfd);
            continue;                   /* Failed handle; skip request */
        }
        
        if (write(cfd, answer, strlen(answer)) != strlen(answer))
            fprintf(stderr, "Error on write");

		/* Close connection */
        if (close(cfd) == -1)           
            errMsg("close");
    }
}
