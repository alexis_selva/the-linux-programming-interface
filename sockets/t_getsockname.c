/* Exercice 61-4 */

#include "inet_sockets.h"               /* Declares our socket functions */
#include "tlpi_hdr.h"

static int
create_listening_socket(const char *service, int type, char do_bind, socklen_t *addrlen, int backlog)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd, s;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;
    hints.ai_socktype = type;
    hints.ai_family = AF_UNSPEC;
    hints.ai_flags = AI_PASSIVE;

    s = getaddrinfo(NULL, service, &hints, &result);
    if (s != 0)
        return -1;

    /* Create a listening socket */
    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);

        if (sfd != -1) {
			if (do_bind) {
				if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
					break;
				close(sfd);
			} else {
            	break;  
			}
        }
    }
    if (rp && listen(sfd, backlog) == -1) {
        close(sfd);
        freeaddrinfo(result);
        return -1;
    }
   	if (rp && addrlen)
    	*addrlen = rp->ai_addrlen;      
    freeaddrinfo(result);

    return (rp == NULL) ? -1 : sfd;
}

int
main(int argc, char *argv[])
{
    int listenFd;
    socklen_t len;
    void *addr;
    char addrStr[IS_ADDR_STR_LEN];

    /* Create listening socket, obtain size of address structure */
    listenFd = create_listening_socket("12345", SOCK_STREAM, 0, &len, 5);
    if (listenFd == -1)
        errExit("inetListen");
        
    addr = malloc(len);
    if (addr == NULL)
        errExit("malloc");

    if (getsockname(listenFd, addr, &len) == -1)
        errExit("getsockname");
    printf("getsockname(connFd):   %s\n",
            inetAddressStr(addr, len, addrStr, IS_ADDR_STR_LEN));

    exit(EXIT_SUCCESS);
}
