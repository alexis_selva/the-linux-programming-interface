/* Exercice 57-1 */

#define BUF_VAL		"noe "
#define BUF_SIZE_CL sizeof(BUF_VAL)
#include "us_xfr.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int
main(int argc, char *argv[])
{
    struct sockaddr_un addr;
    int sfd;
    ssize_t numRead;
    char buf[BUF_SIZE_CL] = BUF_VAL;

    sfd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);      /* Create client socket */
    if (sfd == -1)
        errExit("socket");

    /* Construct server address, and make the connection */

    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(&addr.sun_path[1], SV_SOCK_PATH, sizeof(addr.sun_path) - 2);

    if (connect(sfd, (struct sockaddr *) &addr,
                sizeof(struct sockaddr_un)) == -1)
        errExit("connect");

    /* Copy stdin to socket */

    while ( 1 )
        if (write(sfd, buf, BUF_SIZE_CL) != BUF_SIZE_CL)
            errExit("write");

    if (numRead == -1)
        errExit("read");

    exit(EXIT_SUCCESS);         /* Closes our socket; server sees EOF */
}
