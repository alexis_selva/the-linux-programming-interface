/* Exercice 61-5 */

#include "inet_sockets.h"
#include "tlpi_hdr.h"

#define SERVICE "55555"          /* Name of TCP service */
#define BUF_SIZE 100

int
main(int argc, char *argv[])
{
    int sfd;
    ssize_t numRead;
    char buf[BUF_SIZE];

    if (argc != 3 || strcmp(argv[1], "--help") == 0)
        usageErr("%s host command\n", argv[0]);

    sfd = inetConnect(argv[1], SERVICE, SOCK_STREAM);
    if (sfd == -1)
        errExit("inetConnect");

    switch (fork()) {
    case -1:
        errExit("fork");

    case 0:             /* Child: read server's response, echo on stdout */
        for (;;) {
            numRead = read(sfd, buf, BUF_SIZE);
            if (numRead <= 0)                   /* Exit on EOF or error */
                break;
            printf("%.*s", (int) numRead, buf);
        }
        exit(EXIT_SUCCESS);

    default:            /* Parent: write contents of stdin to socket */
    	numRead = strlen(argv[2]);
        if (write(sfd, argv[2], numRead) != numRead) {
        	fatal("write() failed");
        }

        /* Close writing channel, so server sees EOF */
        if (shutdown(sfd, SHUT_WR) == -1)
            errExit("shutdown");
        exit(EXIT_SUCCESS);
    }
}
