/* Exercise 59-4 */

#include "t_is_pair.h"

int
main(int argc, char *argv[])
{
    char answer[STRING_LEN];
    int cfd;
    ssize_t numRead;

    if (argc < 5 || strcmp(argv[1], "--help") == 0)
        usageErr("%s server-host (add|delete|modify|retrieve) pair-name pair-value\n", argv[0]);

    cfd = inetConnect(argv[1], PORT_NUM_STR, SOCK_STREAM);
    if (cfd == -1)
        fatal("inetConnect() failed");

    if (write(cfd, argv[2], strlen(argv[2])) !=  strlen(argv[2]))
        fatal("Partial/failed write (mode)");
    if (write(cfd, "\n", 1) != 1)
        fatal("Partial/failed write (newline)");
    if (write(cfd, argv[3], strlen(argv[3])) !=  strlen(argv[3]))
        fatal("Partial/failed write (pair-name)");
    if (write(cfd, "\n", 1) != 1)
        fatal("Partial/failed write (newline)");
    if (write(cfd, argv[4], strlen(argv[4])) !=  strlen(argv[4]))
        fatal("Partial/failed write (pair-value)");
    if (write(cfd, "\n", 1) != 1)
        fatal("Partial/failed write (newline)");

    numRead = readLine(cfd, answer, STRING_LEN);
    if (numRead == -1)
        errExit("readLine");
    if (numRead == 0)
        fatal("Unexpected EOF from server");
    printf("%s", answer);   					/* Includes '\n' */

    exit(EXIT_SUCCESS);                         /* Closes 'cfd' */
}
