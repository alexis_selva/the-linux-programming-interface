/* Exercice 52-4 */

#include "t_pmsg_chat.h"

static mqd_t client;
static mqd_t server;
static char* username;

static void
register_client(void)
{
    struct message m;
    struct timespec timeout_ts;
    unsigned int prio = 0;

    m.mtype = REQ_REGISTER;
    strncpy(m.from, username, USER_SIZE);
    strncpy(m.to, username, USER_SIZE);
    memcpy(m.data, username, sizeof(int));
    if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
		errExit("clock_gettime");
	timeout_ts.tv_sec += TIMEOUT;
    if (mq_timedsend(server, (char *)&m, MSG_SIZE, prio, &timeout_ts) == -1) {
        errExit("mq_timedsend");
    }   
}

static void
exit_client(void)
{
    struct message m;
    struct timespec timeout_ts;
    unsigned int prio = 0;

    m.mtype = REQ_EXIT;
    strncpy(m.from, username, USER_SIZE);
    strncpy(m.to, username, USER_SIZE);
    if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
		errExit("clock_gettime");
	timeout_ts.tv_sec += TIMEOUT;
    if (mq_timedsend(server, (char *)&m, MSG_SIZE, prio, &timeout_ts) == -1) {
        errExit("mq_timedsend");
    }  
}

static void
write_message(void)
{
    struct message req;
    fd_set readfds;
    int    fd_stdin = fileno(stdin);
    struct timeval tv;
    struct timespec timeout_ts;
    unsigned int prio = 0;

    FD_ZERO(&readfds);
    FD_SET(fd_stdin, &readfds);
    tv.tv_sec = TIMEOUT;
    tv.tv_usec = 0;
    if (select(fd_stdin + 1, &readfds, NULL, NULL, &tv) <= 0)
        return;
    
    scanf("%s", req.to);
    printf("+---> ");
    fflush(stdin);
    scanf("%s", req.data);
    strncpy(req.from, username, USER_SIZE);
    req.mtype = REQ_DATA;
    if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
		errExit("clock_gettime");
	timeout_ts.tv_sec += TIMEOUT;
    if (mq_timedsend(server, (char *)&req, MSG_SIZE, prio, &timeout_ts) == -1) {
        errExit("mq_timedsend");
    } 
}

static void
write_response(struct message *req)
{
	struct timespec timeout_ts;
    unsigned int prio = 0;
    
    req->mtype++;
    if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
		errExit("clock_gettime");
	timeout_ts.tv_sec += TIMEOUT;
    if (mq_timedsend(server, (char *)req, MSG_SIZE, prio, &timeout_ts) == -1) {
        errExit("mq_timedsend");
    } 
}

static void
read_message(void)
{
    struct message req;
    int msgLen;
    struct timespec timeout_ts;
    unsigned int prio = 0;

	if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
		errExit("clock_gettime");
	timeout_ts.tv_sec += TIMEOUT;
    msgLen = mq_timedreceive(client, (char *)&req, MSG_SIZE, &prio, &timeout_ts);
    if (msgLen == -1) {
        if (errno == EINTR || errno == ETIMEDOUT) {
            return;
        }
        errExit("msgrcv");
    }
    
    switch (req.mtype) {
        case REQ_REGISTER:
            printf("%s is in\n", req.from);
            write_response(&req);
            break;
        
        case REQ_EXIT:
            printf("%s is out\n", req.from);
            write_response(&req);
            break;
            
        case REQ_DATA:
            printf("%s\n|---> %s\n", req.from, req.data);
            write_response(&req);
            break;
            
        default:
            break;
    }
}

static void
tidy_exit(int sig)
{
    /* Exit client */
    exit_client();

    /* Remove client's queue */
	if (mq_close(client) == -1)
        errExit("mq_close");
    if (mq_unlink(username) == -1)
        errExit("mq_unlink");
        
    /* Disestablish handler */
    signal(sig, SIG_DFL); 
    
    /* Raise signal again */
    raise(sig);           
}

static void
no_exit(int sig)
{
}

static void 
init_client(void)
{
    struct sigaction sa;
    struct mq_attr attr;

    /* Open well known queue */
    server = mq_open(WELL_KNOWN_QUEUE, O_WRONLY);
    if (server == (mqd_t) -1)
        errExit("mq_open");

    /* Create client message queue */
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MSG_SIZE;
    //client = mq_open(username, O_RDONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR | S_IWGRP, &attr);
    client = mq_open(username, O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IWGRP, &attr);
    if (client == (mqd_t) -1)
        errExit("mq_open");
        
    /* Establish SIGTERM & SIGINT handlers to perform a tidy exit */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = SA_RESETHAND;
    sa.sa_handler = tidy_exit;
    if (sigaction(SIGTERM, &sa, NULL) == -1)
        errExit("sigaction");
    if (sigaction(SIGINT, &sa, NULL) == -1)
        errExit("sigaction");
        
    /* Establish SIGALARM to avoid terminating process */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");
}

static void
run(void)
{
    for ( ; ; ) {
        read_message();
        write_message();
    }
}

int
main(int argc, char *argv[])
{
    /* Check command line */
    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s username\n", argv[0]);
    username = argv[1];

    /* Initialize client */
    init_client();

    /* Register client */
    register_client();

    /* Run client */
    run();
    
    exit(EXIT_SUCCESS);
}
