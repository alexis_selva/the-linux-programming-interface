/* Exercice 52-2 */

#include "t_pmsg_seqnum.h"

static mqd_t mqd;
static int backup_fd;
static int seq_num;

#define BD_MAX_CLOSE  8192              

static int
become_daemon(void)
{
    int maxfd, fd;

    /* Become background process */
    switch (fork()) {                   
    case -1: return -1;
    case 0:  break;
    default: _exit(EXIT_SUCCESS);
    }

    /* Become leader of new session */
    if (setsid() == -1)
        return -1;
        
    /* Ensure we are not session leader */
    switch (fork()) {
    case -1: return -1;
    case 0:  break;
    default: _exit(EXIT_SUCCESS);
    }

    /* Clear file mode creation mask */
    umask(0);                           

    /* Change to root directory */
    chdir("/");                        

    /* Close all open files */
    maxfd = sysconf(_SC_OPEN_MAX);
    if (maxfd == -1)
        maxfd = BD_MAX_CLOSE;
    for (fd = 0; fd < maxfd; fd++)
        close(fd);

    /* Reopen standard fd's to /dev/null */
    fd = open("/dev/null", O_RDWR);
    if (fd != STDIN_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDOUT_FILENO) != STDOUT_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDERR_FILENO) != STDERR_FILENO)
        return -1;

    return 0;
}

static void
tidy_exit(int sig)
{
    /* Remove server MQ */
    if (mq_unlink(WELL_KNOWN_QUEUE) == -1)
        errExit("mq_unlink");
    
    /* Backup seq_num */
    if (lseek(backup_fd, 0, SEEK_SET) == -1)
        errExit("lseek");
    if (write(backup_fd, &seq_num, sizeof(seq_num)) != sizeof(seq_num))
        errExit("write");
    if (close(backup_fd) == -1)
        errExit("close 2");

    /* Close syslog */
    closelog();
    
    /* Disestablish handler */
    signal(sig, SIG_DFL); 
    
    /* Raise signal again */
    raise(sig);           
}

static void
no_exit(int sig)
{
}

static void
handle_send_error(int seq_len)
{
    int ret = EXIT_SUCCESS;
    if (errno == EINTR) {
        syslog(LOG_NOTICE, "Send through %d queue timed out", mqd);
    } else {
        syslog(LOG_NOTICE, "Unable to send message through %d queue", mqd);
        ret = EXIT_FAILURE;
    }
    
    exit(ret);
}

static void
serveRequest(int seq_len, unsigned int prio)
{
    struct timespec timeout_ts;

    /* Send response */
    if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
		errExit("clock_gettime");
	timeout_ts.tv_sec += TIMEOUT;
    if (mq_timedsend(mqd, (char *)&seq_num, sizeof(int), prio, &timeout_ts) == -1) {
        handle_send_error(seq_len);
    }        
    
    /* Update our sequence number */
    seq_num += seq_len;    
}

int
main(int argc, char *argv[])
{
    ssize_t msg_len;
    struct sigaction sa;
    unsigned int prio;
    struct mq_attr attr;
    int seq_len;
    
    /* Open syslog */
    openlog(argv[0], LOG_PID, LOG_DAEMON);
    
    /* Become a daeamon */
    if (become_daemon() == -1)
        errExit("become_daemon");
        
    /* Create backup file */
    backup_fd = open(BACKUP_FILE, 
                    O_RDWR | O_CREAT | O_SYNC,
                    S_IRUSR | S_IWUSR);
    if (backup_fd == -1)
        errExit("open");
    if (read(backup_fd, &seq_num, sizeof(seq_num)) == -1)
        errExit("read");

    /* Create server message queue */
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(int);
    mqd = mq_open(WELL_KNOWN_QUEUE, O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR | S_IWGRP, &attr);
    if (mqd == (mqd_t) -1)
        errExit("mq_open");

    /* Establish SIGTERM & SIGINT handlers to perform a tidy exit */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = tidy_exit;
    if (sigaction(SIGTERM, &sa, NULL) == -1)
        errExit("sigaction");
    if (sigaction(SIGINT, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Establish SIGALARM to avoid terminating process */
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Read requests, handle each in a separate child process */
    for (;;) {
    	msg_len = mq_receive(mqd, (char *)&seq_len, sizeof(int), &prio);
        if (msg_len == -1) {
            if (errno == EINTR)         /* Interrupted by SIGCHLD handler? */
                continue;               /* ... then restart msgrcv() */
            errMsg("mq_receive");           /* Some other error */
            break;                      /* ... so terminate loop */
        }
        serveRequest(seq_len, prio);
    }
   
    exit(EXIT_SUCCESS);
}
