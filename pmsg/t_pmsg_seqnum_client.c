/* Exercice 52-2 */

#include "t_pmsg_seqnum.h"

static void
no_exit(int sig)
{
}

int
main(int argc, char *argv[])
{
	mqd_t mqd; 
    ssize_t msg_len;
    struct sigaction sa;
    struct timespec timeout_ts;
    unsigned int prio = 0;
    int seq_len;

    if (argc > 1 && strcmp(argv[1], "--help") == 0)
        usageErr("%s [seq-len...]\n", argv[0]);

    /* Open well known queue */
    mqd = mq_open(WELL_KNOWN_QUEUE, O_RDWR);
    if (mqd == (mqd_t) -1)
        errExit("mq_open");
        
    /* Establish SIGALARM to avoid terminating process */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");

    /* Construct request and send message */
    seq_len = (argc > 1) ? getInt(argv[1], GN_GT_0, "seq-len") : 1;

    if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
		errExit("clock_gettime");
	timeout_ts.tv_sec += TIMEOUT;
    if (mq_timedsend(mqd, (char *)&seq_len, sizeof(int), prio, &timeout_ts) == -1) {
        errExit("mq_timedsend");
    }

    /* Get response, which may be failure notification */
    int seq_num;
    msg_len = mq_receive(mqd, (char *)&seq_num, sizeof(int), &prio);
    if (msg_len == -1)
        errExit("Unable to receive message through %d queue", mqd);
    printf("%d\n", seq_num);
    
    exit(EXIT_SUCCESS);
}