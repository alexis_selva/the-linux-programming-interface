/* Exercice 52-2 */

#include "tlpi_hdr.h"
#include <sys/stat.h>
#include <signal.h>
#include <mqueue.h>
#include <time.h>
#include <syslog.h>

#define TIMEOUT		 		10
#define BACKUP_FILE     	"/tmp/t_pmsg_seqnum_server.mem"
#define WELL_KNOWN_QUEUE 	"/t_pmsg_seqnum_server"


