/* Exercice 52-4 */

#include <syslog.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "t_pmsg_chat.h"

#define MAX_USER        (255)
#define BD_MAX_CLOSE    (8192)        

typedef struct client_t {
    mqd_t id;
    char  name[USER_SIZE];
} client_t;

static client_t clients[MAX_USER];
static mqd_t    server;

static int
become_daemon(void)
{
	return 0;
    int maxfd, fd;

    /* Become background process */
    switch (fork()) {                   
        case -1: 
            return -1;
        case 0:  
            break;
        default: 
            _exit(EXIT_SUCCESS);
    }

    /* Become leader of new session */
    if (setsid() == -1)
        return -1;
        
    /* Ensure we are not session leader */
    switch (fork()) {
        case -1: 
            return -1;
        case 0:  
            break;
        default: 
            _exit(EXIT_SUCCESS);
    }

    /* Clear file mode creation mask */
    umask(0);                           

    /* Change to root directory */
    chdir("/");                        

    /* Close all open files */
    maxfd = sysconf(_SC_OPEN_MAX);
    if (maxfd == -1)
        maxfd = BD_MAX_CLOSE;
    for (fd = 0; fd < maxfd; fd++)
        close(fd);

    /* Reopen standard fd's to /dev/null */
    fd = open("/dev/null", O_RDWR);
    if (fd != STDIN_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDOUT_FILENO) != STDOUT_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDERR_FILENO) != STDERR_FILENO)
        return -1;

    return 0;
}

static void
tidy_exit(int sig)
{
    /* Remove server MQ and exit */
    if (mq_close(server) == -1)
        errExit("mq_close");
    if (mq_unlink(WELL_KNOWN_QUEUE) == -1)
        errExit("mq_unlink");

    /* Close syslog */
    closelog();
    
    /* Raise signal again */
    raise(sig);           
}

static void
no_exit(int sig)
{
}

static void
handle_error(mqd_t id, const struct message *req)
{
    if (errno == EINTR) {
        syslog(LOG_INFO, "Send through %s queue timed out", req->to);
    } else {
        syslog(LOG_NOTICE, "Unable to send message through %s queue", req->to);
    }
}

static void
handle_register(struct message *m)
{
    unsigned char target = (unsigned char) m->from[1];
    unsigned char other = 0;
    struct timespec timeout_ts;
    unsigned int prio = 0;
    
    if (clients[target].id == 0) {
    	clients[target].id = mq_open(m->from, O_WRONLY);
    	if (clients[target].id == (mqd_t) -1)
        	errExit("mq_open");
        strcpy(clients[target].name, m->from);

        /* Accept register */
        m->mtype++;
        if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
			errExit("clock_gettime");
		timeout_ts.tv_sec += TIMEOUT;
    	if (mq_timedsend(clients[target].id, (char *)m, MSG_SIZE, prio, &timeout_ts) == -1) {
			handle_error(clients[target].id, m);
		} 
        
        /* Broadcast register */
        m->mtype = REQ_REGISTER;
        for ( ; other < MAX_USER ; other++) {
            if (other != target && clients[other].id) {
                strcpy(m->to, clients[other].name);
                if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
					errExit("clock_gettime");
				timeout_ts.tv_sec += TIMEOUT;
    			if (mq_timedsend(clients[other].id, (char *)m, MSG_SIZE, prio, &timeout_ts) == -1) {
					handle_error(clients[other].id, m);
				} 
            }
        }
    }    
}

static void
handle_exit(struct message *m)
{
    unsigned char target = (unsigned char) m->from[1];
    unsigned char other = 0;
    struct timespec timeout_ts;
    unsigned int prio = 0;
    
    if (clients[target].id) {
        
        /* Accept exit: it may fail if the client has already removed its queue */
        m->mtype++;
        if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
			errExit("clock_gettime");
		timeout_ts.tv_sec += TIMEOUT;
    	mq_timedsend(clients[other].id, (char *)m, MSG_SIZE, prio, &timeout_ts);
		
        clients[target].id = 0;
        clients[target].name[0] = '\0';
        
        /* Broadcast exit */
        m->mtype = REQ_EXIT;
        for ( ; other < MAX_USER ; other++) {
            if (other != target && clients[other].id) {
                strcpy(m->to, clients[other].name);
                if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
					errExit("clock_gettime");
				timeout_ts.tv_sec += TIMEOUT;
    			if (mq_timedsend(clients[other].id, (char *)m, MSG_SIZE, prio, &timeout_ts) == -1) {
					handle_error(clients[other].id, m);
				} 
            }
        }
    }  
}

static void
handle_data(struct message *m, char is_request)
{
	struct timespec timeout_ts;
    unsigned int prio = 0;
    unsigned char target = 0;
    if (is_request)
        target = (unsigned char) m->to[1];
    else
        target = (unsigned char) m->from[1];

    if (clients[target].id) {
        
        /* Forward data */
        if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
			errExit("clock_gettime");
		timeout_ts.tv_sec += TIMEOUT;
    	if (mq_timedsend(clients[target].id, (char *)m, MSG_SIZE, prio, &timeout_ts) == -1) {
			handle_error(clients[target].id, m);
		} 
    }  
}

static void
serve_request(struct message *m)
{
    switch (m->mtype) {
        
        case REQ_REGISTER:
            handle_register(m);
            break;
        
        case REQ_EXIT: 
            handle_exit(m);
            break;
        
        case REQ_DATA:
            handle_data(m, 1);
            break;
            
        case RESP_DATA:
            handle_data(m, 0);
            break;
        
        default:
            break;
    }
}

static void
init_server(char *program)
{
    struct sigaction sa;
    struct mq_attr attr;

    /* Open syslog */
    openlog(program, LOG_PID | LOG_PERROR, LOG_DAEMON);
    
    /* Become a daeamon */
    if (become_daemon() == -1)
        errExit("Unable to become a daemon");

    /* Create server message queue */
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MSG_SIZE;
    //server = mq_open(WELL_KNOWN_QUEUE, O_RDONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR | S_IWGRP, &attr);
    server = mq_open(WELL_KNOWN_QUEUE, O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IWGRP, &attr);
    if (server == (mqd_t) -1)
        errExit("mq_open");

    /* Establish SIGTERM & SIGINT handlers to perform a tidy exit */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("Unable to fill mask for the signal handle");
    sa.sa_flags = SA_RESETHAND;
    sa.sa_handler = tidy_exit;
    if (sigaction(SIGTERM, &sa, NULL) == -1)
        errExit("Unable to establish a signal handler for SIGTERM");
    if (sigaction(SIGINT, &sa, NULL) == -1)
        errExit("Unable to establish a signal handler for SIGINT");
    
    /* Establish SIGALARM to avoid terminating process */
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("Unable to establish a signal handler for SIGALRM");
}

static void
read_request(void)
{
    int msgLen;
    struct message req;
    unsigned int prio = 0;

    /* Read incoming request */
    for (;;) {
        msgLen = mq_receive(server, (char *)&req, MSG_SIZE, &prio);
        if (msgLen == -1) {
            if (errno == EINTR)
                continue;
            syslog(LOG_NOTICE, "Unable to receive incoming request");
        }
        serve_request(&req);
    }
}

int
main(int argc, char *argv[])
{
    /* Initialize the server */
    init_server(argv[0]);
    
    /* Read request */
    read_request();

    exit(EXIT_SUCCESS);
}
