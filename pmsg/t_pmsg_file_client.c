/* Exercice 52-3 */

#include "t_pmsg_file.h"

static char* clientId;
static mqd_t client_queue;


static void
removeQueue(void)
{
	if (mq_close(client_queue) == -1)
        errExit("mq_close");
    if (mq_unlink(clientId) == -1)
        errExit("mq_unlink");
}

static void
no_exit(int sig)
{
}

int
main(int argc, char *argv[])
{
    struct requestMsg req;
    struct responseMsg resp;
    int numMsgs;
    ssize_t msgLen, totBytes;
    struct sigaction sa;
    struct mq_attr attr;
    struct timespec timeout_ts;
    unsigned int prio = 0;
    
    if (argc != 3 || strcmp(argv[1], "--help") == 0)
        usageErr("%s id pathname\n", argv[0]);

    if (strlen(argv[1]) > ID_MAX)
        cmdLineErr("id too long (max: %ld bytes)\n", ID_MAX);
    clientId = argv[1];

    if (strlen(argv[2]) > PATH_MAX)
        cmdLineErr("pathname too long (max: %ld bytes)\n", PATH_MAX);
        
    /* Open well known queue */
    mqd_t serveur_queue = mq_open(WELL_KNOWN_QUEUE, O_WRONLY);
    if (serveur_queue == (mqd_t) -1)
        errExit("mq_open");
        
    /* Create client message queue */
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(struct responseMsg);
    client_queue = mq_open(clientId, O_RDONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR | S_IWGRP, &attr);
    if (client_queue == (mqd_t) -1)
        errExit("mq_open");

    if (atexit(removeQueue) != 0)
        errExit("atexit 2");
        
    /* Establish SIGALARM to avoid terminating process */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");

    /* Send message asking for file named in argv[1] */
    strncpy(req.clientId, argv[1], ID_MAX - 1);
    req.clientId[ID_MAX - 1] = '\0';
    strncpy(req.pathname, argv[2], PATH_MAX - 1);
    req.pathname[PATH_MAX - 1] = '\0';
    if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
		errExit("clock_gettime");
	timeout_ts.tv_sec += TIMEOUT;
    if (mq_timedsend(serveur_queue, (char *)&req, sizeof(struct requestMsg), prio, &timeout_ts) == -1) {
        errExit("mq_timedsend");
    }      

    /* Get first response, which may be failure notification */
    msgLen = mq_receive(client_queue, (char *)&resp, sizeof(struct responseMsg), &prio);
    if (msgLen == -1)
        errExit("Unable to receive message through %s queue", clientId);
    if (resp.mtype == RESP_MT_FAILURE) {
        printf("%s\n", resp.data); 
        exit(EXIT_FAILURE);
    }

    /* File was opened successfully by server; process messages
       (including the one already received) containing file data */
    totBytes = msgLen;
    for (numMsgs = 1; resp.mtype == RESP_MT_DATA; numMsgs++) {
    	msgLen = mq_receive(client_queue, (char *)&resp, sizeof(struct responseMsg), &prio);
    	if (msgLen == -1)
        	errExit("Unable to receive message through %s queue", clientId);
        totBytes += msgLen;
    }

    printf("Received %ld bytes (%d messages)\n", (long) totBytes, numMsgs);

    exit(EXIT_SUCCESS);
}
