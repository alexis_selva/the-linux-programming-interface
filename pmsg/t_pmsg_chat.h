/* Exercice 52-4 */

#include <sys/types.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stddef.h>                     /* For definition of offsetof() */
#include <limits.h>
#include <fcntl.h>
#include <signal.h>
#include <mqueue.h>
#include <time.h>
#include <syslog.h>
#include "tlpi_hdr.h"

#define USER_SIZE   	   	(8)
#define DATA_SIZE   	   	(1000)
#define TIMEOUT   		  	(1)
#define WELL_KNOWN_QUEUE 	"/t_pmsg_chat_server"

/* Type of message */
enum {
    REQ_REGISTER = 1,                   /* Register request */
    RESP_REGISTER,                      /* Register OK */
    REQ_EXIT,                           /* Exit request */
    RESP_EXIT,                          /* Exit OK */
    REQ_DATA,                           /* Message request */
    RESP_DATA,                          /* Message OK */
};

struct message {                        /* Message between lient and server */
    long mtype;                         /* Type of message */
    char from[USER_SIZE];               /* From id */
    char to[USER_SIZE];                 /* To id */
    char data[DATA_SIZE];               /* Message content */
};

#define MSG_SIZE        sizeof(struct message)
