/* Exercice 52-3 */

#include "t_pmsg_file.h"

static mqd_t serveur_queue;

#define BD_MAX_CLOSE  8192              

static int
become_daemon(void)
{
    int maxfd, fd;

    /* Become background process */
    switch (fork()) {                   
    case -1: return -1;
    case 0:  break;
    default: _exit(EXIT_SUCCESS);
    }

    /* Become leader of new session */
    if (setsid() == -1)
        return -1;
        
    /* Ensure we are not session leader */
    switch (fork()) {
    case -1: return -1;
    case 0:  break;
    default: _exit(EXIT_SUCCESS);
    }

    /* Clear file mode creation mask */
    umask(0);                           

    /* Change to root directory */
    chdir("/");                        

    /* Close all open files */
    maxfd = sysconf(_SC_OPEN_MAX);
    if (maxfd == -1)
        maxfd = BD_MAX_CLOSE;
    for (fd = 0; fd < maxfd; fd++)
        close(fd);

    /* Reopen standard fd's to /dev/null */
    fd = open("/dev/null", O_RDWR);
    if (fd != STDIN_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDOUT_FILENO) != STDOUT_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDERR_FILENO) != STDERR_FILENO)
        return -1;

    return 0;
}

static void
grimReaper(int sig)
{
    int savedErrno;

    /* waitpid() might change 'errno' */
    savedErrno = errno;                 
    while (waitpid(-1, NULL, WNOHANG) > 0)
        continue;
    errno = savedErrno;
}

static void
tidy_exit(int sig)
{
    /* Remove server MQ and exit */
    if (mq_close(serveur_queue) == -1)
        errExit("mq_close");
    if (mq_unlink(WELL_KNOWN_QUEUE) == -1)
        errExit("mq_unlink");
    
    /* Close syslog */
    closelog();
    
    /* Disestablish handler */
    signal(sig, SIG_DFL); 
    
    /* Raise signal again */
    raise(sig);           
}

static void
no_exit(int sig)
{
}

static void
handle_send_error(const struct requestMsg *req)
{
    int ret = EXIT_SUCCESS;
    if (errno == EINTR) {
        syslog(LOG_NOTICE, "Send through %s queue timed out", req->clientId);
    } else {
        syslog(LOG_NOTICE, "Unable to send message through %s queue", req->clientId);
        ret = EXIT_FAILURE;
    }
    
    exit(ret);
}

static void
serveRequest(const struct requestMsg *req)
{
    int fd;
    ssize_t numRead;
    struct responseMsg resp;
    struct timespec timeout_ts;
    unsigned int prio = 0;
    
    /* Open client queue */
    mqd_t client_queue = mq_open(req->clientId, O_WRONLY);
    if (client_queue == (mqd_t) -1)
        errExit("mq_open");
        
    /* Executed in child process: serve a single client */
    fd = open(req->pathname, O_RDONLY);
    if (fd == -1) {
        resp.mtype = RESP_MT_FAILURE;
        numRead = snprintf(resp.data, sizeof(resp.data), "Couldn't open %s", req->pathname);
    	if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
			errExit("clock_gettime");
		timeout_ts.tv_sec += TIMEOUT;
    	if (mq_timedsend(client_queue, (char *)&resp, sizeof(long) + numRead , prio, &timeout_ts) == -1) {
            handle_send_error(req);
    	}   
        syslog(LOG_NOTICE, "Unable to open %s", req->pathname);
        exit(EXIT_FAILURE);
    }

    /* Transmit file contents in messages with type RESP_MT_DATA. We don't
       diagnose read() and msgsnd() errors since we can't notify client. */
    resp.mtype = RESP_MT_DATA;
    while ((numRead = read(fd, resp.data, DATA_MAX)) > 0) {
        if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
			errExit("clock_gettime");
		timeout_ts.tv_sec += TIMEOUT;
    	if (mq_timedsend(client_queue, (char *)&resp, sizeof(long) + numRead, prio, &timeout_ts) == -1) {
            handle_send_error(req);
    	}   
    }

    /* Send a message of type RESP_MT_END to signify end-of-file */
    resp.mtype = RESP_MT_END;
    if (clock_gettime(CLOCK_REALTIME, &timeout_ts) == -1)
		errExit("clock_gettime");
	timeout_ts.tv_sec += TIMEOUT;
    if (mq_timedsend(client_queue, (char *)&resp, sizeof(long), prio, &timeout_ts) == -1) {
        handle_send_error(req);
    }  
    
    /* Close client MQ */
    if (mq_close(client_queue) == -1)
        errExit("mq_close");
        
    exit(EXIT_SUCCESS);
}

int
main(int argc, char *argv[])
{
    struct requestMsg req;
    pid_t pid;
    ssize_t msgLen;
    struct sigaction sa;
    struct mq_attr attr;
    unsigned int prio = 0;

    /* Open syslog */
    openlog(argv[0], LOG_PID, LOG_DAEMON);
    
    /* Become a daeamon */
    if (become_daemon() == -1)
        errExit("become_daemon");

    /* Create server message queue */
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(struct requestMsg);
    serveur_queue = mq_open(WELL_KNOWN_QUEUE, O_RDONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR | S_IWGRP, &attr);
    if (serveur_queue == (mqd_t) -1)
        errExit("mq_open");

    /* Establish SIGCHLD handler to reap terminated children */
    if (sigemptyset(&sa.sa_mask) == -1)
        errExit("sigemptyset");
    sa.sa_flags = SA_RESTART;
    sa.sa_handler = grimReaper;
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Establish SIGTERM & SIGINT handlers to perform a tidy exit */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = tidy_exit;
    if (sigaction(SIGTERM, &sa, NULL) == -1)
        errExit("sigaction");
    if (sigaction(SIGINT, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Establish SIGALARM to avoid terminating process */
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Read requests, handle each in a separate child process */
    for (;;) {
    	msgLen = mq_receive(serveur_queue, (char *)&req, sizeof(struct requestMsg), &prio);
        if (msgLen == -1) {
            if (errno == EINTR)         /* Interrupted by SIGCHLD handler? */
                continue;               /* ... then restart msgrcv() */
            errMsg("msgrcv");           /* Some other error */
            break;                      /* ... so terminate loop */
        }

        pid = fork();                   /* Create child process */
        if (pid == -1) {
            errMsg("fork");
            break;
        }

        if (pid == 0) {                 /* Child handles request */
            serveRequest(&req);
            _exit(EXIT_SUCCESS);
        }

        /* Parent loops to receive next client request */
    }

    exit(EXIT_SUCCESS);
}
