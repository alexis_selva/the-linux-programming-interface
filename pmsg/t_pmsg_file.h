/* Exercice 52-3 */

#include "tlpi_hdr.h"
#include <sys/stat.h>
#include <signal.h>
#include <mqueue.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <syslog.h>

#define ID_MAX 		16
#define PATH_MAX 	16

struct requestMsg {                     /* Requests (client to server) */
    char clientId[ID_MAX];
    char pathname[PATH_MAX];
};

#define DATA_MAX 1024

struct responseMsg {                    /* Responses (server to client) */
    long mtype;                         /* One of RESP_MT_* values below */
    char data[DATA_MAX];           		/* File content / response message */
};

/* Types for response messages sent from server to client */
#define RESP_MT_FAILURE 1               /* File couldn't be opened */
#define RESP_MT_DATA    2               /* Message contains file data */
#define RESP_MT_END     3               /* File data complete */

#define TIMEOUT 10
#define WELL_KNOWN_QUEUE 	"/t_pmsg_file_server"
