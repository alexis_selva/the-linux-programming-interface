/* Exercise 15-6 */

#define _GNU_SOURCE
#include <sys/stat.h>

#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
    int j;
    struct stat sb;
    mode_t mode;
    
    /* Open each filename in turn, and change its permission */
    for (j = 1; j < argc; j++) {
        if (stat(argv[j], &sb) == -1)
            errExit("stat");
        mode = sb.st_mode;
        mode |= S_IXUSR;
        mode |= S_IXGRP;
        mode |= S_IXOTH;
        if (chmod(argv[j], mode) == -1) 
            errExit("chmod");
    }

    exit(EXIT_SUCCESS);
}
