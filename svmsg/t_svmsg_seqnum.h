/* Exercice 46-2 */

#include <sys/types.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <stddef.h>                     /* For definition of offsetof() */
#include <limits.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/wait.h>
#include "tlpi_hdr.h"

#define BACKUP_FILE     "/tmp/t_svmsg_seqnum_server.mem"
#define WELL_KNOWN_FILE "/tmp/t_svmsg_seqnum_server.id"

struct request_msg {                    /* Requests (client to server) */
    long mtype;                         
    pid_t pid;                          /* Client PID */
    int seq_len;                        /* Length of desired sequence */
};

/* Adapt request size in function of struct padding */
#define REQ_MSG_SIZE    (offsetof(struct request_msg, seq_len) - offsetof(struct request_msg, pid) + sizeof(int))
                      
struct response_msg {                   /* Responses (server to client) */
    long mtype;                         
    int seq_num;                        /* Start of sequence */
};

/* Adapt response size in function of struct padding */
#define RESP_MSG_SIZE   (sizeof(int))

#define SVMSG_TIMEOUT 10