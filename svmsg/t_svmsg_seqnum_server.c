/* Exercice 46-2 */

#include <syslog.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "t_svmsg_seqnum.h"

static int server_id;
static int wk_fd;
static int backup_fd;
static int seq_num;

static int
become_daemon(void)
{
#define BD_MAX_CLOSE  8192              
    int maxfd, fd;

    /* Become background process */
    switch (fork()) {                   
    case -1: return -1;
    case 0:  break;
    default: _exit(EXIT_SUCCESS);
    }

    /* Become leader of new session */
    if (setsid() == -1)
        return -1;
        
    /* Ensure we are not session leader */
    switch (fork()) {
    case -1: return -1;
    case 0:  break;
    default: _exit(EXIT_SUCCESS);
    }

    /* Clear file mode creation mask */
    umask(0);                           

    /* Change to root directory */
    chdir("/");                        

    /* Close all open files */
    maxfd = sysconf(_SC_OPEN_MAX);
    if (maxfd == -1)
        maxfd = BD_MAX_CLOSE;
    for (fd = 0; fd < maxfd; fd++)
        close(fd);

    /* Reopen standard fd's to /dev/null */
    fd = open("/dev/null", O_RDWR);
    if (fd != STDIN_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDOUT_FILENO) != STDOUT_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDERR_FILENO) != STDERR_FILENO)
        return -1;

    return 0;
}

static void
tidy_exit(int sig)
{
    /* Remove well known file */
    if (unlink(WELL_KNOWN_FILE) == -1)
        errExit("unlink");
    if (close(wk_fd) == -1)
        errExit("close 1");
    
    /* Remove server MQ */
    if (msgctl(server_id, IPC_RMID, NULL) == -1)
        errExit("msgctl");
    
    /* Backup seq_num */
    if (lseek(backup_fd, 0, SEEK_SET) == -1)
        errExit("lseek");
    if (write(backup_fd, &seq_num, sizeof(seq_num)) != sizeof(seq_num))
        errExit("write");
    if (close(backup_fd) == -1)
        errExit("close 2");

    /* Close syslog */
    closelog();
    
    /* Disestablish handler */
    signal(sig, SIG_DFL); 
    
    /* Raise signal again */
    raise(sig);           
}

static void
no_exit(int sig)
{
}

static void
handle_send_error(const struct request_msg *req)
{
    int ret = EXIT_SUCCESS;
    if (errno == EINTR) {
        syslog(LOG_NOTICE, "Send through %d svmsg queue timed out", server_id);
    } else {
        syslog(LOG_NOTICE, "Unable to send message through %d svmsg queue", server_id);
        ret = EXIT_FAILURE;
    }
    
    exit(ret);
}

static void
serveRequest(const struct request_msg *req)
{
    struct response_msg resp;

    /* Send response */
    resp.mtype = req->pid;
    resp.seq_num = seq_num;
    
    alarm(SVMSG_TIMEOUT);
    if (msgsnd(server_id, &resp, RESP_MSG_SIZE, 0) == -1) {
        handle_send_error(req);
    }
    
    /* Update our sequence number */
    seq_num += req->seq_len;           
}

int
main(int argc, char *argv[])
{
    struct request_msg req;
    ssize_t msg_len;
    struct sigaction sa;
    
    /* Open syslog */
    openlog(argv[0], LOG_PID, LOG_DAEMON);
    
    /* Become a daeamon */
    if (become_daemon() == -1)
        errExit("become_daemon");
        
    /* Create backup file */
    backup_fd = open(BACKUP_FILE, 
                    O_RDWR | O_CREAT | O_SYNC,
                    S_IRUSR | S_IWUSR);
    if (backup_fd == -1)
        errExit("open");
    if (read(backup_fd, &seq_num, sizeof(seq_num)) == -1)
        errExit("read");

    /* Create server message queue */
    server_id = msgget(IPC_PRIVATE, 
                       IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR | S_IWGRP);
    if (server_id == -1)
        errExit("msgget");
        
    /* Create well known file */
    wk_fd = open(WELL_KNOWN_FILE, O_WRONLY | O_CREAT | O_EXCL | O_SYNC, 
                                  S_IRUSR | S_IWUSR);
    if (wk_fd == -1)
        errExit("open");
        
    /* Write server id */
    if (write(wk_fd, &server_id, sizeof(server_id)) != sizeof(server_id))
        errExit("write");

    /* Establish SIGTERM & SIGINT handlers to perform a tidy exit */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = tidy_exit;
    if (sigaction(SIGTERM, &sa, NULL) == -1)
        errExit("sigaction");
    if (sigaction(SIGINT, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Establish SIGALARM to avoid terminating process */
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Read requests, handle each in a separate child process */
    for (;;) {
        msg_len = msgrcv(server_id, &req, REQ_MSG_SIZE, 1, 0);
        if (msg_len == -1) {
            if (errno == EINTR)         /* Interrupted by SIGCHLD handler? */
                continue;               /* ... then restart msgrcv() */
            errMsg("msgrcv");           /* Some other error */
            break;                      /* ... so terminate loop */
        }
        serveRequest(&req);
    }

    exit(EXIT_SUCCESS);
}
