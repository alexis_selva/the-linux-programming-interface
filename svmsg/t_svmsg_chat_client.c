/* Exercice 46-6 */

#include "t_svmsg_chat.h"

static int clientId;
static int serverId;
static char* username;

static void
register_client(void)
{
    struct message m;

    m.mtype = REQ_REGISTER;
    strncpy(m.from, username, USER_SIZE);
    strncpy(m.to, username, USER_SIZE);
    memcpy(m.data, &clientId, sizeof(int));
    alarm(SVMSG_TIMEOUT);
    if (msgsnd(serverId, &m, MSG_SIZE, 0) == -1)
        errExit("Unable to send message through %d svmsg queue", serverId);
}

static void
exit_client(void)
{
    struct message m;

    m.mtype = REQ_EXIT;
    strncpy(m.from, username, USER_SIZE);
    strncpy(m.to, username, USER_SIZE);
    alarm(SVMSG_TIMEOUT);
    if (msgsnd(serverId, &m, MSG_SIZE, 0) == -1)
        errExit("Unable to send message through %d svmsg queue", serverId);
}

static void
write_message(void)
{
    struct message req;
    fd_set readfds;
    int    fd_stdin = fileno(stdin);
    struct timeval tv;

    FD_ZERO(&readfds);
    FD_SET(fd_stdin, &readfds);
    tv.tv_sec = SVMSG_TIMEOUT;
    tv.tv_usec = 0;
    if (select(fd_stdin + 1, &readfds, NULL, NULL, &tv) <= 0)
        return;
    
    scanf("%s", req.to);
    printf("+---> ");
    fflush(stdin);
    scanf("%s", req.data);
    strncpy(req.from, username, USER_SIZE);
    req.mtype = REQ_DATA;
    alarm(SVMSG_TIMEOUT);
    if (msgsnd(serverId, &req, MSG_SIZE, 0) == -1)
        errExit("Unable to send message through %d svmsg queue", serverId);
}

static void
write_response(struct message *req)
{
    req->mtype++;
    alarm(SVMSG_TIMEOUT);
    if (msgsnd(serverId, req, MSG_SIZE, 0) == -1)
        errExit("Unable to send message through %d svmsg queue", serverId);
}

static void
read_message(void)
{
    struct message req;
    int msgLen;

    alarm(SVMSG_TIMEOUT);
    msgLen = msgrcv(clientId, &req, MSG_SIZE, 0, 0);
    if (msgLen == -1) {
        if (errno == EINTR) {
            return;
        }
        errExit("msgrcv");
    }
    
    switch (req.mtype) {
        case REQ_REGISTER:
            printf("%s is in\n", req.from);
            write_response(&req);
            break;
        
        case REQ_EXIT:
            printf("%s is out\n", req.from);
            write_response(&req);
            break;
            
        case REQ_DATA:
            printf("%s\n|---> %s\n", req.from, req.data);
            write_response(&req);
            break;
            
        default:
            break;
    }
}

static void
tidy_exit(int sig)
{
    /* Exit client */
    exit_client();

    /* Remove client's queue */
    if (msgctl(clientId, IPC_RMID, NULL) == -1)
        errExit("msgctl");
    
    /* Disestablish handler */
    signal(sig, SIG_DFL); 
    
    /* Raise signal again */
    raise(sig);           
}

static void
no_exit(int sig)
{
}

static void 
init_client(void)
{
    int wk_fd;
    struct sigaction sa;

    /* Open well known file */
    wk_fd = open(WELL_KNOWN_FILE,
                     O_RDONLY | S_IRUSR | S_IWUSR);
    if (wk_fd == -1)
        errExit("open");
        
    /* Get server's queue identifier */
    if (read(wk_fd, &serverId, sizeof(serverId)) != sizeof(serverId))
        errExit("read");
        
    /* close well known file */
    if (close(wk_fd) == -1)
        errMsg("close");
        
    /* Create client's queue  */
    clientId = msgget(IPC_PRIVATE, S_IRUSR | S_IWUSR | S_IWGRP);
    if (clientId == -1)
        errExit("msgget - client message queue");
        
    /* Establish SIGTERM & SIGINT handlers to perform a tidy exit */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = SA_RESETHAND;
    sa.sa_handler = tidy_exit;
    if (sigaction(SIGTERM, &sa, NULL) == -1)
        errExit("sigaction");
    if (sigaction(SIGINT, &sa, NULL) == -1)
        errExit("sigaction");
        
    /* Establish SIGALARM to avoid terminating process */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");
}

static void
run(void)
{
    for ( ; ; ) {
        read_message();
        write_message();
    }
}

int
main(int argc, char *argv[])
{
    /* Check command line */
    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s username\n", argv[0]);

    /* Initialize client */
    init_client();
    username = argv[1];
            
    /* Register client */
    register_client();

    /* Run client */
    run();
    
    exit(EXIT_SUCCESS);
}
