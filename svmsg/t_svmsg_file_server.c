/* Exercice 46-4 */

#include <syslog.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "t_svmsg_file.h"

static int serverId;
static int wk_fd;

static int
become_daemon(void)
{
#define BD_MAX_CLOSE  8192              
    int maxfd, fd;

    /* Become background process */
    switch (fork()) {                   
    case -1: return -1;
    case 0:  break;
    default: _exit(EXIT_SUCCESS);
    }

    /* Become leader of new session */
    if (setsid() == -1)
        return -1;
        
    /* Ensure we are not session leader */
    switch (fork()) {
    case -1: return -1;
    case 0:  break;
    default: _exit(EXIT_SUCCESS);
    }

    /* Clear file mode creation mask */
    umask(0);                           

    /* Change to root directory */
    chdir("/");                        

    /* Close all open files */
    maxfd = sysconf(_SC_OPEN_MAX);
    if (maxfd == -1)
        maxfd = BD_MAX_CLOSE;
    for (fd = 0; fd < maxfd; fd++)
        close(fd);

    /* Reopen standard fd's to /dev/null */
    fd = open("/dev/null", O_RDWR);
    if (fd != STDIN_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDOUT_FILENO) != STDOUT_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDERR_FILENO) != STDERR_FILENO)
        return -1;

    return 0;
}

static void
grimReaper(int sig)
{
    int savedErrno;

    /* waitpid() might change 'errno' */
    savedErrno = errno;                 
    while (waitpid(-1, NULL, WNOHANG) > 0)
        continue;
    errno = savedErrno;
}

static void
tidy_exit(int sig)
{
    /* If msgrcv() or fork() fails, remove server MQ and exit */
    if (msgctl(serverId, IPC_RMID, NULL) == -1)
        errExit("msgctl");
    
    /* Remove well known file */
    if (unlink(WELL_KNOWN_FILE) == -1)
        errExit("unlink");
    if (close(wk_fd) == -1)
        errMsg("close");
        
    /* Close syslog */
    closelog();
    
    /* Disestablish handler */
    signal(sig, SIG_DFL); 
    
    /* Raise signal again */
    raise(sig);           
}

static void
no_exit(int sig)
{
}

static void
handle_send_error(const struct requestMsg *req)
{
    int ret = EXIT_SUCCESS;
    if (errno == EINTR) {
        syslog(LOG_NOTICE, "Send through %d svmsg queue timed out", req->clientId);
        
        /* Remove client */
        if (msgctl(req->clientId, IPC_RMID, NULL) == -1)
            errExit("msgctl");
            
    } else {
        syslog(LOG_NOTICE, "Unable to send message through %d svmsg queue", req->clientId);
        ret = EXIT_FAILURE;
    }
    
    exit(ret);
}

static void
serveRequest(const struct requestMsg *req)
{
    int fd;
    ssize_t numRead;
    struct responseMsg resp;

    /* Executed in child process: serve a single client */
    fd = open(req->pathname, O_RDONLY);
    if (fd == -1) {
        resp.mtype = RESP_MT_FAILURE;
        snprintf(resp.data, sizeof(resp.data), "Couldn't open %s", req->pathname);
        alarm(SVMSG_TIMEOUT);
        if (msgsnd(req->clientId, &resp, strlen(resp.data) + 1, 0) == -1) {
            handle_send_error(req);
        }
        syslog(LOG_NOTICE, "Unable to open %s", req->pathname);
        exit(EXIT_FAILURE);
    }

    /* Transmit file contents in messages with type RESP_MT_DATA. We don't
       diagnose read() and msgsnd() errors since we can't notify client. */
    resp.mtype = RESP_MT_DATA;
    while ((numRead = read(fd, resp.data, RESP_MSG_SIZE)) > 0) {
        alarm(SVMSG_TIMEOUT);
        if (msgsnd(req->clientId, &resp, numRead, 0) == -1) {
            handle_send_error(req);
        }
    }

    /* Send a message of type RESP_MT_END to signify end-of-file */
    resp.mtype = RESP_MT_END;
    alarm(SVMSG_TIMEOUT);
    if (msgsnd(req->clientId, &resp, 0, 0) == -1) {
        handle_send_error(req);
    }
    
    exit(EXIT_SUCCESS);
}

int
main(int argc, char *argv[])
{
    struct requestMsg req;
    pid_t pid;
    ssize_t msgLen;
    struct sigaction sa;
    
    /* Open syslog */
    openlog(argv[0], LOG_PID, LOG_DAEMON);
    
    /* Become a daeamon */
    if (become_daemon() == -1)
        errExit("become_daemon");

    /* Create server message queue */
    serverId = msgget(IPC_PRIVATE, 
                      IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR | S_IWGRP);
    if (serverId == -1)
        errExit("msgget");
        
    /* Create well known file */
    wk_fd = open(WELL_KNOWN_FILE, O_WRONLY | O_CREAT | O_EXCL | O_SYNC, 
                                      S_IRUSR | S_IWUSR);
    if (wk_fd == -1)
        errExit("open");
        
    /* Write server id */
    if (write(wk_fd, &serverId, sizeof(int)) != sizeof(int))
        errExit("write");

    /* Establish SIGCHLD handler to reap terminated children */
    if (sigemptyset(&sa.sa_mask) == -1)
        errExit("sigemptyset");
    sa.sa_flags = SA_RESTART;
    sa.sa_handler = grimReaper;
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Establish SIGTERM & SIGINT handlers to perform a tidy exit */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = tidy_exit;
    if (sigaction(SIGTERM, &sa, NULL) == -1)
        errExit("sigaction");
    if (sigaction(SIGINT, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Establish SIGALARM to avoid terminating process */
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");
    
    /* Read requests, handle each in a separate child process */
    for (;;) {
        msgLen = msgrcv(serverId, &req, REQ_MSG_SIZE, 0, 0);
        if (msgLen == -1) {
            if (errno == EINTR)         /* Interrupted by SIGCHLD handler? */
                continue;               /* ... then restart msgrcv() */
            errMsg("msgrcv");           /* Some other error */
            break;                      /* ... so terminate loop */
        }

        pid = fork();                   /* Create child process */
        if (pid == -1) {
            errMsg("fork");
            break;
        }

        if (pid == 0) {                 /* Child handles request */
            serveRequest(&req);
            _exit(EXIT_SUCCESS);
        }

        /* Parent loops to receive next client request */
    }

    exit(EXIT_SUCCESS);
}
