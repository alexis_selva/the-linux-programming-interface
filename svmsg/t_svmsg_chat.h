/* Exercice 46-4 + 46.5 */

#include <sys/types.h>
#include <sys/select.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <stddef.h>                     /* For definition of offsetof() */
#include <limits.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/wait.h>
#include "tlpi_hdr.h"

#define USER_SIZE       (8)
#define DATA_SIZE       (1000)
#define SVMSG_TIMEOUT   (1)
#define WELL_KNOWN_FILE "/tmp/t_chat_server.id"

/* Type of message */
enum {
    REQ_REGISTER = 1,                   /* Register request */
    RESP_REGISTER,                      /* Register OK */
    REQ_EXIT,                           /* Exit request */
    RESP_EXIT,                          /* Exit OK */
    REQ_DATA,                           /* Message request */
    RESP_DATA,                          /* Message OK */
};

struct message {                        /* Message between lient and server */
    long mtype;                         /* Type of message */
    char from[USER_SIZE];               /* From id */
    char to[USER_SIZE];                 /* To id */
    char data[DATA_SIZE];               /* Message content */
};

/* MSG_SIZE computes size of 'mtext' part of 'request' structure.
   We use offsetof() to handle the possibility that there are padding
   bytes between the 'from' and 'data' fields. */
#define MSG_SIZE        (offsetof(struct message, data) - \
                         offsetof(struct message, from) + DATA_SIZE)
