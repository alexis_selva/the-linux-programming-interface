/* Exercice 46-5 */

#include "t_svmsg_file.h"

static int clientId;
static int wk_fd;

static void
remove_wk_file(void)
{
    if (close(wk_fd) == -1)
        errMsg("close");
}

static void
removeQueue(void)
{
    if (msgctl(clientId, IPC_RMID, NULL) == -1)
        errExit("msgctl");
}

static void
no_exit(int sig)
{
}

int
main(int argc, char *argv[])
{
    struct requestMsg req;
    struct responseMsg resp;
    int serverId, numMsgs;
    ssize_t msgLen, totBytes;
    struct sigaction sa;

    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s pathname\n", argv[0]);

    if (strlen(argv[1]) > sizeof(req.pathname) - 1)
        cmdLineErr("pathname too long (max: %ld bytes)\n",
                (long) sizeof(req.pathname) - 1);

    /* Open well known file */
    wk_fd = open(WELL_KNOWN_FILE,
                     O_RDONLY | S_IRUSR | S_IWUSR);
    if (wk_fd == -1)
        errExit("open");
        
    /* Get server's queue identifier; create queue for response */
    if (read(wk_fd, &serverId, sizeof(int)) != sizeof(int))
        errExit("read");

    if (atexit(remove_wk_file) != 0)
        errExit("atexit 1");

    clientId = msgget(IPC_PRIVATE, S_IRUSR | S_IWUSR | S_IWGRP);
    if (clientId == -1)
        errExit("msgget - client message queue");

    if (atexit(removeQueue) != 0)
        errExit("atexit 2");
        
    /* Establish SIGALARM to avoid terminating process */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");

    /* Send message asking for file named in argv[1] */
    req.mtype = 1; 
    req.clientId = clientId;
    strncpy(req.pathname, argv[1], sizeof(req.pathname) - 1);
    req.pathname[sizeof(req.pathname) - 1] = '\0';
    alarm(SVMSG_TIMEOUT);
    if (msgsnd(serverId, &req, REQ_MSG_SIZE, 0) == -1)
        errExit("Unable to send message through %d svmsg queue", serverId);

    /* Get first response, which may be failure notification */
    msgLen = msgrcv(clientId, &resp, RESP_MSG_SIZE, 0, 0);
    if (msgLen == -1)
        errExit("Unable to receive message through %d svmsg queue", clientId);

    if (resp.mtype == RESP_MT_FAILURE) {
        printf("%s\n", resp.data); 
        exit(EXIT_FAILURE);
    }

    /* File was opened successfully by server; process messages
       (including the one already received) containing file data */
    totBytes = msgLen;
    for (numMsgs = 1; resp.mtype == RESP_MT_DATA; numMsgs++) {
        msgLen = msgrcv(clientId, &resp, RESP_MSG_SIZE, 0, 0);
        if (msgLen == -1)
            errExit("Unable to receive message through %d svmsg queue", clientId);
        totBytes += msgLen;
    }

    printf("Received %ld bytes (%d messages)\n", (long) totBytes, numMsgs);

    exit(EXIT_SUCCESS);
}
