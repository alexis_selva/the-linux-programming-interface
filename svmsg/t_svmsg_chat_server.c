/* Exercice 46-6 */

#include <syslog.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "t_svmsg_chat.h"

#define MAX_USER        (255)
#define BD_MAX_CLOSE    (8192)        

typedef struct client_t {
    int id;
    char name[USER_SIZE];
} client_t;

static client_t clients[MAX_USER];
static int serverId;
static int wk_fd;

static int
become_daemon(void)
{
    int maxfd, fd;

    /* Become background process */
    switch (fork()) {                   
        case -1: 
            return -1;
        case 0:  
            break;
        default: 
            _exit(EXIT_SUCCESS);
    }

    /* Become leader of new session */
    if (setsid() == -1)
        return -1;
        
    /* Ensure we are not session leader */
    switch (fork()) {
        case -1: 
            return -1;
        case 0:  
            break;
        default: 
            _exit(EXIT_SUCCESS);
    }

    /* Clear file mode creation mask */
    umask(0);                           

    /* Change to root directory */
    chdir("/");                        

    /* Close all open files */
    maxfd = sysconf(_SC_OPEN_MAX);
    if (maxfd == -1)
        maxfd = BD_MAX_CLOSE;
    for (fd = 0; fd < maxfd; fd++)
        close(fd);

    /* Reopen standard fd's to /dev/null */
    fd = open("/dev/null", O_RDWR);
    if (fd != STDIN_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDOUT_FILENO) != STDOUT_FILENO)
        return -1;
    if (dup2(STDIN_FILENO, STDERR_FILENO) != STDERR_FILENO)
        return -1;

    return 0;
}

static void
tidy_exit(int sig)
{
    /* Remove well known file */
    if (unlink(WELL_KNOWN_FILE) == -1)
        syslog(LOG_NOTICE, "Unable to remove well known file");
    
    /* If msgrcv() or fork() fails, remove server MQ and exit */
    if (msgctl(serverId, IPC_RMID, NULL) == -1)
        syslog(LOG_NOTICE, "Unable to remove svmsg queue");

    /* Close syslog */
    closelog();
    
    /* Raise signal again */
    raise(sig);           
}

static void
no_exit(int sig)
{
}

static void
handle_error(int clientId, const struct message *req)
{
    if (errno == EINTR) {
        syslog(LOG_INFO, "Send through %d svmsg queue timed out", clientId);
        
        /* Remove client svmsg queue */
        if (msgctl(clientId, IPC_RMID, NULL) == -1)
            syslog(LOG_NOTICE, "Unable to remove %d svmsg queue", clientId);
            
    } else {
        syslog(LOG_NOTICE, "Unable to send message through %d svmsg queue", clientId);
    }
}

static void
handle_register(struct message *m)
{
    unsigned char target = (unsigned char) m->from[0];
    unsigned char other = 0;
    
    if (clients[target].id == 0) {
        memcpy(&clients[target].id, m->data, sizeof(int));
        strcpy(clients[target].name, m->from);

        /* Accept register */
        m->mtype++;
        alarm(SVMSG_TIMEOUT);
        if (msgsnd(clients[target].id, m, 0, 0) == -1) {
            handle_error(clients[target].id, m);
        }
        
        /* Broadcast register */
        m->mtype = REQ_REGISTER;
        for ( ; other < MAX_USER ; other++) {
            if (other != target && clients[other].id) {
                strcpy(m->to, clients[other].name);
                alarm(SVMSG_TIMEOUT);
                if (msgsnd(clients[other].id, m, MSG_SIZE, 0) == -1) {
                    handle_error(clients[other].id, m);
                }
            }
        }
    }    
}

static void
handle_exit(struct message *m)
{
    unsigned char target = (unsigned char) m->from[0];
    unsigned char other = 0;
    
    if (clients[target].id) {
        
        /* Accept exit: it may fail if the client has already removed its queue */
        m->mtype++;
        alarm(SVMSG_TIMEOUT);
        msgsnd(clients[target].id, m, 0, 0);
        clients[target].id = 0;
        clients[target].name[0] = '\0';
        
        /* Broadcast exit */
        m->mtype = REQ_EXIT;
        for ( ; other < MAX_USER ; other++) {
            if (other != target && clients[other].id) {
                strcpy(m->to, clients[other].name);
                alarm(SVMSG_TIMEOUT);
                if (msgsnd(clients[other].id, m, MSG_SIZE, 0) == -1) {
                    handle_error(clients[other].id, m);
                }
            }
        }
    }  
}

static void
handle_data(struct message *m, char is_request)
{
    
    unsigned char target = 0;
    if (is_request)
        target = (unsigned char) m->to[0];
    else
        target = (unsigned char) m->from[0];

    if (clients[target].id) {
        
        /* Forward data */
        alarm(SVMSG_TIMEOUT);
        if (msgsnd(clients[target].id, m, MSG_SIZE, 0) == -1) {
            handle_error(clients[target].id, m);
        }
    }  
}

static void
serve_request(struct message *m)
{
    switch (m->mtype) {
        
        case REQ_REGISTER:
            handle_register(m);
            break;
        
        case REQ_EXIT: 
            handle_exit(m);
            break;
        
        case REQ_DATA:
            handle_data(m, 1);
            break;
            
        case RESP_DATA:
            handle_data(m, 0);
            break;
        
        default:
            break;
    }
}

static void
init_server(char *program)
{
    struct sigaction sa;
    
    /* Open syslog */
    openlog(program, LOG_PID | LOG_PERROR, LOG_DAEMON);
    
    /* Become a daeamon */
    if (become_daemon() == -1)
        errExit("Unable to become a daemon");

    /* Create server message queue */
    serverId = msgget(IPC_PRIVATE, 
                      IPC_CREAT | IPC_EXCL |
                      S_IRUSR | S_IWUSR | S_IWGRP);
    if (serverId == -1)
        errExit("Unable to get svmsg queue id");
        
    /* Create well known file */
    wk_fd = open(WELL_KNOWN_FILE, 
                 O_WRONLY | O_CREAT | O_TRUNC | O_SYNC, 
                 S_IRUSR | S_IWUSR);
    if (wk_fd == -1)
        errExit("Unable to open well known file");
        
    /* Write server id */
    if (write(wk_fd, &serverId, sizeof(serverId)) != sizeof(serverId))
        errExit("Unable to write svmsg queue id");

    /* Establish SIGTERM & SIGINT handlers to perform a tidy exit */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("Unable to fill mask for the signal handle");
    sa.sa_flags = SA_RESETHAND;
    sa.sa_handler = tidy_exit;
    if (sigaction(SIGTERM, &sa, NULL) == -1)
        errExit("Unable to establish a signal handler for SIGTERM");
    if (sigaction(SIGINT, &sa, NULL) == -1)
        errExit("Unable to establish a signal handler for SIGINT");
    
    /* Establish SIGALARM to avoid terminating process */
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("Unable to establish a signal handler for SIGALRM");
}

static void
read_request(void)
{
    int msgLen;
    struct message req;
    
    /* Read incoming request */
    for (;;) {
        msgLen = msgrcv(serverId, &req, MSG_SIZE, 0, 0);
        if (msgLen == -1) {
            if (errno == EINTR)
                continue;
            syslog(LOG_NOTICE, "Unable to receive incoming request");
        }
        serve_request(&req);
    }
}

int
main(int argc, char *argv[])
{
    /* Initialize the server */
    init_server(argv[0]);
    
    /* Read request */
    read_request();

    exit(EXIT_SUCCESS);
}
