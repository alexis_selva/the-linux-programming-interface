/* Exercice 46-2 */

#include "t_svmsg_seqnum.h"

static int wk_fd;

static void
remove_wk_file(void)
{
    if (close(wk_fd) == -1)
        errMsg("close");
}

static void
no_exit(int sig)
{
}

int
main(int argc, char *argv[])
{
    struct request_msg req;
    struct response_msg resp;
    int server_id;
    ssize_t msg_len;
    struct sigaction sa;

    if (argc > 1 && strcmp(argv[1], "--help") == 0)
        usageErr("%s [seq-len...]\n", argv[0]);

    /* Open well known file */
    umask(0);
    wk_fd = open(WELL_KNOWN_FILE, O_RDONLY | S_IRUSR | S_IWUSR);
    if (wk_fd == -1)
        errExit("open");
        
    /* Get server's queue identifier */
    if (read(wk_fd, &server_id, sizeof(server_id)) != sizeof(server_id))
        errExit("read");

    if (atexit(remove_wk_file) != 0)
        errExit("atexit 1");
        
    /* Establish SIGALARM to avoid terminating process */
    if (sigfillset(&sa.sa_mask) == -1)
        errExit("sigfillset");
    sa.sa_flags = 0;
    sa.sa_handler = no_exit;
    if (sigaction(SIGALRM, &sa, NULL) == -1)
        errExit("sigaction");

    /* Construct request and send message */
    req.mtype = 1; 
    req.pid = getpid(); 
    req.seq_len = (argc > 1) ? getInt(argv[1], GN_GT_0, "seq-len") : 1;

    alarm(SVMSG_TIMEOUT);
    if (msgsnd(server_id, &req, REQ_MSG_SIZE, 0) == -1)
        errExit("Unable to send message through %d svmsg queue", server_id);

    /* Get response, which may be failure notification */
    msg_len = msgrcv(server_id, &resp, RESP_MSG_SIZE, getpid(), 0);
    if (msg_len == -1)
        errExit("Unable to receive message through %d svmsg queue", server_id);

    printf("%d\n", resp.seq_num);
    exit(EXIT_SUCCESS);
}