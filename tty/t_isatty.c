/* Exercise 62-1 */

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include "tlpi_hdr.h"

static int 
my_isatty(int fd)
{
	struct termios tp;

    /* Retrieve current terminal settings */
    if (tcgetattr(fd, &tp) == 0) return 1;
    return 0;
}

int
main(int argc, char *argv[])
{
	int fd;
	
	if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s device\n", argv[0]);
	
	fd = open(argv[1], O_RDONLY);
	if (fd == -1) {
		printf("Unable to open %s\n", argv[1]);
		return EXIT_FAILURE;
	}
	
	if (!my_isatty(fd)) {
		printf("%s is not a tty\n", argv[1]);
		return EXIT_FAILURE;
	}
	
	printf("%s is a tty\n", argv[1]);
	return EXIT_SUCCESS;
}