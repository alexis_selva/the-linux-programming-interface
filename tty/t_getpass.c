/* Exercice 62-3 */

#define _BSD_SOURCE
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE
#endif
#include <sys/capability.h>
#include <unistd.h>
#include <limits.h>
#include <pwd.h>
#include <shadow.h>
#include <termios.h>
#include "tlpi_hdr.h"

#define PASS_MAX 128

static char *
my_getpass (char *prompt)
{
  	struct termios fp, save;
  	static char password[PASS_MAX];
  	int nread;
  
  	/* Print the prompt */
  	printf("%s", prompt);
 	fflush(stdout);
 
  	/* Turn echoing off and fail if we can’t. */
  	if (tcgetattr (STDIN_FILENO, &save) != 0)
    	return NULL;
  	fp = save;
  	fp.c_lflag &= ~ECHO;
  	if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &fp) != 0)
    	return NULL;

  	/* Read the password. */
  	nread = read(STDIN_FILENO, password, PASS_MAX);
  	
  	/* Remove \n */
  	password[nread - 1] = '\0';
  	
  	/* Print carriage return */
  	printf("\n");

  	/* Restore terminal. */
  	if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &save) != 0)
    	return NULL;

  	return nread ? password : NULL;
}

int
main(int argc, char *argv[])
{
    char *username = argv[1];
    char *password, *encrypted, *p;
    struct passwd *pwd;
    struct spwd *spwd;
    Boolean authOk;
    size_t len;
    
    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s login\n", argv[0]);
    
    /* Purify username */
    len = strlen(username);
    if (username[len - 1] == '\n')
        username[len - 1] = '\0';  

    /* Look up password record for username */
    pwd = getpwnam(username);
    if (pwd == NULL)
        fatal("couldn't get password record");

    /* Look up shadow password record for username */
    spwd = getspnam(username);
    if (spwd == NULL && errno == EACCES)
        fatal("no permission to read shadow password file");

    /* Use the possible shadow password */
    if (spwd != NULL) 
        pwd->pw_passwd = spwd->sp_pwdp;     

    /* Request the password */
    password = my_getpass("Password: ");
    
    /* Encrypt password and erase cleartext version immediately */
    encrypted = crypt(password, pwd->pw_passwd);
    for (p = password; *p != '\0'; )
        *p++ = '\0';
    if (encrypted == NULL)
        errExit("crypt");

    /* Authentify the user */
    authOk = strcmp(encrypted, pwd->pw_passwd) == 0;
    if (!authOk) {
        printf("Incorrect password\n");
        exit(EXIT_FAILURE);
    }

    printf("Correct password\n");
    exit(EXIT_SUCCESS);
}
