/* Exercise 62-4 */

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include "tlpi_hdr.h"

static int
is_canon(int fd, cc_t *vmin, cc_t *vtime)
{
    struct termios t;

    if (tcgetattr(fd, &t) == -1)
        return -1;
    
    if (t.c_lflag & ICANON) {
    	*vmin = t.c_cc[VMIN];
    	*vtime = t.c_cc[VTIME];
    	return 1;
    }

    return 0;
}

int
main(int argc, char *argv[])
{
	int  fd;
	cc_t vmin; 
	cc_t vtime;
	
	if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s device\n", argv[0]);
	
	fd = open(argv[1], O_RDONLY);
	if (fd == -1) {
		printf("Unable to open %s\n", argv[1]);
		return EXIT_FAILURE;
	}
	
	if (!is_canon(fd, &vmin, &vtime)) {
		printf("%s is not in canonical mode\n", argv[1]);
		return EXIT_FAILURE;
	}
	
	printf("%s is in canonical mode: min = %u, time = %u\n", argv[1], vmin, vtime);
	return EXIT_SUCCESS;
}