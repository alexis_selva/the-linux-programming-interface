/* Exercice 47-6 */

#include <signal.h>
#include <fcntl.h>
#include "t_binary_sems.h"

/* Initialize semaphore */
int                     
init_sem(char *pathname)
{
	int fd;

  	/* Create well-known FIFO, and open it */
    umask(0);
    if (mkfifo(pathname, S_IRUSR | S_IWUSR | S_IWGRP) == -1 && errno != EEXIST)
        errExit("mkfifo %s", pathname);
    fd = open(pathname, O_RDWR);
    if (fd == -1)
        errExit("open %s", pathname);

    /* Let's find out about broken client pipe via failed write() */
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
        errExit("signal");
        
    return fd;
}

/* Reserve semaphore */
int                     
reserve_sem(int fd)
{
	char value;
	
	if (read(fd, &value, sizeof(char)) != sizeof(char))
    	return -1;
    
    return 0;
}

/* Reserve conditionnaly semaphore */
int                     
reserve_sem_NB(int fd)
{    
	int  old_flags;
	int  new_flags;
	char value;
	int  ret;
	
	old_flags = fcntl(fd, F_GETFL);
	new_flags = old_flags | O_NONBLOCK; 
    fcntl(fd, F_SETFL, new_flags); 
	
	ret = (read(fd, &value, sizeof(char)) != sizeof(char));
	
	fcntl(fd, F_SETFL, old_flags); 

    return ret;
}

/* Release semaphore */
int                     
release_sem(int fd)
{
	char value = 1;
	
    if (write(fd, &value, sizeof(char)) != sizeof(char))
    	return -1;
    	
    return 0;
}
