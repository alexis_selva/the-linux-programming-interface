/* Exercice 47-6 TODO */

#ifndef T_BINARY_SEMS_H
#define T_BINARY_SEMS_H

#include "tlpi_hdr.h"

int init_sem(char *pathname);

int reserve_sem(int fd);

int reserve_sem_NB(int fd);

int release_sem(int fd);

#endif /* T_BINARY_SEMS_H */
