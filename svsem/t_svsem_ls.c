/* Exercice 47-7 */

#define _GNU_SOURCE
#include <sys/sem.h>
#include "semun.h"
#include "tlpi_hdr.h"
#include "ugid_functions.h"

int
main(int argc, char *argv[])
{
    int             maxind;
    int             semid;
    int             ind;
    struct semid_ds ds;
    union semun     arg;

    /* Obtain size of kernel 'entries' array */
    arg.buf = &ds;
    maxind = semctl(0, 0, IPC_INFO, arg);
    if (maxind == -1)
        errExit("semctl-IPC_INFO");
    printf("maxind: %d\n\n", maxind);
    printf("index     id       owner\n");

    /* Retrieve and display information from each element of 'entries' array */
    for (ind = 0; ind <= maxind; ind++) {
        semid = semctl(ind, 0, SEM_STAT, arg);
        if (semid == -1) {
            if (errno != EINVAL && errno != EACCES)
                errMsg("semctl-SEM_STAT");              /* Unexpected error */
            continue;                                   /* Ignore this item */
        }
        printf("%d %13d %9s\n", ind, semid, userNameFromId(ds.sem_perm.uid));
    }

    exit(EXIT_SUCCESS);
}
