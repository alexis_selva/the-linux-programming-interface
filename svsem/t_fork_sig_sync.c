/* Exercice 47-2 */

#include <sys/sem.h>
#include <sys/stat.h>
#include "curr_time.h"
#include "tlpi_hdr.h"
#include "semun.h"
#include "binary_sems.h"

/* Permissions for our IPC objects */
#define OBJ_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)

/* Key for semaphore set */
#define SEM_KEY 0x5678    

int
main(int argc, char *argv[])
{
    pid_t childPid;
    union semun dummy;
    int semid;

    setbuf(stdout, NULL);               /* Disable buffering of stdout */
    
	/* Create semaphore */
    semid = semget(SEM_KEY, 1, IPC_CREAT | IPC_EXCL | OBJ_PERMS);
    if (semid == -1) 
    	errExit("shmget-SEM_KEY");
    if (initSemInUse(semid, 0) == -1)
        errExit("initSemInUse");

    switch (childPid = fork()) {
    case -1:
        errExit("fork");

    case 0: /* Child */

        /* Child does some required action here... */
        printf("[%s %ld] Child started - doing some work\n",
                currTime("%T"), (long) getpid());
        sleep(2);               /* Simulate time spent doing some work */

        /* And then signals parent that it's done */
        printf("[%s %ld] Child about to signal parent\n",
                currTime("%T"), (long) getpid());
        
        if (releaseSem(semid, 0) == -1)
 			errExit("reserveSem");

        /* Now child can do other things... */
        _exit(EXIT_SUCCESS);

    default: /* Parent */

        /* Parent may do some work here, and then waits for child to
           complete the required action */
        printf("[%s %ld] Parent about to wait for signal\n",
                currTime("%T"), (long) getpid());
		
		if (reserveSem(semid, 0) == -1)
 			errExit("reserveSem");
        
        printf("[%s %ld] Parent got signal\n", currTime("%T"), (long) getpid());
        
        if (releaseSem(semid, 0) == -1)
 			errExit("reserveSem");
        
        if (semctl(semid, 0, IPC_RMID, dummy) == -1)
        	errExit("semctl");

        /* Parent carries on to do other things... */
        exit(EXIT_SUCCESS);
    }
}
