/* Excercice 30-1 */

#include <pthread.h>
#include "tlpi_hdr.h"

static volatile int glob = 0;   

typedef struct thread_arg_t {
    int id;
    int loop;
} thread_arg_t;

static void *
threadFunc(void *arg)
{
    thread_arg_t *targ = (thread_arg_t *) arg;
    int loc, j;

    for (j = 0; j < targ->loop; j++) {
        printf("Thread %d: glob = %d\n", targ->id, glob);
        loc = glob;
        loc++;
        glob = loc;
    }

    return NULL;
}

int
main(int argc, char *argv[])
{
    int s;
    int loops = (argc > 1) ? getInt(argv[1], GN_GT_0, "num-loops") : 10000000;
    pthread_t t1, t2;
    thread_arg_t targ1 = { 0, loops };
    thread_arg_t targ2 = { 1, loops };
    
    s = pthread_create(&t1, NULL, threadFunc, &targ1);
    if (s != 0)
        errExitEN(s, "pthread_create");
    
    s = pthread_create(&t2, NULL, threadFunc, &targ2);
    if (s != 0)
        errExitEN(s, "pthread_create");

    s = pthread_join(t1, NULL);
    if (s != 0)
        errExitEN(s, "pthread_join");
    s = pthread_join(t2, NULL);
    if (s != 0)
        errExitEN(s, "pthread_join");

    printf("glob = %d\n", glob);
    exit(EXIT_SUCCESS);
}
