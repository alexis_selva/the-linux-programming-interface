/* Exercice 31-2 */

#include <pthread.h>
#include "tlpi_hdr.h"

static inline char *
my_basename(char *path) 
{
    char *slash = strrchr(path, '/');
    return slash ? slash + 1 : path;
}

static inline char *
my_dirname (char *path)
{
#define MAX_DIRNAME_LEN 256
    static __thread char newpath[MAX_DIRNAME_LEN];
    int length;
    char *slash = strrchr(path, '/');
    if (slash == 0) {
        path = "";
        length = 0;
    } else {
      length = slash - path + 1;
    }
    strncpy (newpath, path, length);
    newpath[length] = 0;
    return newpath;
}

static void *
threadFunc(void *arg)
{
    char str[] = "/home/hello/boy.cfg";
    char *dir  = my_dirname(str);
    char *base = my_basename(str);
    
    printf("Other thread: %s ==> %s + %s\n", str, dir, base);
    
    return NULL;
}

int
main(int argc, char *argv[])
{
    pthread_t t;
    int s;
    char str[] = "/home/bye/baby.txt";
    char *dir  = my_dirname(str);
    char *base = my_basename(str);
    
    s = pthread_create(&t, NULL, threadFunc, NULL);
    if (s != 0)
        errExitEN(s, "pthread_create");

    s = pthread_join(t, NULL);
    if (s != 0)
        errExitEN(s, "pthread_join");
    
    printf("Main thread: %s ==> %s + %s\n", str, dir, base);
    
    exit(EXIT_SUCCESS);
}