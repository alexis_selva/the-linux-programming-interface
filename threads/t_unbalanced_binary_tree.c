/* Excercice 30-2 */

#include <pthread.h>
#include "tlpi_hdr.h"

enum {
    ADD = 1,
    LOOKUP,
    DELETE,
    EXIT,
};

typedef struct action_t {
    unsigned char type;
    char         *pattern;
    size_t        value;
} action_t;

enum child_t {
    LEFT,
    RIGHT,
    MAX,
};

typedef struct node_t {
    char            *key;
    void            *value;
    struct node_t   *children[MAX];
    pthread_mutex_t  mtx;
} node_t;

typedef struct thread_arg_t {
    node_t **tree;
    char    *key;
    void    *value;
} thread_arg_t;

#define MAX_NB_THREADS 4
static pthread_t thread[MAX_NB_THREADS];

#define MAX_NB_ACTIONS 8192
static action_t actions[MAX_NB_ACTIONS];
static size_t index_read = 0;
static size_t index_write = 0;
static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

static node_t *tree = NULL;

static int
initialize(node_t **node)
{
    *node = NULL;
    return 0;
}

static int
compare(char *key1, char*key2)
{
    return strcmp(key1, key2);
}

static node_t *
add_node(node_t *node, char *key, void *value)
{
    int s;
    if (node == NULL) {
        node = malloc(sizeof(node_t));
        if (node == NULL) {
            return NULL;
        }
        node->key = key;
        node->value = value;
        bzero(node->children, MAX * sizeof(node_t *));
        s = pthread_mutex_init(&node->mtx, NULL);
        if (s != 0)
            errExitEN(s, "pthread_mutex_init");
        return node;
    }
    s = pthread_mutex_lock(&node->mtx);
    if (s != 0)
        errExitEN(s, "pthread_mutex_lock");
    int cmp = compare(key, node->key);
    if (cmp < 0) {
        node->children[LEFT] = add_node(node->children[LEFT], key, value);
    } else if (cmp > 0) {
        node->children[RIGHT] = add_node(node->children[RIGHT], key, value);
    } else {
        node->value = value;
    }
    s = pthread_mutex_unlock(&node->mtx);
    if (s != 0)
        errExitEN(s, "pthread_mutex_unlock");
    return node;
}

static int
add(node_t **root, char *key, void *value)
{
    *root = add_node(*root, key, value);
    return 0;
}

static node_t *
find_min(node_t *node) 
{
    if (node->children[LEFT] == NULL) {
        return node;
    } else {
        return find_min(node->children[LEFT]);
    }
}

static node_t *
delete_min(node_t *node)
{
    int s;
    s = pthread_mutex_lock(&node->mtx);
    if (s != 0)
        errExitEN(s, "pthread_mutex_lock");
    if (node->children[LEFT] == NULL) {
        s = pthread_mutex_unlock(&node->mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_unlock");
        return node->children[RIGHT];
    }  
    node->children[LEFT] = delete_min(node->children[LEFT]);
    s = pthread_mutex_unlock(&node->mtx);
    if (s != 0)
        errExitEN(s, "pthread_mutex_unlock");
    return node;
}

static node_t *
delete_node(node_t *node, char *key)
{
    int s;
    if (node == NULL) {
        return NULL;
    }
    s = pthread_mutex_lock(&node->mtx);
    if (s != 0)
        errExitEN(s, "pthread_mutex_lock");
    int cmp = compare(key, node->key);
    if (cmp < 0) {
        node->children[LEFT] = delete_node(node->children[LEFT], key);
        s = pthread_mutex_unlock(&node->mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_unlock");
    } else if (cmp > 0) {
        node->children[RIGHT] = delete_node(node->children[RIGHT], key);
        s = pthread_mutex_unlock(&node->mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_unlock");
    } else {
        node_t *tmp = node;
        if (node->children[RIGHT] == NULL) {
            node = node->children[LEFT];
        } else if (node->children[LEFT]  == NULL) {
            node = node->children[RIGHT];
        } else {
            node = find_min(tmp->children[RIGHT]);
            s = pthread_mutex_lock(&node->mtx);
            if (s != 0)
                errExitEN(s, "pthread_mutex_lock");
            node->children[RIGHT] = delete_min(tmp->children[RIGHT]);
            node->children[LEFT] = tmp->children[LEFT];
            s = pthread_mutex_unlock(&node->mtx);
            if (s != 0)
                errExitEN(s, "pthread_mutex_unlock");
        }
        s = pthread_mutex_unlock(&tmp->mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_unlock");
        s = pthread_mutex_destroy(&tmp->mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_destroy");
        free(tmp);
    }
    return node;
}

static int
delete(node_t **root, char *key)
{
    *root = delete_node(*root, key);
    return 0;
}

static int
lookup_node(node_t *node, char *key, void **value)
{
    int s, res;
    if (node == NULL) {
        return 0;
    }
    s = pthread_mutex_lock(&node->mtx);
    if (s != 0)
        errExitEN(s, "pthread_mutex_lock");
    int cmp = compare(key, node->key);
    if (cmp < 0) {
        res = lookup_node(node->children[LEFT], key, value);
    } else if (cmp > 0) {
        res = lookup_node(node->children[RIGHT], key, value);
    } else {
        *value = node->value;
        res = 1;
    }
    s = pthread_mutex_unlock(&node->mtx);
    if (s != 0)
        errExitEN(s, "pthread_mutex_unlock");
    return res;
}

static int
lookup(node_t **root, char *key, void **value)
{
    return lookup_node(*root, key, value);
}

static void *               
consume_action(void *arg)
{
    int s;
    void *value;
    
    for (;;) {
        s = pthread_mutex_lock(&mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_lock");
        while (index_write <= index_read) {
            s = pthread_cond_wait(&cond, &mtx);
            if (s != 0)
                errExitEN(s, "pthread_cond_wait");
        }
        while (index_write > index_read) {
            action_t *action = &actions[index_read % MAX_NB_ACTIONS];
            //printf("%d\n", action->type);
            switch (action->type) {
                case ADD:
                    add(&tree, action->pattern, (void *)action->value);
                    break;
                case LOOKUP:
                    if (lookup(&tree, action->pattern, &value) != action->value) {
                        errExitEN(s, "lookup");
                    }
                    break;
                case DELETE:
                    delete(&tree, action->pattern);
                    break;
                case EXIT:
                    index_read++;
                    s = pthread_mutex_unlock(&mtx);
                    if (s != 0)
                        errExitEN(s, "pthread_mutex_unlock");
                    return NULL;
                default:
                    errExit("unknown action");
                    break;
            }
            index_read++;
        }
        s = pthread_mutex_unlock(&mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_unlock");
    }
    return NULL;
}

static inline int
initialize_thread_pool(void) 
{
    int j, s;
    for (j = 0; j < MAX_NB_THREADS; j++) {
        s = pthread_create(&thread[j], NULL, consume_action, NULL);
        if (s != 0)
            errExitEN(s, "pthread_create");
    }
    return 0;     
}

static inline int
cleanup_thread_pool(void) 
{
    int j, s;
    for (j = 0; j < MAX_NB_THREADS; j++) {
        s = pthread_mutex_lock(&mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_lock");
        action_t *action = &actions[index_write % MAX_NB_ACTIONS];
        action->type = EXIT;
        action->pattern = NULL;
        action->value = 0;
        index_write++;
        s = pthread_mutex_unlock(&mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_unlock");
        s = pthread_cond_signal(&cond);
        if (s != 0)
            errExitEN(s, "pthread_cond_signal");
    }
    for (j = 0; j < MAX_NB_THREADS; j++) {
            s = pthread_join(thread[j], NULL);
        if (s != 0)
            errExitEN(s, "pthread_join");
    }
    return 0;     
}

static inline int
produce_action(unsigned char type, int argc, char *argv[], unsigned char value)
{
    int j, s;
    for (j = 1; j < argc; j++) {
        s = pthread_mutex_lock(&mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_lock");
        action_t *action = &actions[index_write % MAX_NB_ACTIONS];
        action->type = type;
        action->pattern = argv[j];
        action->value = value;
        index_write++;
        s = pthread_mutex_unlock(&mtx);
        if (s != 0)
            errExitEN(s, "pthread_mutex_unlock");
        s = pthread_cond_signal(&cond);
        if (s != 0)
            errExitEN(s, "pthread_cond_signal");
    }
    return 0;
}

int
main(int argc, char *argv[])
{
    initialize_thread_pool();
    initialize(&tree);
    produce_action(ADD, argc, argv, 0); 
    produce_action(LOOKUP, argc, argv, 1);
    produce_action(DELETE, argc, argv, 0);
    produce_action(LOOKUP, argc, argv, 0);
    cleanup_thread_pool();
    exit(EXIT_SUCCESS);
}