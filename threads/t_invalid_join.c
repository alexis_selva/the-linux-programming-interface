/* Exercide 29-1 */

#include <pthread.h>
#include "tlpi_hdr.h"

static void *
threadFunc(void *arg)
{
    int s;
    
    s = pthread_join(pthread_self(), NULL);
    if (s != 0)
        errExitEN(s, "pthread_join");
    
    return NULL;
}

int
main(int argc, char *argv[])
{
    pthread_t t1;
    void *res;
    int s;

    s = pthread_create(&t1, NULL, threadFunc, "Hello world\n");
    if (s != 0)
        errExitEN(s, "pthread_create");

    s = pthread_join(t1, &res);
    if (s != 0)
        errExitEN(s, "pthread_join");

    exit(EXIT_SUCCESS);
}
