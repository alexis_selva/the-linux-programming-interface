/* Exercice 33-2 */

#include <signal.h>
#include <pthread.h>
#include "signal_functions.h"  
#include "tlpi_hdr.h"

static volatile sig_atomic_t print_pending_signals[2];   

static void *
thread_function(void *arg)
{
    size_t id = (size_t) arg;
    sigset_t pending;
    
    while (!print_pending_signals[id]);
    if (sigpending(&pending) == -1)
        errExit("sigpending");
    printf("Thread %lu - pending signals are: \n", id);
    printSigset(stdout, "\t\t", &pending);

    return NULL;
}

int
main(int argc, char *argv[])
{
    pthread_t t1, t2;
    int s;
    sigset_t blockMask;

    sigfillset(&blockMask);
    if (sigprocmask(SIG_SETMASK, &blockMask, NULL) == -1)
            errExit("sigprocmask - SIG_SETMASK");

    s = pthread_create(&t1, NULL, thread_function, (void *) 0);
    if (s != 0)
        errExitEN(s, "pthread_create");
    s = pthread_create(&t2, NULL, thread_function, (void *) 1);
    if (s != 0)
        errExitEN(s, "pthread_create");
        
    s = pthread_kill(t1, SIGUSR1);    
    if (s != 0)
        errExitEN(s, "pthread_create");
    s = pthread_kill(t2, SIGUSR2);    
    if (s != 0)
        errExitEN(s, "pthread_create");
    
    print_pending_signals[0] = 1;
    s = pthread_join(t1, NULL);
    if (s != 0)
        errExitEN(s, "pthread_join");
    
    print_pending_signals[1] = 1;
    s = pthread_join(t2, NULL);
    if (s != 0)
        errExitEN(s, "pthread_join");

    exit(EXIT_SUCCESS);
}
