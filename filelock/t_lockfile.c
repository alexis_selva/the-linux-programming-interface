/* Exercice 55-10 */

#include <fcntl.h>
#include "tlpi_hdr.h"

#define NB_RETRIES_DEFAULT 	(-1)
#define TIMEOUT_DEFAULT 	(8)

static void
usage(const char *progName, const char *msg)
{
    if (msg != NULL)
        fprintf(stderr, "%s", msg);
    fprintf(stderr, "Usage: %s [-s sleeptime] [-r retries] file\n", progName);
    fprintf(stderr, "    -s           set sleeptime\n");
    fprintf(stderr, "    -r           set retries\n");
    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
	int fd;
	int opt;
	unsigned int i;
	int retries = NB_RETRIES_DEFAULT;
	int sleeptime = TIMEOUT_DEFAULT;

	while ((opt = getopt(argc, argv, "s:r:")) != -1) {
        switch (opt) {
        case 's':
            sleeptime = atoi(optarg);
            break;
		case 'r':
            retries = atoi(optarg);
            break;
        default:
            usage(argv[0], "Bad option\n");
        }
	}

	if (argc < 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file\n", argv[0]);
    
    /* Open file */
    fd = open(argv[optind], O_RDONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR);
    while (fd == -1 && i < retries) {
    	sleep(sleeptime);
    	fd = open(argv[1], O_RDONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR);
    	i++;
    }
	if (i == retries)
		fatal("giving up on %s", argv[optind]);
		
	exit(EXIT_SUCCESS);
}
