/* Exercice 55-5 a */

#include <sys/stat.h>
#include <fcntl.h>
#include "region_locking.h"             /* For lockRegion() */
#include "tlpi_hdr.h"

#define REGION_POSITION_MAX 	(80000)
#define REGION_LENGTH			(2)
#define SLEEP_LENGTH			(5)

int
main(int argc, char *argv[])
{
	int fd;
	int i;
	
	if (argc < 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file\n", argv[0]);
    
    /* Open file */
    fd = open(argv[1], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    if (fd == -1)
        errExit("Could not open PID file %s", argv[1]);

	/* Lock alternative position */
	for (i = 0 ; i < REGION_POSITION_MAX ; i+=2) {
    	if (lockRegion(fd, F_WRLCK, SEEK_SET, i, REGION_POSITION_MAX - i) == -1)
            errExit("Unable to lock PID file '%s'", argv[1]);
    }
    
    /* Sleep */
    sleep(SLEEP_LENGTH);

    return EXIT_SUCCESS;
}
