/* Exercice 55-5 b */

#include <sys/stat.h>
#include <fcntl.h>
#include "region_locking.h"             /* For lockRegion() */
#include "tlpi_hdr.h"

#define TRIES_MAX 				(10000)

int
main(int argc, char *argv[])
{
	int fd;
	int i;
	int pos;
	
	if (argc < 3 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file position\n", argv[0]);
    
    /* Open file */
    fd = open(argv[1], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    if (fd == -1)
        errExit("Could not open PID file %s", argv[1]);
    
    /* Determine position to lock */
    pos = getInt(argv[2], GN_NONNEG, "position");

	/* Try to lock the positon */
	for (i = 0 ; i < TRIES_MAX ; i++) {
    	if (lockRegion(fd, F_WRLCK, SEEK_SET, pos * 2, 1) == -1) {
    		if (errno != EAGAIN && errno != EACCES)
            	errExit("Unable to lock PID file '%s'", argv[1]);
    	}
    }
    
    return EXIT_SUCCESS;
}
