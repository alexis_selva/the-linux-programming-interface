/* Exercice 55-3 */

#include <sys/file.h>
#include <fcntl.h>
#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
    int fd1, new_fd1;
    int fd2;
    
    if (argc < 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file\n", argv[0]);

	/* Open file to be locked */
    fd1 = open(argv[1], O_RDONLY);               
    if (fd1 == -1)
        errExit("open");
    new_fd1 = dup(fd1);
    if (new_fd1 == -1)
        errExit("open");
    fd2 = open(argv[1], O_RDONLY);               
    if (fd2 == -1)
        errExit("open");

	/* Lock file */
    if (flock(fd1, LOCK_EX) == -1) {
        errExit("flock");
    }
    if (flock(new_fd1, LOCK_UN) == -1)
        errExit("flock");
    if (flock(fd2, LOCK_EX) == -1) {
        errExit("flock");
    }

	/* Unlock file */
    if (flock(fd1, LOCK_UN) == -1)
        errExit("flock");

    exit(EXIT_SUCCESS);
}
