/* Exercice 55-9 */

#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>
#include "region_locking.h"             /* For lockRegion() */
#include "tlpi_hdr.h"

#define REGION_1_START	(0)
#define REGION_1_END	(99)
#define REGION_2_START	(100)
#define REGION_2_END	(1999)

#define SYNC_SIG 		SIGUSR1                /* Synchronization signal */

/* Signal handler - does nothing but return */
static void             
handler(int sig)
{
}

int
main(int argc, char *argv[])
{
	int fd;
	pid_t childPid;
	sigset_t blockMask, origMask, emptyMask;
    struct sigaction sa;

	if (argc < 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file\n", argv[0]);
    
    /* Open file */
    fd = open(argv[1], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    if (fd == -1)
        errExit("Could not open PID file %s", argv[1]);
    
    /* Block signal */
    sigemptyset(&blockMask);
    sigaddset(&blockMask, SYNC_SIG);    
    if (sigprocmask(SIG_BLOCK, &blockMask, &origMask) == -1)
        errExit("sigprocmask");

	/* Register signal handler */
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    sa.sa_handler = handler;
    if (sigaction(SYNC_SIG, &sa, NULL) == -1)
        errExit("sigaction");

	/* Fork process */
	switch (childPid = fork()) {
	case -1:
        errExit("fork");

    case 0: /* Child */
    
        printf("child lock\n");
        if (lockRegionWait(fd, F_WRLCK, SEEK_SET, REGION_2_START, REGION_2_END - REGION_2_START))
        	errExit("child: lockRegionWait");
        	
        printf("child kill\n");
        if (kill(getppid(), SYNC_SIG) == -1)
            errExit("kill");
            
    	/* Jump the write offset */
    	printf("child lseek\n");
        if (lseek(fd, REGION_1_START, SEEK_SET) == -1)
        	errExit("child: lseek KO");

        /* Write data */
        printf("child write\n");
        if (write(fd, "c", 1) != 1)
            errExit("child: write KO");
        printf("child write OK\n");
        
        _exit(EXIT_SUCCESS);
    
    default: /* Parent */
    
        printf("parent lock\n");
        if (lockRegionWait(fd, F_WRLCK, SEEK_SET, REGION_1_START, REGION_1_END - REGION_1_START))
        	errExit("parent: lockRegionWait");
       
       	printf("parent suspend\n");
        sigemptyset(&emptyMask);
        if (sigsuspend(&emptyMask) == -1 && errno != EINTR)
            errExit("sigsuspend");

    	/* Jump the write offset */
    	printf("parent lseek\n");
        if (lseek(fd, REGION_2_START, SEEK_SET) == -1)
        	errExit("parent: lseek KO");

        /* Write data */
        printf("parent write\n");
        if (write(fd, "p", 1) != 1)
            errExit("parent: write KO");
        printf("parent write OK\n");

        printf("parent wait\n");
        wait(NULL);
         
        printf("parent exit\n");
		exit(EXIT_SUCCESS);
	}
}
