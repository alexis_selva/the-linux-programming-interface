/* Exercice 34-3 */

#define _GNU_SOURCE
#include <string.h>
#include "curr_time.h"
#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
    printf("[%s %ld] process: PPID=%ld, PGID=%ld, SID=%ld\n",
            currTime("%T"),
            (long) getpid(), (long) getppid(),
            (long) getpgrp(), (long) getsid(0));

    /* Change session ID of the process */
    printf("[%s %ld] process: change session ID\n",
            currTime("%T"), (long) getpid());
    if (setsid() == -1)
        errExit("setsid");
            
    printf("process exiting\n");
    exit(EXIT_SUCCESS);
}
