/* Exercice 34-2 */

#define _GNU_SOURCE
#include <string.h>
#include <signal.h>
#include "curr_time.h"
#include "tlpi_hdr.h"

static void
handler(int sig)
{
}

int
main(int argc, char *argv[])
{
    sigset_t blockMask, origMask, emptyMask;
    struct sigaction sa;
    pid_t childPid;
    
    /* Disable buffering of stdout */
    setbuf(stdout, NULL);               
 
    printf("[%s %ld] parent: PPID=%ld, PGID=%ld, SID=%ld\n",
            currTime("%T"),
            (long) getpid(), (long) getppid(),
            (long) getpgrp(), (long) getsid(0));

    sigemptyset(&blockMask);
    sigaddset(&blockMask, SIGUSR1);
    if (sigprocmask(SIG_BLOCK, &blockMask, &origMask) == -1)
        errExit("sigprocmask");
        
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    sa.sa_handler = handler;
    if (sigaction(SIGUSR1, &sa, NULL) == -1)
        errExit("sigaction");

    /* Create one child */
    childPid = fork();
    switch (childPid) {
    case -1:
        errExit("fork");

    case 0:         /* Child */
        printf("[%s %ld] child: PPID=%ld, PGID=%ld, SID=%ld\n",
                currTime("%T"),
                (long) getpid(), (long) getppid(),
                (long) getpgrp(), (long) getsid(0));
     
        /* Wait signal */
         printf("[%s %ld] Child: waits for parent signal\n", 
                currTime("%T"), (long) getpid());
        sigemptyset(&emptyMask);
        if (sigsuspend(&emptyMask) == -1 && errno != EINTR)
            errExit("sigsuspend");
        printf("[%s %ld] child: gets parent signal\n", currTime("%T"), (long) getpid());
     
        /* Signals parent² */
        printf("[%s %ld] child: signals parent\n",
                currTime("%T"), (long) getpid());
        if (kill(getppid(), SIGUSR1) == -1)
            errExit("kill");
     
        execlp("ls", "ls", "-l", argv[0], (char *) NULL);
       
        _exit(EXIT_SUCCESS);

    default:        /* Parent change parent process group id of its child */
        
        /* Change process group ID of the child */
        printf("[%s %ld] parent: change child PGID\n",
                currTime("%T"), (long) getpid());
        if (setpgid(childPid, childPid) == -1)
            errExit("setpgid");
            
        /* Signals child */
        printf("[%s %ld] parent: signals child\n",
                currTime("%T"), (long) getpid());
        if (kill(childPid, SIGUSR1) == -1)
            errExit("kill");
        
        /* Wait signal */
        printf("[%s %ld] parent: waits for child signal\n", 
                currTime("%T"), (long) getpid());
        sigemptyset(&emptyMask);
        if (sigsuspend(&emptyMask) == -1 && errno != EINTR)
            errExit("sigsuspend");
        printf("[%s %ld] parent: gets child signal\n", currTime("%T"), (long) getpid());

        /* Change process group ID of the child */
        sleep(3);
         printf("[%s %ld] parent: change child PGID\n",
                currTime("%T"), (long) getpid());
        if (setpgid(childPid, childPid) == -1)
            errExit("setpgid");
    }

    printf("parent exiting\n");
    exit(EXIT_SUCCESS);
}
