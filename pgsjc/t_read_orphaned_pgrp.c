/* Exercice 34-6 */

#define _GNU_SOURCE     /* Get declaration of strsignal() from <string.h> */
#include <string.h>
#include <signal.h>
#include "tlpi_hdr.h"

#define BUF_SIZE 1024

int
main(int argc, char *argv[])
{
    int numread;
    char buf[BUF_SIZE];
    
    printf("parent: PID=%ld, PPID=%ld, PGID=%ld, SID=%ld\n",
            (long) getpid(), (long) getppid(),
            (long) getpgrp(), (long) getsid(0));

    if (fork() != 0)            /* Exit if parent, or on error */
        _exit(EXIT_SUCCESS);
        
    printf("child: PID=%ld, PPID=%ld, PGID=%ld, SID=%ld\n",
            (long) getpid(), (long) getppid(),
            (long) getpgrp(), (long) getsid(0));

    numread = read(STDIN_FILENO, buf, BUF_SIZE);
    if (numread == -1)
        errExit("read");

    exit(EXIT_SUCCESS);                 /* And orphan them and their group */
}
