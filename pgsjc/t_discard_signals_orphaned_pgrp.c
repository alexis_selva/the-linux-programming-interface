/* Exercice 34-7 */

#define _GNU_SOURCE     /* Get declaration of strsignal() from <string.h> */
#include <string.h>
#include <signal.h>
#include "tlpi_hdr.h"

#define BUF_SIZE 1024

static void             /* Signal handler */
handler(int sig)
{
    printf("PID=%ld: caught signal %d (%s)\n", (long) getpid(),
            sig, strsignal(sig));       /* UNSAFE (see Section 21.1.2) */
}

int
main(int argc, char *argv[])
{
    struct sigaction sa;
   
    printf("parent: PID=%ld, PPID=%ld, PGID=%ld, SID=%ld\n",
            (long) getpid(), (long) getppid(),
            (long) getpgrp(), (long) getsid(0));

    if (fork() != 0)            /* Exit if parent, or on error */
        _exit(EXIT_SUCCESS);
        
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = handler;
    if (sigaction(SIGTTIN, &sa, NULL) == -1)
        errExit("sigaction");
    if (sigaction(SIGTSTP, &sa, NULL) == -1)
        errExit("sigaction");
    
    printf("child: PID=%ld, PPID=%ld, PGID=%ld, SID=%ld\n",
            (long) getpid(), (long) getppid(),
            (long) getpgrp(), (long) getsid(0));

    raise(SIGTTIN);
    raise(SIGTTOU);
    raise(SIGTSTP);
    
    alarm(60);              /* So we die if not SIGHUPed */
    printf("PID=%ld pausing\n", (long) getpid());
    pause();

    exit(EXIT_SUCCESS);                 /* And orphan them and their group */
}
