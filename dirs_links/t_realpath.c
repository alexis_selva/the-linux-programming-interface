/* Exercice 18-3 */

#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>  /* For definition of PATH_MAX */
#include "tlpi_hdr.h"

static inline char *
my_realpath(const char *path, char *resolved_path) {
    int fd = open(path, O_RDONLY);
    if (fd < 0) errExit("open");
    char proc_path[PATH_MAX];
    snprintf(proc_path, PATH_MAX, "/proc/%d/fd/%d", getpid(), fd);
    char buf[PATH_MAX];
    ssize_t length = readlink(proc_path, buf, PATH_MAX);
    if (length == -1) 
        errExit("readlink");
    buf[length] = '\0';
    close(fd);
    if (resolved_path == NULL) resolved_path = malloc(length);
    strncpy(resolved_path, buf, length);
    return resolved_path;
}

int
main(int argc, char *argv[])
{
    struct stat statbuf;
    char buf[PATH_MAX];
    
    /* Parse command line */
    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s pathname\n", argv[0]);

    /* Is it a symbolic link ? */
    if (lstat(argv[1], &statbuf) == -1)
        errExit("lstat");
    if (!S_ISLNK(statbuf.st_mode))
        fatal("%s is not a symbolic link", argv[1]);

    /* Evaluate the real path */
    if (my_realpath(argv[1], buf) == NULL)
        errExit("realpath");
    printf("realpath: %s --> %s\n", argv[1], buf);

    exit(EXIT_SUCCESS);
}
