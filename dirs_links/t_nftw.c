/*  Exercise 18-8 */

#define _GNU_SOURCE

#include <ftw.h>
#include <dirent.h>
#include <sys/stat.h>
#include "tlpi_hdr.h"
#include <limits.h>  /* For definition of PATH_MAX */

static int numReg = 0, numDir = 0, numSymLk = 0, numSocket = 0,
           numFifo = 0, numChar = 0, numBlock = 0,
           numNonstatable = 0;

static int
countFile(const char *path, const struct stat *sb, int flag, struct FTW *ftwb) {
    if (flag == FTW_NS) {
        numNonstatable++;
        return 0;
    }

    switch (sb->st_mode & S_IFMT) {
    case S_IFREG:  numReg++;    break;
    case S_IFDIR:  numDir++;    break;
    case S_IFCHR:  numChar++;   break;
    case S_IFBLK:  numBlock++;  break;
    case S_IFLNK:  numSymLk++;  break;
    case S_IFIFO:  numFifo++;   break;
    case S_IFSOCK: numSocket++; break;
    }
    return 0;           /* Always tell nftw() to continue */
}

static void
printStats(const char *msg, int num, int numFiles) {
    printf("%-15s   %6d %6.1f%%\n", msg, num, num * 100.0 / numFiles);
}

typedef int (*fn_t) (const char *fpath, 
                   const struct stat *sb,
                   int typeflag, 
                   struct FTW *ftwbuf);

static int 
my_nftw(const char *dirpath,
        fn_t fn,
        int nopenfd, 
        int flags) {
        
    DIR * dirp = opendir(dirpath);
    if (dirp == NULL)
        errExit("opendir");
        
    /* Scan entries under /proc directory */
    for (;;) {
        errno = 0;              /* To distinguish error from end-of-directory */
        struct dirent *entryp = readdir(dirp);
        if (entryp == NULL) {
            if (errno != 0)
                errExit("readdir");
            else
                break;
        }
        
        /* Skip . and .. */
        if (strcmp(entryp->d_name, "..") == 0)
            continue;
        
        /* Retrieve metadata */
        char next_path[PATH_MAX];
        snprintf(next_path, PATH_MAX, "%s/%s", dirpath, entryp->d_name);
        struct stat sb;
        int flag = entryp->d_type;
        if (lstat(next_path, &sb)) {
            flag = FTW_NS;
        }

        char skip_subtree = 0;
        if (strcmp(entryp->d_name, ".") == 0) {
            char current_path[PATH_MAX];
            if (getcwd(current_path, PATH_MAX) == NULL)
                errExit("getcwd");
            char real_dirpath[PATH_MAX];
            if (realpath(dirpath, real_dirpath) == NULL)
                errExit("realpath");
            if (strcmp(real_dirpath, current_path) != 0)
                continue;
            skip_subtree = 1;
        }
        
        /* Call fn */
        struct FTW ftwbuff = { 0 };
        if (fn) fn(next_path, &sb, flag, &ftwbuff);
        
        /* Recursive call the function */
        if ((!skip_subtree) && (entryp->d_type == DT_DIR)) {
            my_nftw(next_path, fn, nopenfd, flags);
        }
    }
    closedir(dirp);
    
    return 0;
}

int
main(int argc, char *argv[]) {
    
    /* Parse command line */
    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s dir-path\n", argv[0]);

    /* Traverse directory tree counting files; don't follow symbolic links */
    if (my_nftw(argv[1], &countFile, 20, FTW_PHYS) == -1) {
        perror("nftw");
        exit(EXIT_FAILURE);
    }

    /* Print stats */
    int numFiles = numReg + numDir + numSymLk + numSocket +
                numFifo + numChar + numBlock + numNonstatable;
    if (numFiles == 0) {
        printf("No files found\n");
    } else {
        printf("Total files:      %6d\n", numFiles);
        printStats("Regular:", numReg, numFiles);
        printStats("Directory:", numDir, numFiles);
        printStats("Char device:", numChar, numFiles);
        printStats("Block device:", numBlock, numFiles);
        printStats("Symbolic link:", numSymLk, numFiles);
        printStats("FIFO:", numFifo, numFiles);
        printStats("Socket:", numSocket, numFiles);
        printStats("Non-statable:", numNonstatable, numFiles);
    }
    exit(EXIT_SUCCESS);
}
