/* Exercice 18-5 */

#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>  /* For definition of PATH_MAX */
#include "tlpi_hdr.h"

static inline char *
my_getcwd(char *pwd, size_t size) {
    char proc_path[PATH_MAX];
    snprintf(proc_path, PATH_MAX, "/proc/%d/cwd", getpid());
    ssize_t length = readlink(proc_path, pwd, size);
    if (length == -1) 
        return NULL;
    return pwd;
}

int
main(int argc, char *argv[])
{
    char buf[PATH_MAX];

    /* Evaluate the current path */
    if (my_getcwd(buf, PATH_MAX) == NULL)
        errExit("getcwd");
    printf("getcwd --> %s\n", buf);

    exit(EXIT_SUCCESS);
}
