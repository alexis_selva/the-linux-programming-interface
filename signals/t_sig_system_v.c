/* Exercice 22-4 */

#include <signal.h>
#include "tlpi_hdr.h"

static int my_sighold(int sig) 
{
    sigset_t mask;
    
    if (sigprocmask(0, NULL, &mask) == -1) return -1;
    if (sigaddset(&mask, sig) == -1) return -1;
    if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1) return -1;
    
    return 0;
}

static int my_sigrelse(int sig) 
{
    sigset_t mask;
    
    if (sigprocmask(0, NULL, &mask) == -1) return -1;
    if (sigdelset(&mask, sig) == -1) return -1;
    if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1) return -1;
    
    return 0;
}

static int my_sigignore(int sig) 
{
    struct sigaction sa;
    
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = SIG_IGN;
    if (sigaction(sig, &sa, NULL) == -1) return -1;
    
    return 0;
}

static void (*my_sigset(int sig, void (*handler)(int)))(int) 
{
    struct sigaction sa, old_sa;
    sigset_t mask;
    
    if (sigprocmask(0, NULL, &mask) == -1) return SIG_ERR;
    int blocked = sigismember(&mask, sig);
    if (blocked == -1) return SIG_ERR;
    if (handler == SIG_HOLD) {
        if (sigaction(sig, NULL, &old_sa) == -1) return SIG_ERR;
        if (my_sighold(sig) == -1) return SIG_ERR;
    } else {   
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        sa.sa_handler = handler;
        if (sigaction(sig, &sa, &old_sa) == -1) return SIG_ERR;
        if (my_sigrelse(sig) == -1) return SIG_ERR;
    }
   
    return blocked ? SIG_HOLD : old_sa.sa_handler;
}

static int my_sigpause(int sig)
{
    sigset_t mask;
    
    if (sigprocmask(0, NULL, &mask) == -1) return -1;
    if (sigdelset(&mask, sig) == -1) return -1;
    if (sigsuspend(&mask) == -1) return -1;

    return 0;
}

static void
handler(int sig)
{
}

int
main(int argc, char *argv[])
{
    int numSigs, scnt;
    pid_t childPid;

    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s num-sigs\n", argv[0]);

    numSigs = getInt(argv[1], GN_GT_0, "num-sigs");

    if (my_sigset(SIGUSR1, handler) == SIG_ERR)
        errExit("sigset");

    /* Block the signal before fork(), so that the child doesn't manage
       to send it to the parent before the parent is ready to catch it */

    if (my_sighold(SIGUSR1) == -1)
        errExit("sighold 1");
    if (my_sighold(SIGUSR2) == -1)
        errExit("sighold 2");
    if (my_sigrelse(SIGUSR2) == -1)
        errExit("sigrelse");
    if (my_sigignore(SIGUSR2) == -1)
        errExit("sigignore");

    switch (childPid = fork()) {
        case -1: errExit("fork");

        case 0:     /* child */
            for (scnt = 0; scnt < numSigs; scnt++) {
                if (kill(getppid(), SIGUSR1) == -1)
                    errExit("kill 1");
                if (my_sigpause(SIGUSR1) == -1 && errno != EINTR)
                    errExit("sigpause");
            }
            exit(EXIT_SUCCESS);

        default: /* parent */
            for (scnt = 0; scnt < numSigs; scnt++) {
                if (my_sigpause(SIGUSR1) == -1 && errno != EINTR)
                    errExit("sigpause");
                if (kill(childPid, SIGUSR1) == -1)
                    errExit("kill 2");
            }
            exit(EXIT_SUCCESS);
    }
}
