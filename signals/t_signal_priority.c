/* Exercice 22-2 */

#define _GNU_SOURCE
#include <signal.h>
#include "tlpi_hdr.h"

static void
standard_handler(int sig)
{
    printf("\t standard handler (%d) is called.\n", sig);
}

static void
realtime_handler(int sig)
{
    printf("\t realtime handler (%d) is called.\n", sig);
}

int
main(int argc, char *argv[])
{
    /* Fill the proces mask */
    sigset_t mask;
    if (sigemptyset(&mask) == -1)
        errExit("sigemptyset");
    if (sigaddset(&mask, SIGUSR1) == -1)
        errExit("sigaddset 1"); 
    int realtime_signal = SIGRTMIN;
    if (sigaddset(&mask, realtime_signal) == -1)
        errExit("sigaddset 2");
        
    /* Block SIGCONT */
    if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1)
        errExit("sigprocmask 1");
    
    /* Specify an handler for SIGCONT */
    struct sigaction action = {
        .sa_handler = standard_handler,
        .sa_mask = mask,
        .sa_flags = 0,
    };
    sigaction(SIGUSR1, &action, NULL);
    action.sa_handler = realtime_handler;
    sigaction(realtime_signal, &action, NULL);
    
    /* Raise signals */
    printf("Raise realtime signal: %d\n", realtime_signal);
    raise(realtime_signal);

    printf("Raise standard signal: %d\n", SIGUSR1);
    raise(SIGUSR1);
    
    /* Unblock SIGCONT*/
    printf("Which handler is going to be called first ?\n");
    if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1)
        errExit("sigprocmask 2");

    exit(EXIT_SUCCESS);
}
