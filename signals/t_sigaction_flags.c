/* Exercice 20-3 */

#define _GNU_SOURCE
#include <signal.h>
#include "signal_functions.h"           /* Declaration of printSigset() */
#include "tlpi_hdr.h"

static void
handler_SIGUSR1(int sig) 
{
    printf("SIGUSR1 handler is triggered.\n");
}

static void
handler_SIGUSR2(int sig) 
{
    sigset_t proc_mask;
    if (sigprocmask(0, NULL, &proc_mask) == -1)
        return;
    if (sigismember(&proc_mask, SIGUSR2))
        printf("SIGUSR2 handler is triggered and it is blocked.\n");
    else
        printf("SIGUSR2 handler is triggered and it is not blocked.\n");
}

int
main(int argc, char *argv[])
{
    /* Test SA_RESETHAND flag */
    sigset_t emptyMask;
    if (sigemptyset(&emptyMask) == -1)
        errExit("sigemptyset");
    struct sigaction action = {
        .sa_handler = handler_SIGUSR1,
        .sa_mask = emptyMask,
        .sa_flags = SA_RESETHAND,
    };
    if (sigaction(SIGUSR1, &action, NULL) == -1)
        errExit("sigaction 1");
    if (raise(SIGUSR1) == -1)
        errExit("kill");
    if (sigaction(SIGUSR1, NULL, &action) == -1)
        errExit("sigaction 2");
    if (action.sa_handler == SIG_DFL)
        printf("SIGUSR1 handler was reset.\n");
    
    /* Test SA_NODEFER flag */
    action.sa_handler = handler_SIGUSR2;
    action.sa_mask = emptyMask;
    action.sa_flags = 0;
    if (sigaction(SIGUSR2, &action, NULL) == -1)
        errExit("sigaction 3");
    if (raise(SIGUSR2) == -1)
        errExit("kill");
    action.sa_handler = handler_SIGUSR2;
    action.sa_mask = emptyMask;
    action.sa_flags = SA_NODEFER;
    if (sigaction(SIGUSR2, &action, NULL) == -1)
        errExit("sigaction 4");
    if (raise(SIGUSR2) == -1)
        errExit("kill");
    
    exit(EXIT_SUCCESS);
}
