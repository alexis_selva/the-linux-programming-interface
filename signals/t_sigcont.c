/* Exercice 22-1 */

#define _GNU_SOURCE
#include <signal.h>
#include "tlpi_hdr.h"

static void
sigcont_handler(int sig)
{
    printf("SIGCONT handler is called.\n");
}

int
main(int argc, char *argv[])
{
    /* Fill the proces mask */
    sigset_t mask;
    if (sigemptyset(&mask) == -1)
        errExit("sigemptyset");
    if (sigaddset(&mask, SIGCONT) == -1)
        errExit("sigaddset"); 
        
    /* Block SIGCONT */
    if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1)
        errExit("sigprocmask 1");
    
    /* Specify an handler for SIGCONT */
    struct sigaction action = {
        .sa_handler = sigcont_handler,
        .sa_mask = mask,
        .sa_flags = 0,
    };
    sigaction(SIGCONT, &action, NULL);  
    
    /* Stop the process */
    printf("Type fg to Wake me up\n");
    raise(SIGTSTP);
    
    /* Unblock SIGCONT*/
    printf("Let's continue...\n");
    if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1)
        errExit("sigprocmask 2");

    exit(EXIT_SUCCESS);
}
