/* Exercice 21-1 */

#define _GNU_SOURCE
#include <signal.h>
#include "tlpi_hdr.h"

static void 
my_abort(void)
{
    /* Raise SIGABRT */
    raise(SIGABRT);
 
    /* Reset the handling of SIGABRT */
    sigset_t mask;
    sigemptyset(&mask);   
    struct sigaction action = {
        .sa_handler = SIG_DFL,
        .sa_mask = mask,
        .sa_flags = 0,
    };
    sigaction(SIGABRT, &action, NULL);  
    
    /* Remove blocked signals */
    sigprocmask(SIG_SETMASK, &mask, NULL);
 
    /* Raise SIGABRT */
    raise(SIGABRT);
 }

int
main(int argc, char *argv[])
{
    /* Fill the proces mask */
    sigset_t mask;
    if (sigfillset(&mask) == -1)
        errExit("sigfillset");
        
    /* Block any signals */
    if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1)
        errExit("sigprocmask");
    struct sigaction action = {
        .sa_handler = SIG_IGN,
        .sa_mask = mask,
        .sa_flags = 0,
    };
    
    /* Ignore SIGABRT */
    sigaction(SIGABRT, &action, NULL);
    
    /* Sleep a little bit */
    printf("Sleeping for few seconds before abort()\n");
    sleep(2);
    
    /* Try aborting */
    my_abort();
    
    /* Sleep again */
    printf("Sleeping for few seconds after abort()\n");
    sleep(3);
    printf("No abort ???\n");
    
    exit(EXIT_SUCCESS);
}
