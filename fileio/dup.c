/* aooend.c */
#include <sys/stat.h>
#include <fcntl.h>
#include "tlpi_hdr.h"

static inline int 
my_dup(int old_fd) {
    if (fcntl(old_fd, F_GETFD) == -1)  {
        errno = EBADF;
        return -1;
    }
    return fcntl(old_fd, F_DUPFD, 0);
}

static inline int 
my_dup2(int old_fd, int new_fd) {
    if (fcntl(old_fd, F_GETFD) == -1) {
        errno = EBADF;
        return -1;
    }
    if (old_fd == new_fd)
        return new_fd;
    if (fcntl(new_fd, F_GETFD) != -1) 
        close(new_fd);
    return fcntl(old_fd, F_DUPFD, new_fd);
}

static inline int 
test_dup(int fd1, int fd2, int expected_fd) {
    int flag1, flag2;
    off_t off1, off2;
    flag1 = fcntl(fd1, F_GETFD);
    flag2 = fcntl(fd2, F_GETFD);
    off1 = lseek(fd1, 10, SEEK_SET);
    off2 = lseek(fd2, 0, SEEK_CUR);
    
    return (fd2 == expected_fd) && (flag1 == flag2) && (off1 == off2);
}

int
main(int argc, char *argv[]) {
    int fd1, fd2, fd3;
    
    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file\n", argv[0]);

    fd1 = open(argv[1], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (fd1 == -1)
        errExit("open fd1");

    fd2 = my_dup(fd1);
    printf("Test %d %d %s\n", fd1, fd2, test_dup(fd1, fd2, 4) == 1 ? "OK" : "KO");
    fd3 = my_dup2(fd1, 2);
    printf("Test %d %d %s\n", fd1, fd3, test_dup(fd1, fd3, 2) == 1 ? "OK" : "KO");
    
    if (close(fd1) == -1)
        errExit("close fd1");
    if (close(fd2) == -1)
        errExit("close fd2");
    if (close(fd3) == -1)
        errExit("close fd3");

    exit(EXIT_SUCCESS);
}
