/* copy_sparse.c */
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "tlpi_hdr.h"

#ifndef _GNU_SOURCE
#define BUF_SIZE 1
static inline int
copy(int inputFd, int outputFd){
    ssize_t numRead = 0;
    char buf[BUF_SIZE];
    off_t offset = 0;
    
    while (numRead >= 0) {
        
        /* Read data */
        numRead = read(inputFd, buf, BUF_SIZE);
        switch (numRead) {

            case BUF_SIZE:            
                if (buf[0]) {
                
                    /* Jump the write offset */
                    if (lseek(outputFd, offset, SEEK_SET) == -1)
                        fatal("couldn't jump after the hole");
                
                    /* Write data */
                    if (write(outputFd, buf, BUF_SIZE) != 1)
                        fatal("couldn't write whole buffer");
                }
                break;
                
            case 0:
                return numRead;
                
            case -1:
                errExit("read");
        }
        offset++;
    }
    
    return numRead;
}
#else
#define BUF_SIZE 1024
static inline int
copy(int inputFd, int outputFd){
    ssize_t numRead = 0;
    char buf[BUF_SIZE];
    off_t start, end = 0;
    
    while (start >= 0) {
        
        /* Determine the position of data */
        start = lseek(inputFd, end, SEEK_DATA);
        end = lseek(inputFd, start, SEEK_HOLE);

        /* Read data */
        while (end > start) {
            
            /* Set the read offset */
            if (lseek(inputFd, start, SEEK_SET) == -1)
                fatal("couldn't re-jump before the data to read");
                
            /* Read data */
            numRead = read(inputFd, buf, BUF_SIZE);
            if (numRead) {

                /* Jump the write offset */
                if (lseek(outputFd, start, SEEK_SET) == -1)
                    fatal("couldn't jump before the data to write");
                
                /* Write data */
                if (write(outputFd, buf, numRead) != numRead)
                    fatal("couldn't write whole buffer");

                start += numRead;
            }
        }
    }
    
    return numRead;
}
#endif /* _GNU_SOURCE */

int
main(int argc, char *argv[]) {
    int inputFd, outputFd, openFlags;
    mode_t filePerms;

    if (argc != 3 || strcmp(argv[1], "--help") == 0)
        usageErr("%s old-file new-file\n", argv[0]);

    /* Open input and output files */
    inputFd = open(argv[1], O_RDONLY);
    if (inputFd == -1)
        errExit("opening file %s", argv[1]);

    openFlags = O_CREAT | O_WRONLY | O_TRUNC;
    filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
                S_IROTH | S_IWOTH;      /* rw-rw-rw- */
    outputFd = open(argv[2], openFlags, filePerms);
    if (outputFd == -1)
        errExit("opening file %s", argv[2]);

    /* Copy input data to output data */
    copy(inputFd, outputFd);

    if (close(inputFd) == -1)
        errExit("close input");
    if (close(outputFd) == -1)
        errExit("close output");

    exit(EXIT_SUCCESS);
}
