/* aooend.c */
#include <sys/stat.h>
#include <fcntl.h>
#include "tlpi_hdr.h"

int
main(int argc, char *argv[]) {
    int fd;

    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file\n", argv[0]);

    /* Open output file */
    fd = open(argv[1], O_WRONLY | O_APPEND, S_IRUSR | S_IWUSR);
    if (fd == -1)
        errExit("opening file %s", argv[1]);

    /* Write output data */
    if (lseek(fd, 0, SEEK_SET) == -1)
        errExit("lseek");
    if (write(fd, "test", 4) == -1)
        errExit("write");

    if (close(fd) == -1)
        errExit("close output");

    exit(EXIT_SUCCESS);
}
