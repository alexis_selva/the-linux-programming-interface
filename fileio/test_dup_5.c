/* aooend.c */
#include <sys/stat.h>
#include <fcntl.h>
#include "tlpi_hdr.h"

static inline int 
test_dup(int fd1, int fd2) {
    int flag1, flag2;
    off_t off1, off2;
    flag1 = fcntl(fd1, F_GETFD);
    flag2 = fcntl(fd2, F_GETFD);
    off1 = lseek(fd1, 10, SEEK_SET);
    off2 = lseek(fd2, 0, SEEK_CUR);
    
    return (flag1 == flag2) && (off1 == off2);
}

int
main(int argc, char *argv[]) {
    int fd1, fd2;

    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file\n", argv[0]);

    fd1 = open(argv[1], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (fd1 == -1)
        errExit("open fd1");
    fd2 = dup(fd1);
    if (fd2 == -1)
        errExit("open fd2");
    
    printf("Test %s\n", test_dup(fd1, fd2) == 1 ? "OK" : "KO");

    if (close(fd1) == -1)
        errExit("close fd1");
    if (close(fd2) == -1)
        errExit("close fd2");

    exit(EXIT_SUCCESS);
}
