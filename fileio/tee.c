/* tee.c */
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "tlpi_hdr.h"

#ifndef BUF_SIZE        /* Allow "cc -D" to override definition */
#define BUF_SIZE 1024
#endif

static void             /* Print "usage" message and exit */
usageError(char *progName, char *msg, int opt)
{
    fprintf(stderr, "Usage: %s [-a] file\n", progName);
    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[]) {
    int outputFd, openFlags;
    mode_t filePerms;
    ssize_t numRead;
    char buf[BUF_SIZE];
    int opt;
    char option_append = 0;
    
    while ((opt = getopt(argc, argv, ":a")) != -1) {
        switch (opt) {
            case 'a': 
                option_append = 1; 
                break;
            case ':': 
                usageError(argv[0], "Missing argument", optopt);
            case '?': 
                usageError(argv[0], "Unrecognized option", optopt);
            default:  
                fatal("Unexpected case in switch()");
        }
    }

    /* Open output file */
    if (option_append)
        openFlags = O_CREAT | O_WRONLY | O_APPEND;
    else
        openFlags = O_CREAT | O_WRONLY | O_TRUNC;
    filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
                S_IROTH | S_IWOTH;      /* rw-rw-rw- */
    outputFd = open(argv[optind], openFlags, filePerms);
    if (outputFd == -1)
        errExit("opening file %s", argv[optind]);

    /* Transfer data until we encounter end of input or an error */
    while ((numRead = read(STDIN_FILENO, buf, BUF_SIZE)) > 0) {
        if (write(outputFd, buf, numRead) != numRead)
            fatal("couldn't write whole buffer");
        if (write(STDOUT_FILENO, buf, numRead) != numRead) 
            fatal("couldn't write whole buffer");     
    }   
    
    if (numRead == -1)
        errExit("read");
    if (close(outputFd) == -1)
        errExit("close output");

    exit(EXIT_SUCCESS);
}
