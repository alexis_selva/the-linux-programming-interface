/* readv_writev.c */
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <limits.h>
#include "tlpi_hdr.h"

static inline int 
my_readv(int fd, const struct iovec *iov, int iovcnt) {
    size_t size = 0, numRead = 0, off = 0;
    int i = 0;
    char * buffer = NULL;
    
    if ((iovcnt < 0) || (iovcnt > IOV_MAX)) {
        errno = EINVAL;
        return -1;
    }

    for ( ; i < iovcnt ; i++) {
        if (size > size + iov[i].iov_len) {
            errno = EINVAL;
            return -1;
        }
        size += iov[i].iov_len;
    }
    
    buffer = malloc(size);
    if (buffer == NULL)
        return -1;
    
    numRead = read(fd, buffer, size);
    if (numRead == -1) {
        free(buffer);
        return numRead;
    }

    i = 0;
    while (off < numRead) {
        memcpy(iov[i].iov_base, buffer + off, iov[i].iov_len);
        off += iov[i++].iov_len;
    }
    
    free(buffer);
    
    return numRead;
}

static inline int 
my_writev(int fd, const struct iovec *iov, int iovcnt) {
    size_t size = 0, numWrite = 0, off = 0;
    int i = 0;
    char * buffer = NULL;
    
    if ((iovcnt < 0) || (iovcnt > IOV_MAX)) {
        errno = EINVAL;
        return -1;
    }

    for ( ; i < iovcnt ; i++) {
        if (size > size + iov[i].iov_len) {
            errno = EINVAL;
            return -1;
        }
        size += iov[i].iov_len;
    }
    
    buffer = malloc(size);
    if (buffer == NULL)
        return -1;

    for (i = 0 ; i < iovcnt ; i++) {
        memcpy(buffer + off, iov[i].iov_base, iov[i].iov_len);
        off += iov[i].iov_len;
    }
    
    numWrite = write(fd, buffer, size);
    free(buffer);
    
    return numWrite;
}

int
main(int argc, char *argv[])
{
    int fd;
    struct iovec iov[3];
    struct stat myStruct;       /* First buffer */
    int x;                      /* Second buffer */
#define STR_SIZE 100
    char str[STR_SIZE];         /* Third buffer */
    ssize_t numRead, numWrite, totRequired;

    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s file\n", argv[0]);

    fd = open(argv[1], O_RDWR);
    if (fd == -1)
        errExit("open");

    totRequired = 0;
    iov[0].iov_base = &myStruct;
    iov[0].iov_len = sizeof(struct stat);
    totRequired += iov[0].iov_len;
    iov[1].iov_base = &x;
    iov[1].iov_len = sizeof(x);
    totRequired += iov[1].iov_len;
    iov[2].iov_base = str;
    iov[2].iov_len = STR_SIZE;
    totRequired += iov[2].iov_len;

    numRead = my_readv(fd, iov, 3);
    if (numRead == -1)
        errExit("my_readv");
    printf("total bytes requested: %ld; bytes read: %ld\n",
            (long) totRequired, (long) numRead);

    numWrite = my_writev(fd, iov, 3);
    if (numWrite == -1)
        errExit("my_writev");
    printf("total bytes requested: %ld; bytes written: %ld\n",
            (long) totRequired, (long) numWrite);

    
    if (close(fd) == -1)
        errExit("close fd");
    
    exit(EXIT_SUCCESS);
}
