/* getpwnam.c */
#include <pwd.h>
#include "tlpi_hdr.h"

static inline struct passwd *
my_getpwnam(const char *name) {
    if (name == NULL)
        return NULL;
    ssize_t len = strlen(name);
    struct passwd *pwd = NULL;
    while ((pwd = getpwent())) {
        if ((len == strlen(pwd->pw_name)) && (strncmp(name, pwd->pw_name, len) == 0)) {
            return pwd;
        }
    }
    endpwent();
    
    return NULL;
}

int
main(int argc, char *argv[]) {
    struct passwd *result = NULL;

    if (argc != 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s username\n", argv[0]);

    result = my_getpwnam(argv[1]);
    if (result != NULL)
        printf("Name: %s\n", result->pw_gecos);
    else
        printf("Not found\n");

    exit(EXIT_SUCCESS);
}
