/* Exercice 44-2 */

#include <ctype.h>
#include <limits.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include "print_wait_status.h"          /* For printWaitStatus() */
#include "tlpi_hdr.h"

#define POPEN_FMT "/bin/ls -d %s 2> /dev/null"
#define PAT_SIZE 50
#define PCMD_BUF_SIZE (sizeof(POPEN_FMT) + PAT_SIZE)

#define MAX_CHILD 0xFF

static pid_t childs[MAX_CHILD];

static FILE *
my_popen(const char *cmd, const char *type)
{
    int pfd[2];
    pid_t child;
    int unused;
    int selected;
    
    /* Create a pipe */
    if (pipe(pfd) == -1)
        errExit("pipe");
        
    /* Create a child */
    child = fork();
    switch (child) {
    case -1:
        errExit("fork");

    case 0: 
        unused = (*type == 'r') ? 0 : 1;
        selected = (unused + 1) % 2;
        
        /* Close unused end */
        if (close(pfd[unused]) == -1)
            errExit("close - child");
            
        /* Duplicate stdout on write end of pipe */
        if (pfd[selected] != STDOUT_FILENO) {              
        
            /* Defensive check */
            if (dup2(pfd[selected], STDOUT_FILENO) == -1)
                errExit("dup2 1");
            if (close(pfd[selected]) == -1)
                errExit("close 2");
        }
        
        /* Run command */
        system(cmd);
        _exit(EXIT_SUCCESS);

    default:
        unused = (*type == 'r') ? 1 : 0;
        selected = (unused + 1) % 2;
        
        /* Close unused end */
        if (close(pfd[unused]) == -1)
            errExit("close - parent");
        
        /* Store child PID */
        childs[pfd[selected]] = child;
        
        /* Convert write end file descriptor */
        return fdopen(pfd[selected], type);
    }
}

static int 
my_pclose(FILE *stream)
{
    int status;
    int fd;
    
    /* Convert stream */
    fd = fileno(stream);
    if (fd < 0)
        errExit("fileno");
        
    /* Wait child */
    if (waitpid(childs[fd], &status, 0) < 0) 
        errExit("waitpid");
        
    return status;
}

int
main(int argc, char *argv[])
{
    char pat[PAT_SIZE];                 /* Pattern for globbing */
    char popenCmd[PCMD_BUF_SIZE];
    FILE *fp;                           /* File stream returned by popen() */
    Boolean badPattern;                 /* Invalid characters in 'pat'? */
    int len, status, fileCnt, j;
    char pathname[PATH_MAX];

    for (;;) {                  /* Read pattern, display results of globbing */
        printf("pattern: ");
        fflush(stdout);
        if (fgets(pat, PAT_SIZE, stdin) == NULL)
            break;                      /* EOF */
        len = strlen(pat);
        if (len <= 1)                   /* Empty line */
            continue;

        if (pat[len - 1] == '\n')       /* Strip trailing newline */
            pat[len - 1] = '\0';

        /* Ensure that the pattern contains only valid characters,
           i.e., letters, digits, underscore, dot, and the shell
           globbing characters. (Our definition of valid is more
           restrictive than the shell, which permits other characters
           to be included in a filename if they are quoted.) */

        for (j = 0, badPattern = FALSE; j < len && !badPattern; j++)
            if (!isalnum((unsigned char) pat[j]) &&
                    strchr("_*?[^-].", pat[j]) == NULL)
                badPattern = TRUE;

        if (badPattern) {
            printf("Bad pattern character: %c\n", pat[j - 1]);
            continue;
        }

        /* Build and execute command to glob 'pat' */

        snprintf(popenCmd, PCMD_BUF_SIZE, POPEN_FMT, pat);

        fp = my_popen(popenCmd, "r");
        if (fp == NULL) {
            printf("popen() failed\n");
            continue;
        }

        /* Read resulting list of pathnames until EOF */

        fileCnt = 0;
        while (fgets(pathname, PATH_MAX, fp) != NULL) {
            printf("%s", pathname);
            fileCnt++;
        }

        /* Close pipe, fetch and display termination status */

        status = my_pclose(fp);
        printf("    %d matching file%s\n", fileCnt, (fileCnt != 1) ? "s" : "");
        printf("    pclose() status = %#x\n", (unsigned int) status);
        if (status != -1)
            printWaitStatus("\t", status);
    }

    exit(EXIT_SUCCESS);
}
