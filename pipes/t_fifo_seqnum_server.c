/* Exercice 44-3 & exercice 44-4 */

#include <signal.h>
#include "fifo_seqnum.h"

static volatile sig_atomic_t end;

static void
handler(int sig)
{
    end = 1;
}

int
main(int argc, char *argv[])
{
    int backupFd, serverFd, dummyFd, clientFd;
    char clientFifo[CLIENT_FIFO_NAME_LEN];
    struct request req;
    struct response resp;
    int seqNum = 0;
    sigset_t mask;
    struct sigaction action;

    /* Define signal handler */
    if (sigfillset(&mask) == -1)
        errExit("sigfillset");
    action.sa_handler = handler;
    action.sa_mask = mask;
    action.sa_flags = 0;
    if (sigaction(SIGINT, &action, NULL) != 0)
        errExit("sigaction");
    if (sigaction(SIGTERM, &action, NULL) != 0)
        errExit("sigaction");
    
    /* Let's find out about broken client pipe via failed write() */
    action.sa_handler = SIG_IGN;
    if (sigaction(SIGPIPE, &action, NULL) != 0)
        errExit("sigaction");
    
    umask(0);

    /* Create backup file */
    backupFd = open("/tmp/t_fifo_seqnum_server.mem", 
                    O_RDWR | O_CREAT | O_SYNC,
                    S_IRUSR | S_IWUSR);
    if (backupFd == -1)
        errExit("open");
    
    if (read(backupFd, &seqNum, sizeof(seqNum)) == -1)
        errExit("read");

    /* Create well-known FIFO, and open it for reading */
    if (mkfifo(SERVER_FIFO, S_IRUSR | S_IWUSR | S_IWGRP) == -1
            && errno != EEXIST)
        errExit("mkfifo %s", SERVER_FIFO);
    serverFd = open(SERVER_FIFO, O_RDONLY);
    if (serverFd == -1)
        errExit("open %s", SERVER_FIFO);

    /* Open an extra write descriptor, so that we never see EOF */
    dummyFd = open(SERVER_FIFO, O_WRONLY);
    if (dummyFd == -1)
        errExit("open %s", SERVER_FIFO);

    /* Main loop */
    while (end == 0) {
        
        /* Read requests and send responses */
        if (read(serverFd, &req, sizeof(struct request))
                != sizeof(struct request)) {
            fprintf(stderr, "Error reading request; discarding\n");
            continue;                   /* Either partial read or error */
        }

        /* Open client FIFO (previously created by client) */
        snprintf(clientFifo, CLIENT_FIFO_NAME_LEN, CLIENT_FIFO_TEMPLATE,
                (long) req.pid);
        clientFd = open(clientFifo, O_WRONLY);
        if (clientFd == -1) {           /* Open failed, give up on client */
            errMsg("open %s", clientFifo);
            continue;
        }

        /* Send response and close FIFO */
        resp.seqNum = seqNum;
        if (write(clientFd, &resp, sizeof(struct response))
                != sizeof(struct response))
            fprintf(stderr, "Error writing to FIFO %s\n", clientFifo);
        if (close(clientFd) == -1)
            errMsg("close");

        /* Update our sequence number */
        seqNum += req.seqLen;           
    }
    
    /* Backup seqNum */
    if (lseek(backupFd, 0, SEEK_SET) == -1)
        errExit("lseek");
    if (write(backupFd, &seqNum, sizeof(seqNum)) != sizeof(seqNum))
        errExit("write");

    if (close(dummyFd) == -1)
        errMsg("close");
    if (close(serverFd) == -1)
        errMsg("close");
    unlink(SERVER_FIFO);
    if (close(backupFd) == -1)
        errMsg("close");
}
