/* Exercice 44-7 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "tlpi_hdr.h"

#define SERVER_FIFO     "/tmp/receiver_fifo"
#define PAYLOAD_SIZE    (256)

enum {
    NON_BLOCKING_OPTION_OPEN,
    NON_BLOCKING_OPTION_IO,
};

static inline void
usage(char *progName, char *msg, int opt)
{
    if (msg != NULL)
        fprintf(stderr, "%s\n", msg);
    fprintf(stderr, "Usage: %s [-o] [-i]\n", progName);
    fprintf(stderr, "-o: nonblocking open\n");
    fprintf(stderr, "-i: nonblocking I/O operation\n");
    exit(EXIT_FAILURE);
}

static inline void
set_non_block_option(unsigned char *options, unsigned char which)
{
    *options |= 1 << which;
}

static inline unsigned char
get_non_block_option(unsigned char options, unsigned char which)
{
    return options & 1 << which;
}

static inline void
read_options(int argc, char *argv[], unsigned char *options)
{
    int opt;
    while ((opt = getopt(argc, argv, "oi")) != -1) {
        switch (opt) {
        case 'o': set_non_block_option(options, NON_BLOCKING_OPTION_OPEN); break;
        case 'i': set_non_block_option(options, NON_BLOCKING_OPTION_IO); break;
        case ':': usage(argv[0], "Missing argument", optopt);
        case '?': usage(argv[0], "Unrecognized option", optopt);
        default:  fatal("Unexpected case in switch()");
        }
    }
}

static inline void
create_fifo()
{
    umask(0);
    if (mkfifo(SERVER_FIFO, S_IRUSR | S_IWUSR | S_IWGRP) == -1
            && errno != EEXIST)
        errExit("mkfifo %s", SERVER_FIFO);
}   

static inline void
manage_non_block_io(int fd, unsigned char options)
{
    int flags;
    unsigned char non_block_open;
    unsigned char non_block_io;
    
    non_block_open = get_non_block_option(options, NON_BLOCKING_OPTION_OPEN);
    non_block_io = get_non_block_option(options, NON_BLOCKING_OPTION_IO);
    if (non_block_open && non_block_io) 
        return;
    
    /* Fetch open files status flags */
    flags = fcntl(fd, F_GETFL); 
    
    /* Manage file status flags */
    if (non_block_io) {
        flags |= O_NONBLOCK; 
    } else {
        flags &= ~O_NONBLOCK;
    }
    
    /* Update open files status flags */
    fcntl(fd, F_SETFL, flags); 
}