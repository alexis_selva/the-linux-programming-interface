/* Exercice 44-7 */

#include "t_fifo_sender_receiver.h"

int
main(int argc, char *argv[])
{
    unsigned char options;
    int flags;
    int serverFd;
    char payload[PAYLOAD_SIZE];
    int res;

    /* Read command line paramters */
    options = 0;
    read_options(argc, argv, &options);
    
    /* Create FIFO */
    create_fifo();
    
    /* Open server FIFO */
    flags = O_RDONLY;
    flags |= get_non_block_option(options, NON_BLOCKING_OPTION_OPEN) ? O_NONBLOCK : 0;
    serverFd = open(SERVER_FIFO, flags);
    if (serverFd == -1)
        errExit("open %s", SERVER_FIFO);
        
    /* Receive a message */
    bzero(payload, PAYLOAD_SIZE);
    manage_non_block_io(serverFd, options);  
    res = read(serverFd, payload, PAYLOAD_SIZE);
    if (res != PAYLOAD_SIZE)
        errExit("read %s (%d)", SERVER_FIFO, res);
    printf("Message received: %s\n", payload);
    
    if (close(serverFd) == -1)
        errMsg("close");
    unlink(SERVER_FIFO);
    
    exit(EXIT_SUCCESS);
}
