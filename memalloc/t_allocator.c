/* Exercice 7-2 */

#include "tlpi_hdr.h"

typedef struct free_block_t {
    size_t length;
    struct free_block_t *next;
    struct free_block_t *prev;
    unsigned char data[];
} free_block_t;

static free_block_t *free_list;

static inline free_block_t *
allocate_free_block(size_t n) {
    free_block_t *node = (free_block_t *) sbrk(sizeof(free_block_t) + n);
    if (node == (void *) -1) return NULL;
    node->length = n;
    return node;
}

static inline int 
add_free_block(free_block_t *node) {
    node->prev = NULL;
    if (free_list == NULL) {
        node->next = NULL;
        free_list = node;
        return 0;
    }
    node->next = free_list;
    free_list = node;
    return 0;
}

static inline free_block_t *
get_free_block(size_t n) {
    free_block_t *node = free_list;
    while (node) {
        if (node->length >= n) return node;
        node = node->next;
    }
    return node;
}

static inline free_block_t *
del_free_block(free_block_t *node) {
    if (node->next) {
        node->next->prev = node->prev;
    }
    if (node->prev) {
        node->prev->next = node->next;
    } else {
        free_list = node->next;
    }
    return node;
}

static inline void*
my_malloc(size_t n) {
    free_block_t *node = get_free_block(n);
    if (node == NULL) {
        node = allocate_free_block(n);
        if (node == NULL) return NULL;
    } else {
        del_free_block(node);
    }
    size_t address = (size_t) node;
    return (void *) (address + sizeof(size_t));
}

static inline void*
my_free(void *ptr) {
    size_t address = (size_t) ptr;
    free_block_t *node = (free_block_t *)(address - sizeof(size_t));
    add_free_block(node);
    return NULL;
}

static inline char *
my_strndup(const char *s, size_t n) {
    char *ptr = my_malloc(n + 1);
    if (ptr == NULL) {
        return ptr;
    }
    return strncpy(ptr, s, n + 1);
}


int
main(int argc, char *argv[])
{
    char *val_1 = NULL;
    char *val_2 = NULL;
    char *val_3 = NULL;
    char *val_4 = NULL;
    char *val_5 = NULL;
    
    val_1 = my_strndup("a", sizeof("a") - 1);
    printf("%s\n", val_1);
    val_1 = my_free(val_1);
 
    val_2 = my_strndup("ba", sizeof("ba") - 1);
    val_1 = my_strndup("a", sizeof("a") - 1);
    printf("%s\n", val_1);
    printf("%s\n", val_2);
    val_1 = my_free(val_1);
    val_2 = my_free(val_2);
    
    val_3 = my_strndup("cba", sizeof("cba") - 1);
    val_1 = my_strndup("a", sizeof("a") - 1);
    val_2 = my_strndup("ba", sizeof("ba") - 1);
    printf("%s\n", val_1);
    printf("%s\n", val_2);
    printf("%s\n", val_3);
    val_2 = my_free(val_2);
    val_3 = my_free(val_3);
    val_1 = my_free(val_1);
    
    val_4 = my_strndup("dcba", sizeof("dcba") - 1);
    val_2 = my_strndup("ba", sizeof("ba") - 1);
    val_1 = my_strndup("a", sizeof("a") - 1);
    val_3 = my_strndup("cba", sizeof("cba") - 1);
    printf("%s\n", val_1);
    printf("%s\n", val_2);
    printf("%s\n", val_3);
    printf("%s\n", val_4);
    val_3 = my_free(val_3);
    val_1 = my_free(val_1);
    val_2 = my_free(val_2);
    val_4 = my_free(val_4);

    val_5 = my_strndup("edcba", sizeof("edcba") - 1);
    val_4 = my_strndup("dcba", sizeof("dcba") - 1);
    val_3 = my_strndup("cba", sizeof("cba") - 1);
    val_1 = my_strndup("a", sizeof("a") - 1);
    val_2 = my_strndup("ba", sizeof("ba") - 1);
    printf("%s\n", val_1);
    printf("%s\n", val_2);
    printf("%s\n", val_3);
    printf("%s\n", val_4);
    printf("%s\n", val_5);
    val_5 = my_free(val_5);
    val_4 = my_free(val_4);
    val_3 = my_free(val_3);
    val_2 = my_free(val_2);
    val_1 = my_free(val_1);

    exit(EXIT_SUCCESS);
}